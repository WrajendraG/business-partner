package com.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Currency")
public class Currency {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="currency_id")
	public int id;
	@Column(name="currency_name")
	public String name;
	
	
	public int getId() {
		System.out.println("Currency = "+id);
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		System.out.println("Currency = "+name);
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
