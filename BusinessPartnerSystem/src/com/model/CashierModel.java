package com.model;



public class CashierModel {
	
	
	private Integer id;
	
	private String firstname;

	private String lastname;

	private String username;

	private String email;
	
	private String password;
	
	private String phonePrefix;
	
	private String phone;
	
	private String language;
	
	private String sscnum;
	
	private String address;
	
	private String country;
	
	private String postcode;
	
	private String bdate;

	private String gender;

	private String currency;

	private String City;
	
	private String affiliateId;

	public Integer getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getPhonePrefix() {
		return phonePrefix;
	}

	public String getPhone() {
		return phone;
	}

	public String getLanguage() {
		return language;
	}

	public String getSscnum() {
		return sscnum;
	}

	public String getAddress() {
		return address;
	}

	public String getCountry() {
		return country;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getBdate() {
		return bdate;
	}

	public String getGender() {
		return gender;
	}

	public String getCurrency() {
		return currency;
	}

	public String getCity() {
		return City;
	}

	public String getAffiliateId() {
		return affiliateId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhonePrefix(String phonePrefix) {
		this.phonePrefix = phonePrefix;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setSscnum(String sscnum) {
		this.sscnum = sscnum;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setBdate(String bdate) {
		this.bdate = bdate;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setCity(String city) {
		City = city;
	}

	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}


}
