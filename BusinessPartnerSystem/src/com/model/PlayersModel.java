package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Email;
@Entity
@Table(name="players")
public class PlayersModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="players_id")
	public Integer id;
	@Column(name="players_firstname")
	public String firstname;
	@Column(name="players_lastname")
	public String lastname;
	@Column(name="players_username")
    public String username;
	@Column(name="players_email")
	public String email;
	@Column(name="players_password")
	public String password;
	@Column(name="players_phone_prefix")
	public String phonePrefix;
	@Column(name="players_phone")
	public String phone;
	@Column(name="players_language")
	public String language;
	@Column(name="players_sscnum")
	public String sscnum;
	@Column(name="players_address")
	public String address;
	@Column(name="players_country")
	public String country;
	@Column(name="players_postcode")
	public String postcode;
	@Column(name="bdate")
	public String bdate;
	@Column(name="gender")
	public String gender;
	@Column(name="players_currency")
	public String currency;
	@Column(name="City")
	public String City;
	@Column(name="affiliated_Id")
	public String affiliateId;

	@Transient
	public String newUser;
	
		//getters and setters
		
	
	public String getNewUser() {
		return newUser;
	}
	public void setNewUser(String newUser) {
		this.newUser = newUser;
	}
		public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
		public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
		public String getBdate() {
		return bdate;
	     }
	    public void setBdate(String bdate) {
		this.bdate = bdate;
	     }
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getFirstname() {
			return firstname;
		}
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		public String getLastname() {
			return lastname;
		}
		public void setLastname(String lastname) {
			this.lastname = lastname;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getPhonePrefix() {
			return phonePrefix;
		}
		public void setPhonePrefix(String phonePrefix) {
			this.phonePrefix = phonePrefix;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getLanguage() {
			return language;
		}
		public void setLanguage(String language) {
			this.language = language;
		}
		public String getSscnum() {
			return sscnum;
		}
		public void setSscnum(String sscnum) {
			this.sscnum = sscnum;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public String getCurrency() {
			return currency;
		}
		public void setCurrency(String currency) {
			this.currency = currency;
		}
		public String getPostcode() {
			return postcode;
		}
		public void setPostcode(String postcode) {
			this.postcode = postcode;
		}
		public String getAffiliateId() {
			return affiliateId;
		}
		public void setAffiliateId(String affiliateId) {
			this.affiliateId = affiliateId;
		}
}
