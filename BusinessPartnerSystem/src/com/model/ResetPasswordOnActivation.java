package com.model;

public class ResetPasswordOnActivation {

	private String userName;
	private String activationCode;
	private String resetPassword;
	private String confirmResetPassword;
	private String salt;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getConfirmResetPassword() {
		return confirmResetPassword;
	}

	public void setConfirmResetPassword(String confirmResetPassword) {
		this.confirmResetPassword = confirmResetPassword;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

}
