package com.model;
	import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

	//change table name in dao according to table name 
	@Entity
	@Table(name="balances")
	public class  DepositToFicha {
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="balances_id")
		public Integer id;
		@Column(name="casino_manager_id")
		public Integer casinoManagerId;
		@Column(name="business_partner_id")
	    public Integer businessPartnerId;
		@Column(name="status")
	    public Integer statusId;
		@Column(name="currency")
	    public Integer currencyId;
		@Column(name="payment")
	    public Integer paymentId;
		@Column(name="amount")
	    public Double amount;
		@Column(name="deposit_or_credit")
	    public Integer depositType;
		@Column(name="request_source")
	    public Integer requestSource;
		@Column(name="commission_amount")
	    public Double commissionAmount;
		@Column(name="created_on")
		public Date createdOn;
		@Column(name="updated_on")
		public Date updatedOn;
		
		@Transient
	    public String amountStr;
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Integer getCasinoManagerId() {
			return casinoManagerId;
		}
		public void setCasinoManagerId(Integer casinoManagerId) {
			this.casinoManagerId = casinoManagerId;
		}
		public Integer getBusinessPartnerId() {
			return businessPartnerId;
		}
		public void setBusinessPartnerId(Integer businessPartnerId) {
			this.businessPartnerId = businessPartnerId;
		}
		public Integer getStatusId() {
			return statusId;
		}
		public void setStatusId(Integer statusId) {
			this.statusId = statusId;
		}
		public Integer getCurrencyId() {
			return currencyId;
		}
		public void setCurrencyId(Integer currencyId) {
			this.currencyId = currencyId;
		}
		public Integer getPaymentId() {
			return paymentId;
		}
		public void setPaymentId(Integer paymentId) {
			this.paymentId = paymentId;
		}
		public Double getAmount() {
			return amount;
		}
		public void setAmount(Double amount) {
			this.amount = amount;
		}
		
		public Integer getDepositType() {
			return depositType;
		}
		public void setDepositType(Integer depositType) {
			this.depositType = depositType;
		}
		
		public Integer getRequestSource() {
			return requestSource;
		}
		public void setRequestSource(Integer requestSource) {
			this.requestSource = requestSource;
		}
		public Double getCommissionAmount() {
			return commissionAmount;
		}
		public void setCommissionAmount(Double commissionAmount) {
			this.commissionAmount = commissionAmount;
		}
		
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
		public Date getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}
		public String getAmountStr() {
			return amountStr;
		}
		public void setAmountStr(String amountStr) {
			this.amountStr = amountStr;
		}
		
		
	}

	
	
/*private int id;
private String currency;
private String payment;
private String amount;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getCurrency() {
	return currency;
}
public void setCurrency(String currency) {
	this.currency = currency;
}
public String getPayment() {
	return payment;
}
public void setPayment(String payment) {
	this.payment = payment;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}


}*/