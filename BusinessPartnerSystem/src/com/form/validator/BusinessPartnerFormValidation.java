package com.form.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.model.BusinessPartnerModel;


public class BusinessPartnerFormValidation implements Validator{
	
	
	private Pattern pattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	//private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	private static final String PASSWORD_PATTERN = "(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=\\S)[A-Za-z0-9@#$%^&+=]+$";
	String ID_PATTERN = "[0-9]+";
	String STRING_PATTERN = "[a-zA-Z]+";
	String MOBILE_PATTERN = "[0-9]{6,12}";
	
	
	

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		BusinessPartnerModel businessForm=(BusinessPartnerModel) target;
		
		//System.out.println("Inside Business form validations!!!!!!!!!!");
		
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "required.id", "Id is required.");

		// input string conatains numeric values only
/*		if (b1.getId() != null) {
			pattern = Pattern.compile(ID_PATTERN);
			matcher = pattern.matcher(b1.getId().toString());
			if (!matcher.matches()) {
				errors.rejectValue("id", "id.incorrect", "Enter a numeric value");
			}

			// input string can not exceed that a limit
			if (b1.getId().toString().length() > 5) {
				errors.rejectValue("id", "id.exceed", "Id should not contain more than 5 digits");
			}
		}*/

		ValidationUtils.rejectIfEmpty(errors, "firstname", "required.firstname", "First Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "lastname", "required.lastname", "Last Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "username", "required.lastname", "User Name is required.");
		//
/*		// input string conatains characters only
		if (!(b1.getName() != null && b1.getName().isEmpty())) {
			pattern = Pattern.compile(STRING_PATTERN);
			matcher = pattern.matcher(b1.getName());
			if (!matcher.matches()) {
				errors.rejectValue("name", "name.containNonChar", "Enter a valid name");
			}
		}*/

		// radio buttons validation
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gender", "required.gender", "Select your gender");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required.email", "Email is required.");

		// email validation in spring
		if (!(businessForm.getEmail() != null && businessForm.getEmail().isEmpty())) {
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(businessForm.getEmail());
			if (!matcher.matches()) {
				errors.rejectValue("email", "email.incorrect", "Enter a correct email.");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "required.phone", "Phone is required.");

		// phone number validation
		if (!(businessForm.getPhone() != null && businessForm.getPhone().isEmpty())) {
			pattern = Pattern.compile(MOBILE_PATTERN);
			matcher = pattern.matcher(businessForm.getPhone());
			if (!matcher.matches()) {
				errors.rejectValue("phone", "phone.incorrect", "Enter a correct phone number");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required.password", "Password is required.");
		
		// email validation in spring
		if (!(businessForm.getPassword() != null && businessForm.getPassword().isEmpty())) {
			pattern = Pattern.compile(PASSWORD_PATTERN);
			matcher = pattern.matcher(businessForm.getPassword());
			if (!matcher.matches()) {
				errors.rejectValue("password", "password.incorrect", "Invalid password pattern. Please provide password in the correct format.");
			}
		}

		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "conpassword", "required.password", "Password is required.");
		
		
		if (!(businessForm.getConpassword() != null && businessForm.getConpassword().isEmpty())) {
			if(!(businessForm.getConpassword().equals(businessForm.getPassword())))
			{
				errors.rejectValue("conpassword", "password.incorrect", "Enter a correct Confirmation Password which should match with Password Field.");
			}
		}
		//conpassword
		//ValidationUtils.rejectIfEmpty(errors, "bdate", "required.bdate", "Birth date is required.");
		
		// password matching validation
/*		if (!b1.getPassword().equals(b1.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "password.mismatch", "Password does not match");
		}*/

		ValidationUtils.rejectIfEmpty(errors, "town", "required.town", "Town is required.");
		ValidationUtils.rejectIfEmpty(errors, "address", "required.address1", "Address is required.");
		ValidationUtils.rejectIfEmpty(errors, "postcode", "required.postcode", "Post code is required.");
		ValidationUtils.rejectIfEmpty(errors, "country", "required.country", "Country is required.");
		
		if (!(businessForm.getCountry() != null && businessForm.getCountry().isEmpty())) {
			
			/*if (b1.getCity().equals("Select Country")) {
				errors.rejectValue("country", "required.country", "Country is required.");
			}*/
		}
		
		ValidationUtils.rejectIfEmpty(errors, "status", "required.status", "Status is required.");
		
		if (businessForm.getStatus() != null) {
			
			if (businessForm.getStatus().equals("Select type")) {
				errors.rejectValue("status", "required.status", "Status is required.");
			}
		}
			
		
		if(businessForm.getAgent())
		{
			//System.out.println("businessForm.getAgent() = "+businessForm.getAgent());
			ValidationUtils.rejectIfEmpty(errors, "affiliateId", "required.affiliate.id", "Affiliate Id is required.");	
		}
		else
		{
			
		}
	}
}
