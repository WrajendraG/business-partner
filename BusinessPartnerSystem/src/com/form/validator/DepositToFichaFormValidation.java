package com.form.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.model.DepositToFicha;

public class DepositToFichaFormValidation implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
		DepositToFicha deposit=(DepositToFicha) target;
		System.out.println("inside deposit to ficha Validation form ");
		ValidationUtils.rejectIfEmpty(errors, "currencyId", "required.currencyId", "Currency is required.");
		ValidationUtils.rejectIfEmpty(errors, "paymentId", "required.paymentId", "Payment is required.");
		ValidationUtils.rejectIfEmpty(errors, "amountStr", "required.amount", "Amount is required.");
		
		// input string conatains numeric values only
		if (deposit.getAmountStr() != null && !deposit.getAmountStr().isEmpty()) {
			Double amount;
			try {
				amount = Double.parseDouble(deposit.getAmountStr());
				
/*				if (((amount == 0.0) || (amount == (-1d)))){
					errors.rejectValue("amountStr", "amountStr.incorrect", "Amount should be greater than 0");
				}*/
				
				if (amount == 0.0 || amount < 0.0){
					errors.rejectValue("amountStr", "amountStr.incorrect.negetive", "Amount should be greater than 0");
				}
			}
			catch (NumberFormatException ex) {
				errors.rejectValue("amountStr", "amountStr.incorrect.alphanumeric", "Amount should be a numeric value");
			}
		// input string can not exceed that a limit
			if (deposit.getAmountStr().toString().length() > 12) {
				errors.rejectValue("amountStr", "amountStr.incorrect.exceed", "Amount should not contain more than 12 digits");
			}
		}
		
		
	}

}
