package com.form.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.model.ChangePassword;

public class ChangePasswordValidation implements Validator {

	private Pattern pattern;
	private Matcher matcher;
	
	private static final String PASSWORD_PATTERN = "(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=\\S)[A-Za-z0-9@#$%^&+=]+$";
	
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unused")
	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		ChangePassword changePasswordForm=(ChangePassword) target;
		
		
		ValidationUtils.rejectIfEmpty(errors, "oldPassword", "required.old.password", "Old Password is required.");
		ValidationUtils.rejectIfEmpty(errors, "newPassword", "required.new.password", "New Password is required.");
		ValidationUtils.rejectIfEmpty(errors, "confirmPassword", "required.confirm.password", "Confirm Password is required.");
		
		
		if (!(changePasswordForm.getOldPassword() != null && changePasswordForm.getOldPassword().isEmpty())) {
			pattern = Pattern.compile(PASSWORD_PATTERN);
			matcher = pattern.matcher(changePasswordForm.getOldPassword());
			if (!matcher.matches()) {
				errors.rejectValue("oldPassword", "password.incorrect", "Invalid password pattern. Please provide password in the correct format.");
			}
		}
		
		
		
		if (!(changePasswordForm.getNewPassword() != null && changePasswordForm.getNewPassword().isEmpty())) {
			pattern = Pattern.compile(PASSWORD_PATTERN);
			matcher = pattern.matcher(changePasswordForm.getNewPassword());
			if (!matcher.matches()) {
				errors.rejectValue("newPassword", "password.incorrect", "Invalid password pattern. Please provide password in the correct format.");
			}
		}
			
		if (!(changePasswordForm.getConfirmPassword() != null && changePasswordForm.getConfirmPassword().isEmpty())) {
			if(!(changePasswordForm.getNewPassword().equals(changePasswordForm.getConfirmPassword())))
			{
				errors.rejectValue("confirmPassword", "password.incorrect", "Enter a correct Confirmation Password which should match with Password Field.");
			}
		}
		
	
	}

}
