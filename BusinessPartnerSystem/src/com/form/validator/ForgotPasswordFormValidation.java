package com.form.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.model.DepositToFicha;
import com.model.ForgotPasswordCredentials;

public class ForgotPasswordFormValidation implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		 @SuppressWarnings("unused")
		ForgotPasswordCredentials forgotPasswordCredentials= (ForgotPasswordCredentials) target;
		 System.out.println("inside deposit to ficha Validation form ");
			ValidationUtils.rejectIfEmpty(errors, "userName", "", "User Name is required.");
	}

}
