package com.form.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.model.ForgotPasswordOnActivation;


public class ForgotPasswordEmailFormActivation implements Validator{

	private Pattern pattern;
	private Matcher matcher;

	//private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	private static final String PASSWORD_PATTERN = "(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=\\S)[A-Za-z0-9@#$%^&+=]+$";

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
		
		
		System.out.println("forgotValidtioncontroller");
		
		ForgotPasswordOnActivation forgotpasswordactivation = (ForgotPasswordOnActivation) target;
		ValidationUtils.rejectIfEmpty(errors, "userName", "Not a valid activation request.");
		
		
		if(forgotpasswordactivation.getUserName() != null && !forgotpasswordactivation.getUserName().isEmpty()){
			ValidationUtils.rejectIfEmpty(errors, "verificationCode", "required.activationCode", "Not a valid activation request.");
			if(forgotpasswordactivation.getVerificationCode() != null && !forgotpasswordactivation.getVerificationCode().isEmpty()){
				
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "resetPassword", "Password is required.");
				
				// email validation in spring
				if (forgotpasswordactivation.getResetPassword() != null && !forgotpasswordactivation.getResetPassword().isEmpty()) {
					pattern = Pattern.compile(PASSWORD_PATTERN);
					matcher = pattern.matcher(forgotpasswordactivation.getResetPassword());
					if (!matcher.matches()) {
						errors.rejectValue("resetPassword", "Invalid password pattern. Please provide password in the correct format.");
					}
				}

				
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmResetPassword", "required.password", "Password is required.");
				
				
				if (forgotpasswordactivation.getConfirmResetPassword() != null && !forgotpasswordactivation.getConfirmResetPassword().isEmpty()) {
					if(!(forgotpasswordactivation.getConfirmResetPassword().equals(forgotpasswordactivation.getResetPassword())))
					{
						errors.rejectValue("confirmResetPassword", "Enter a correct Confirmation Password which should match with Password Field.");
					}
				}

			}
			
		}
		
	}

	
}
