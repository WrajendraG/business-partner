package com.form.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.model.DepositToFicha;
import com.model.DepositWithdrawl;

public class WithdrawDepositFormValidations implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
		DepositWithdrawl depositwithdrawl=(DepositWithdrawl) target;
		System.out.println("inside depositWithdrawl  Validation form ");
		ValidationUtils.rejectIfEmpty(errors, "userName", "required.username", "User Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "amount", "required.amount", "Amount is required.");
	}

}
