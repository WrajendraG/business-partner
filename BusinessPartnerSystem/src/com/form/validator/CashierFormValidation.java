package com.form.validator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.model.CashierModel;
import com.model.PlayersModel;

public class CashierFormValidation implements Validator{

	private Pattern pattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	//private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	private static final String PASSWORD_PATTERN = "(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=\\S)[A-Za-z0-9@#$%^&+=]+$";
	String ID_PATTERN = "[0-9]+";
	String STRING_PATTERN = "[a-zA-Z]+";
	String MOBILE_PATTERN = "[0-9]{6,12}";
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
		
		
		CashierModel cashierModel = (CashierModel) target;
		

		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "required.id", "Id is required.");

		// input string conatains numeric values only


		ValidationUtils.rejectIfEmpty(errors, "firstname", "required.firstname", "First Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "lastname", "required.lastname", "Last Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "username", "required.username", "User Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "sscnum", "required.social.security.no", "Social Security Number is required.");
		ValidationUtils.rejectIfEmpty(errors, "affiliateId", "required.affiliate.id", "Affiliate Id is required.");



		// radio buttons validation
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gender", "required.gender", "Gender is required.");
		
		if (!(cashierModel.getGender() != null && cashierModel.getGender().isEmpty())) {
			
			if (cashierModel.getGender().equals("Select Gender")) {
				errors.rejectValue("gender", "required.gender", "Gender is required.");
			}
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "currency", "required.currency", "Currency is required.");
		
		if (!(cashierModel.getCurrency() != null && cashierModel.getCurrency().isEmpty())) {
			
			if (cashierModel.getCurrency().equals("Select Currency")) {
				errors.rejectValue("currency", "required.currency", "Currency is required.");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required.email", "Email is required.");

		// email validation in spring
		if (!(cashierModel.getEmail() != null && cashierModel.getEmail().isEmpty())) {
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(cashierModel.getEmail());
			if (!matcher.matches()) {
				errors.rejectValue("email", "email.incorrect", "Enter a correct email.");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phonePrefix", "required.phonePrefix", "Country code is required.");
		
		if (!(cashierModel.getPhonePrefix() != null && cashierModel.getPhonePrefix().isEmpty())) {
			
			if (cashierModel.getPhonePrefix().equals("Select Country Code")) {
				errors.rejectValue("phonePrefix", "required.phonePrefix", "Country code is required.");
			}
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "required.phone", "Phone is required.");

		// phone number validation
		if (!(cashierModel.getPhone() != null && cashierModel.getPhone().isEmpty())) {
			pattern = Pattern.compile(MOBILE_PATTERN);
			matcher = pattern.matcher(cashierModel.getPhone());
			if (!matcher.matches()) {
				errors.rejectValue("phone", "phone.incorrect", "Enter a correct phone number.");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required.password", "Password is required.");
		
		// email validation in spring
		if (!(cashierModel.getPassword() != null && cashierModel.getPassword().isEmpty())) {
			pattern = Pattern.compile(PASSWORD_PATTERN);
			matcher = pattern.matcher(cashierModel.getPassword());
			if (!matcher.matches()) {
				errors.rejectValue("password", "password.incorrect", "Invalid password pattern. Please provide password in the correct format.");
			}
		}

		ValidationUtils.rejectIfEmpty(errors, "bdate", "required.bdate", "Birth date is required.");
		


		ValidationUtils.rejectIfEmpty(errors, "City", "required.city", "City is required.");
		ValidationUtils.rejectIfEmpty(errors, "address", "required.address1", "Address is required.");
		ValidationUtils.rejectIfEmpty(errors, "postcode", "required.postcode", "Post code is required.");
		ValidationUtils.rejectIfEmpty(errors, "country", "required.country", "Country is required.");
		
		if (!(cashierModel.getCountry() != null && cashierModel.getCountry().isEmpty())) {
			
			if (cashierModel.getCity().equals("Select Country")) {
				errors.rejectValue("country", "required.country", "Country is required.");
			}
		}
		
		Date date = new Date();
		String todaysDate= new SimpleDateFormat("dd/mm/yyyy").format(date);
		System.out.println("todaysDate = "+todaysDate);
        String bdate=cashierModel.getBdate();
        System.out.println(bdate);
        bdate.split("/");
        todaysDate.split("/");
           //System.out.println("till here");
          //int yy = Integer.parseInt(todaysDate[0]);
		 // text area validation
		//ValidationUtils.rejectIfEmpty(errors, "overview", "required.overview", "Overview is required.");
		
		
		
	}

}
