/*
 * Copyright @2015 Alteatec Pvt Ltd.
 */
package com.util;


// TODO: Auto-generated Javadoc
/**
 * The Class TemplateConstants.
 */
public class TemplateConstants {
	
	/** The Constant T_MARKO. */
	// Templates
	public static final String T_MARKO = "test_template_marko";
	
	/** The Constant MT_REPLACEMENT. */
	// Merge Tags
	public static final String MT_REPLACEMENT = "replacement";
	
	
	/**
	 * To merge tag.
	 *
	 * @param mergeTagName the merge tag name
	 * @return the string
	 */
	public static String toMergeTag(String mergeTagName) {	
		return "*|" + mergeTagName.toUpperCase() + "|*";
	}

}
