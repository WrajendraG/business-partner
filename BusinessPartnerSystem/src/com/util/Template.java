/*
 * Copyright @2015 Alteatec Pvt Ltd.
 */
package com.util;

import java.util.Map;

import com.google.common.collect.Maps;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.model.User;


// TODO: Auto-generated Javadoc
/**
 * The Class Template.
 */
public class Template implements ITemplate {
	
	/** The name. */
	private String name;
	
	/** The merge vars. */
	private Map<String, String> mergeVars;
	
	/** The user. */
	private User user;
	
	/**
	 * Instantiates a new template.
	 *
	 * @param templateName the template name
	 * @param user the user
	 */
	public Template(String templateName, User user) {
		this.mergeVars = Maps.newHashMap();
		this.user = user;
		this.name = templateName;
	}
	
	/* (non-Javadoc)
	 * @see fes.ITemplate#getTemplateName()
	 */
	@Override
	public String getTemplateName() {
		return name;
	}
	
	/* (non-Javadoc)
	 * @see fes.ITemplate#setMergeVar(java.lang.String, java.lang.String)
	 */
	@Override
	public void setMergeVar(String name, String value) {
		mergeVars.put(name, value);
	}
	
	/* (non-Javadoc)
	 * @see fes.ITemplate#reset()
	 */
	@Override
	public void reset() {
		this.user = null;
		this.mergeVars.clear();	
	}
	
	/* (non-Javadoc)
	 * @see fes.ITemplate#getBucket()
	 */
	@Override
	public MergeVarBucket getBucket() throws Exception {
		if(user.equals(null))
			throw new Exception("In order to get Bucket, you need to set desired user first");
		
		MergeVarBucket bucket = new MergeVarBucket();
		bucket.setRcpt(user.getEmail());
		
		MergeVar[] mVars = new MergeVar[mergeVars.size()];
		
		int i = 0;
		for(String key:mergeVars.keySet()) {
			MergeVar mv = new MergeVar();
			mv.setName(key);
			mv.setContent(mergeVars.get(key));
			
			mVars[i++] = mv;
		}
		
		bucket.setVars(mVars);
		return bucket;
	}

	/* (non-Javadoc)
	 * @see fes.ITemplate#getGlobalVars()
	 */
	@Override
	public void getGlobalVars() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see fes.ITemplate#setGlobalVar(java.lang.String, java.lang.String)
	 */
	@Override
	public void setGlobalVar(String name, String value) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see fes.ITemplate#setUser(fes.model.User)
	 */
	@Override
	public void setUser(User user) {
		// TODO Auto-generated method stub
		
	}
}
