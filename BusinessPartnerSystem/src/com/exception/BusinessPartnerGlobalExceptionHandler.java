package com.exception;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BusinessPartnerGlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public String handleSQLException(HttpServletRequest request, Exception ex){
		ex.printStackTrace();
		System.out.println(ex);
		System.out.println("Exception Occured:: URL="+request.getRequestURL());
		return "applicationError";
	}

}