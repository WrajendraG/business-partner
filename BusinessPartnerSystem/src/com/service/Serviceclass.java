package com.service;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.dao.Daointerface;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.jsonentity.GenericApiResponse;
import com.jsonentity.PlayerResponse;
import com.model.BusinessPartnerModel;
import com.model.ChangePassword;
import com.model.DepositToFicha;
import com.model.ForgotPasswordCredentials;
import com.model.ForgotPasswordOnActivation;
import com.model.PlayerCreditModel;
import com.model.PlayerDebitModel;
import com.model.PlayersModel;
import com.model.ResetPasswordOnActivation;
import com.model.User;
import com.util.Constants;
import com.util.EmailSender;

public class Serviceclass implements Serviceinterface {

	@Autowired
	Daointerface daointerface;
	
	@Autowired
	private Random random;
	
	@Autowired
	private EmailSender emailSender;

	@Override
	@Transactional
	public List<HashMap> login(String uname, String pass) {
		List<HashMap> Loginlist=daointerface.login(uname,pass);
		return Loginlist;
	}
	
	@Override
	@Transactional
	public List<String> getSubAgentsByUserName(String subAgentUserName,	Integer agentBusinessPartnerId) {

		return daointerface.getSubAgentsByUserName(subAgentUserName, agentBusinessPartnerId);
	}
	
	@Override
	@Transactional
	public String createSubAgent(BusinessPartnerModel businessPartnerModel, Integer agentBusinessPartnerId) {
		// TODO Auto-generated method stub
		
		byte[] salt = new byte[64];
		random.nextBytes(salt);
		businessPartnerModel.setSalt(Hex.encodeHexString(salt));
		businessPartnerModel.setPassword(createPasswordHash(businessPartnerModel.getPassword(), salt));
		businessPartnerModel.setRegistrationSource(Constants.FICHA_REGISTRATION_SELF);
		businessPartnerModel.setAllowShared(false);
		businessPartnerModel.setStatus(Constants.STATUS_PENDING);
		businessPartnerModel.setActivationCode(UUID.randomUUID().toString());
		businessPartnerModel.setActivatedStatus(false);
		businessPartnerModel.setAgent(true);
		businessPartnerModel.setAgentType(Constants.SUB_AGENT);
		businessPartnerModel.setCasinoManagerId(1);
		if(businessPartnerModel.getCommissionPercentageStr() != null && !businessPartnerModel.getCommissionPercentageStr().isEmpty()){
			businessPartnerModel.setCommissionPercentage(Double.parseDouble(businessPartnerModel.getCommissionPercentageStr()));
		}else{
			businessPartnerModel.setCommissionPercentage(0.0);
		}
		
		String s1=daointerface.createSubAgent(businessPartnerModel, agentBusinessPartnerId);
		return s1;
	}
	
	@Override
	@Transactional
	public String createBusinessPartner(BusinessPartnerModel businessPartnerModel, String username) {
		// TODO Auto-generated method stub
		
		byte[] salt = new byte[64];
		random.nextBytes(salt);
		businessPartnerModel.setSalt(Hex.encodeHexString(salt));
		businessPartnerModel.setPassword(createPasswordHash(businessPartnerModel.getPassword(), salt));

		if(businessPartnerModel.getAgent() == true){
			businessPartnerModel.setAgentType(Constants.MAIN_AGENT);
		}
		businessPartnerModel.setRegistrationSource(Constants.FICHA_REGISTRATION_SELF);
		businessPartnerModel.setAllowShared(false);
		businessPartnerModel.setStatus(Constants.STATUS_PENDING);
		businessPartnerModel.setActivationCode(UUID.randomUUID().toString());
		businessPartnerModel.setActivatedStatus(false);
		businessPartnerModel.setCasinoManagerId(1);
		businessPartnerModel.setCommissionPercentage(0.0);
		
		String s1=daointerface.createBusinessPartner(businessPartnerModel,username);
		return s1;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean isPlayerUserNameAlreadyExists(PlayersModel player, Integer businessPartnerId) throws Exception {
		return daointerface.isPlayerUserNameAlreadyExists(player, businessPartnerId);
	}
	
	@Override
	@Transactional
	public boolean isPlayerExistsForCurrentBP(PlayersModel player, Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return  daointerface.isPlayerExistsForCurrentBP(player, businessPartnerId);
	}
	
	@Override
	@Transactional
	public boolean isPlayerAlreadyExistsForAgents(PlayersModel player, Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return  daointerface.isPlayerAlreadyExistsForAgents(player, businessPartnerId);
	}
	
	@Override
	@Transactional
	public String addExistingPlayerForBP(PlayersModel player, Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return daointerface.addExistingPlayerForBP(player, businessPartnerId);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Map<String, String>> insertplayers(PlayersModel player, String username, Integer businessPartnerId,String affiliateId) throws Exception {
		// TODO Auto-generated method stub
		String s1 ="";
		List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
		try {

			JsonObject request = new JsonObject();
			request.addProperty("skinName", "betmais");
			request.addProperty("defaultCurrency", player.getCurrency());
			request.addProperty("fichaUsername", "AlteatecFicha");
			request.addProperty("fichaPassword", "Alteatec@2013");
			request.addProperty("userName", player.getUsername());
			request.addProperty("password", player.getPassword());
			request.addProperty("confirmPassword", player.getPassword());
			request.addProperty("email", player.getEmail());
			request.addProperty("firstName", player.getFirstname());
			request.addProperty("lastName", player.getLastname());
			request.addProperty("middleName", "");
			request.addProperty("dob", player.getBdate());
			request.addProperty("address1", player.getAddress());
			request.addProperty("phoneNumber", player.getPhone());
			request.addProperty("phonePrefix", player.getPhonePrefix());
			request.addProperty("townCity", player.getCity());
			request.addProperty("postCode", player.getPostcode());
			request.addProperty("countryName", player.getCountry());
			request.addProperty("gender", player.getGender());
			request.addProperty("socialSecurityNumber", player.getSscnum());
			request.addProperty("fichaAffiliateId",affiliateId);   //Affiliated Id is added
			request.addProperty("tnc", true);
			request.addProperty("bonusCode", "");

			String apiUrlToRegisterNewPlayer = "";
			if(Constants.isProduction){
				apiUrlToRegisterNewPlayer = Constants.PROD_API_URL_REGISTER_NEW_PLAYER;
			}else{
				if(Constants.runningForLocalHost){
					apiUrlToRegisterNewPlayer = Constants.TEST_API_URL_REGISTER_NEW_PLAYER;
				}else{
					apiUrlToRegisterNewPlayer = Constants.STAGING_API_URL_REGISTER_NEW_PLAYER;
				}
			}
			// set headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
			RestTemplate restTemplate = new RestTemplate();
			// send request and parse result
			ResponseEntity<String> loginResponse = restTemplate.exchange(apiUrlToRegisterNewPlayer, HttpMethod.POST, entity, String.class);

			ObjectMapper mapper = new ObjectMapper();
			GenericApiResponse genericApiResponse = mapper.readValue(loginResponse.getBody().toString().replace(")]}'", ""), GenericApiResponse.class);

			if (loginResponse.getStatusCode() == HttpStatus.OK) {
				if (genericApiResponse.isSuccess() != true && !genericApiResponse.getErrors().isEmpty()) {
					System.out.println("registration success failed");
					
					errorList = genericApiResponse.getErrors();

				} else {
					player.setAffiliateId(affiliateId);
					s1 = daointerface.insertplayer(player, username, businessPartnerId);
					System.out.println("registration success");
				}

			} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				System.out.println("registration success failed");
			} else {
				System.out.println("registration success failed");
			}
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return errorList;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Map<String, String>> addExistingPlayers(PlayersModel player, String userNameToBeAdd, Integer businessPartnerId, String affiliateId, String userType) throws Exception {
		// TODO Auto-generated method stub
		String s1 ="";
		List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
		try {
			
			String apiUrlToAddExistingPlayer = "";
			if(Constants.isProduction){
				apiUrlToAddExistingPlayer = Constants.PROD_API_URL_ADD_EXISTING_PLAYER;
			}else{
			
				if(Constants.runningForLocalHost){
					apiUrlToAddExistingPlayer = Constants.TEST_API_URL_ADD_EXISTING_PLAYER;
				}else{
					apiUrlToAddExistingPlayer = Constants.STAGING_API_URL_ADD_EXISTING_PLAYER;
				}
			}

			JsonObject request = new JsonObject();
			request.addProperty("skinName", "betmais");
			request.addProperty("userName", userNameToBeAdd);
			request.addProperty("userType", userType);
			//request.addProperty("email", userNameToBeAdd);
			request.addProperty("fichaUsername", "AlteatecFicha");
			request.addProperty("fichaPassword", "Alteatec@2013");
			request.addProperty("fichaAffiliateId", affiliateId); 

			// set headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
			RestTemplate restTemplate = new RestTemplate();
			// send request and parse result
			ResponseEntity<String> loginResponse = restTemplate.exchange(apiUrlToAddExistingPlayer, HttpMethod.POST, entity, String.class);

			ObjectMapper mapper = new ObjectMapper();
			GenericApiResponse genericApiResponse = mapper.readValue(loginResponse.getBody().toString().replace(")]}'", ""), GenericApiResponse.class);

			if (loginResponse.getStatusCode() == HttpStatus.OK) {
				if (genericApiResponse.isSuccess() != true && !genericApiResponse.getErrors().isEmpty()) {
					System.out.println("registration success failed");
					
					errorList = genericApiResponse.getErrors();

				} else {
					mapper = new ObjectMapper();
					PlayerResponse playerResponse = mapper.readValue(genericApiResponse.getMessage(), PlayerResponse.class);
					
					player.setUsername(playerResponse.getUserName());
					player.setAddress(playerResponse.getAddress1());
					player.setBdate(playerResponse.getDob());
					player.setCity(playerResponse.getPlayerCity());
					player.setCountry(playerResponse.getCountryName());
					player.setCurrency(playerResponse.getCurrencyCode());
					player.setEmail(playerResponse.getEmailAddress());
					player.setFirstname(playerResponse.getFirstName());
					player.setGender(playerResponse.getGender());
					player.setLanguage(playerResponse.getPlayerLanguage());
					player.setLastname(playerResponse.getLastName());
					player.setPhone(playerResponse.getPlayerPhone());
					player.setPhonePrefix("");
					player.setPostcode(playerResponse.getPostCode());
					player.setSscnum(playerResponse.getSocialSecurityNumber());
					player.setPassword("");
					s1 = daointerface.insertplayer(player, userNameToBeAdd, businessPartnerId);
					
					System.out.println("registration success");
				}

			} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				System.out.println("registration success failed");
			} else {
				System.out.println("registration success failed");
			}
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return errorList;
	}

	
	@Override
	@Transactional
	public List<String> displayPlayers(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<String> playershow = new ArrayList<String>();
		playershow = daointerface.displayPlayers(businessPartnerId);
		return playershow;
	}

	@Override
	@Transactional
	public List<String> playerallvalue(int id) {
		// TODO Auto-generated method stub
		List<String> playerallvalues = new ArrayList<String>();
		playerallvalues = daointerface.showallvalues(id);
		return playerallvalues;
	}

	@Override
	@Transactional
	public String updateplayers(PlayersModel player, String username) {
		// TODO Auto-generated method stub
		String s1 = daointerface.updateplayer(player, username);
		return s1;
	}

	@Override
	@Transactional
	public String deleteplayers(PlayersModel player) {
		// TODO Auto-generated method stub
		String s1 = daointerface.delete(player);
		return s1;
	}

	@Override
	@Transactional
	public List<String> getpaymentvalues() {
		// TODO Auto-generated method stub
		List<String> payment = new ArrayList<String>();
		payment = daointerface.getpaymentvalues();
		return payment;
	}

	@Override
	@Transactional
	public List<String> getCurrencyList() {
		List<String> currencyList = new ArrayList<String>();
		currencyList = daointerface.getCurrencyList();
		return currencyList;
	}
	
	@Override
	@Transactional
	public List<String> getCountryNameList() {
		List<String> countryNameList = new ArrayList<String>();
		countryNameList = daointerface.getCountryNameList();
		return countryNameList;
	}
	
	@Override
	@Transactional
	public Map<String, String> getCountryPhoneCodeMap() {
		Map<String, String> countryNameList = new HashMap<String, String>();

		countryNameList = daointerface.getCountryPhoneCodeMap();
		return countryNameList;
	}
	
	@Override
	@Transactional
	public Map<Integer, String> getBpTotalBalances(Integer businessPartnerId) {
		Map<Integer, String> businessPartnerTotalBalance = new HashMap<Integer, String>();

		businessPartnerTotalBalance = daointerface.getBpTotalBalances(businessPartnerId);
		return businessPartnerTotalBalance;
	}
	
	@Override
	@Transactional
	public Map<Integer, String> getPlayersList(Integer businessPartnerId) {
		Map<Integer, String> playerList = new HashMap<Integer, String>();

		playerList = daointerface.getPlayersList(businessPartnerId);
		return playerList;
	}
	
	@Override
	@Transactional
	public Map<Integer, String> getPlayersListForAgents(Integer businessPartnerId, Integer agentType) {
		Map<Integer, String> playerList = new HashMap<Integer, String>();

		playerList = daointerface.getPlayersListForAgents(businessPartnerId, agentType);
		return playerList;
	}
		
	@Override
	@Transactional
	public List<String> getPlayersListByCurrency(Integer currencyId, Integer businessPartnerId) {
		List<String> playerList = new ArrayList<String>();

		playerList = daointerface.getPlayersListByCurrency(currencyId, businessPartnerId);
		return playerList;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Map<String, String>> depositToPlayer(PlayerCreditModel playerCreditModel, Integer businessPartnerId, String userType) throws Exception {
		String s1 ="";
		List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
		try {
			System.out.println("getPalyerById");
			//Error Condition
			PlayersModel player = daointerface.getPalyerById(playerCreditModel.getPlayerId());
			//
			System.out.println("after getPalyerById");
			String apiUrlToDepositMoneyToPlayer = "";
			if(Constants.isProduction){
				apiUrlToDepositMoneyToPlayer = Constants.PROD_API_URL_DEPOSIT_MONEY_TO_PLAYER;
			}else{
				if(Constants.runningForLocalHost){
					apiUrlToDepositMoneyToPlayer = Constants.TEST_API_URL_DEPOSIT_MONEY_TO_PLAYER;
				}else{
					apiUrlToDepositMoneyToPlayer = Constants.STAGING_API_URL_DEPOSIT_MONEY_TO_PLAYER;
				}
			}
			System.out.println("apiUrlToDepositMoneyToPlayer -->" + apiUrlToDepositMoneyToPlayer);
			JsonObject request = new JsonObject();
			request.addProperty("skinName", "betmais");
			request.addProperty("fichaUsername", "AlteatecFicha");
			request.addProperty("fichaPassword", "Alteatec@2013");
			request.addProperty("userType", userType);
			request.addProperty("userName", player.getUsername());
			request.addProperty("email", player.getEmail());
			request.addProperty("amount", playerCreditModel.getDepositAmount());
			
			// set headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
			RestTemplate restTemplate = new RestTemplate();
			System.out.println("before call to -->" + apiUrlToDepositMoneyToPlayer);
			// send request and parse result
			ResponseEntity<String> loginResponse = restTemplate.exchange(apiUrlToDepositMoneyToPlayer, HttpMethod.POST, entity, String.class);
			System.out.println("after call to -->" + apiUrlToDepositMoneyToPlayer);
			ObjectMapper mapper = new ObjectMapper();
			GenericApiResponse genericApiResponse = mapper.readValue(loginResponse.getBody().toString().replace(")]}'", ""), GenericApiResponse.class);
			System.out.println("genericApiResponse -->" + genericApiResponse);
			if (loginResponse.getStatusCode() == HttpStatus.OK) {
				if (genericApiResponse.isSuccess() != true && !genericApiResponse.getErrors().isEmpty()) {
					System.out.println("deposit failed");
					errorList = genericApiResponse.getErrors();

				} else if (genericApiResponse.isSuccess()){
					System.out.println("deposit call to api success");
					String jsonString = genericApiResponse.getMessage();
					HashMap hasMapForTransaction = mapper.readValue(jsonString, HashMap.class);
					playerCreditModel.setBusinessPartnerId(businessPartnerId);
					playerCreditModel.setCreatedOn(new Date());
					playerCreditModel.setUpdatedOn(new Date());
					playerCreditModel.setTransactionId((Integer)hasMapForTransaction.get("id"));
					System.out.println("before inserting into player's credit");
					s1 = daointerface.depositToPlayer(playerCreditModel, businessPartnerId);
					System.out.println("after inserting into player's credit");
					System.out.println("deposit success");
				}

			} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				System.out.println("deposit failed");
			} else {
				System.out.println("deposit failed");
			}
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return errorList;
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Map<String, String>> withdrawFromPlayer(PlayerDebitModel playerDebitModel, Integer businessPartnerId) throws Exception {
		String s1 ="";
		List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
		try {
			
			PlayersModel player = daointerface.getPalyerById(playerDebitModel.getPlayerId());
			
			String apiUrlToWithdrawMoneyFromPlayer = "";
			if(Constants.isProduction){
				apiUrlToWithdrawMoneyFromPlayer = Constants.PROD_API_URL_WITHDRAW_MONEY_FROM_PLAYER;
			}else{
				if(Constants.runningForLocalHost){
					apiUrlToWithdrawMoneyFromPlayer = Constants.TEST_API_URL_WITHDRAW_MONEY_FROM_PLAYER;
				}else{
					apiUrlToWithdrawMoneyFromPlayer = Constants.STAGING_API_URL_WITHDRAW_MONEY_FROM_PLAYER;
				}
			}
			
			JsonObject request = new JsonObject();
			request.addProperty("skinName", "betmais");
			request.addProperty("fichaUsername", "AlteatecFicha");
			request.addProperty("fichaPassword", "Alteatec@2013");
			request.addProperty("userName", player.getUsername());
			request.addProperty("email", player.getEmail());
			request.addProperty("amount", playerDebitModel.getWithdrawAmount());
			
			// set headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
			RestTemplate restTemplate = new RestTemplate();
			// send request and parse result
			ResponseEntity<String> loginResponse = restTemplate.exchange(apiUrlToWithdrawMoneyFromPlayer, HttpMethod.POST, entity, String.class);

			ObjectMapper mapper = new ObjectMapper();
			GenericApiResponse genericApiResponse = mapper.readValue(loginResponse.getBody().toString().replace(")]}'", ""), GenericApiResponse.class);

			if (loginResponse.getStatusCode() == HttpStatus.OK) {
				if (genericApiResponse.isSuccess() != true && !genericApiResponse.getErrors().isEmpty()) {
					System.out.println("withdraw failed");
					errorList = genericApiResponse.getErrors();

				} else if (genericApiResponse.isSuccess()){
					
					String jsonString = genericApiResponse.getMessage();
					HashMap hasMapForTransaction = mapper.readValue(jsonString, HashMap.class);
					playerDebitModel.setBusinessPartnerId(businessPartnerId);
					playerDebitModel.setCreatedOn(new Date());
					playerDebitModel.setUpdatedOn(new Date());
					playerDebitModel.setTransactionId((Integer)hasMapForTransaction.get("id"));
					s1 = daointerface.withdrawFromPlayer(playerDebitModel, businessPartnerId);

					System.out.println("withdraw success");
				}

			} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				System.out.println("withdraw failed");
			} else {
				System.out.println("withdraw failed");
			}
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return errorList;
	}

	
	@Override
	@Transactional
	public boolean validateDepositAmount(PlayerCreditModel playerCreditModel, Integer businessPartnerId) {
		boolean result = daointerface.validateDepositAmount(playerCreditModel, businessPartnerId);
		return result;
	}
	
	@Override
	@Transactional
	public boolean validatePlayerCurrencyForDepositWithdraw(Integer playerId, Integer currencyIdUsedforDepositWithdraw){
		boolean result = daointerface.validatePlayerCurrencyForDepositWithdraw(playerId, currencyIdUsedforDepositWithdraw);
		return result;
	}
	
	@Override
	@Transactional
	public List<String> displayPlayersCreditInfo(String username,Integer businessPartnerId) {
		List<String> playerCreditInfo = daointerface.displayPlayersCreditInfo(username,businessPartnerId);
		return playerCreditInfo;
	}
	
	@Override
	@Transactional
	public List<String> displayPlayersDebitInfo(Integer businessPartnerId) {
		List<String> playerDebitInfo = daointerface.displayPlayersDebitInfo(businessPartnerId);
		return playerDebitInfo;
	}

	@Override
	@Transactional
	public String insertDepositToFicha(DepositToFicha df) {
		// TODO Auto-generated method stub
		return daointerface.insertDepositToFicha(df);
	}

	@Override
	@Transactional
	public Map<Integer, String> getCurrencyIdValueMap() {
		// TODO Auto-generated method stub
		return daointerface.currencyValues();
	}

	@Override
	@Transactional
	public Map<Integer, String> getPaymentTypeIdValueMap() {
		// TODO Auto-generated method stub
		return daointerface.paymentValues();
	}

	@Override
	@Transactional
	public List<String> depositToFichaValue(DepositToFicha df) {
		// TODO Auto-generated method stub
		return daointerface.depositToFichaShowValues(df);
	}

	@Override
	@Transactional
	public List<String> businessPartnerCurrencyDetails(DepositToFicha df) {
		// TODO Auto-generated method stub
		return daointerface.businessPartnerCurrencyDetails(df);
	}

	@Override
	@Transactional
	public List<String> businessPartnerInformationDetails(DepositToFicha df) {
		// TODO Auto-generated method stub
		return daointerface.businessPartnerInformationDetails(df);
	}



	@Override
	@Transactional
	public List<String> getPlayersByUserName(String playerUserName,
			Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return daointerface.getPlayersByUserName(playerUserName,businessPartnerId);
	}
	
	@Override
	@Transactional
	public List<String> getPlayersByUserNameForAgents(String playerUserName, Integer businessPartnerId, Integer agentType){
		// TODO Auto-generated method stub
		return daointerface.getPlayersByUserNameForAgents(playerUserName,businessPartnerId, agentType);
	}

	@Override
	@Transactional
	public List<String> getPlayerListByCurrency(Integer currencyValue,Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return daointerface.getPlayersByCurrencyName(currencyValue,businessPartnerId);
	}
	
	@Override
	@Transactional
	public Integer isAgent(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional
	public List playerNamesDetails(String playerName,Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return daointerface.getPlayerNamesDetails(playerName,businessPartnerId);
	}
	
	
	@Override
	@Transactional
	public List<String> allPlayersName(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return daointerface.getAllPlayerName(businessPartnerId);
	}
	
	
	@Override
	@Transactional
	public List<String> depositWithdrwalPlayerInformation(int players_id) {
		// TODO Auto-generated method stub
		return daointerface.depositWithdrwalPlayerInformation(players_id);
	}
	
	
	
	@SuppressWarnings("null")
	@Override
	@Transactional
	public String checkUserName(
			ForgotPasswordCredentials forgotPasswordCredentials) {
		// TODO Auto-generated method stub
		String result;
		BusinessPartnerModel businessPartner=new BusinessPartnerModel() ;
		String s1=daointerface.checkUserName(forgotPasswordCredentials);
		
		if(s1.equals("Fail"))
		{
		 result=""+s1;
		}
		else
		{
			
			businessPartner.setActivationCode(UUID.randomUUID().toString());
			List<Integer>getBusinessPartnerId=daointerface.updateActivationCode(forgotPasswordCredentials,businessPartner);
			List<HashMap> businessPartnerListForActivation = daointerface.getBusinessPartnersForActivation(getBusinessPartnerId.get(0));

			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			for (HashMap businessPartnerForActivation : businessPartnerListForActivation) {
				User user = new User();
				user.setUsername((String) businessPartnerForActivation.get("business_username"));
				user.setEmail((String) businessPartnerForActivation.get("business_email"));
				user.setActivationCode((String) businessPartnerForActivation.get("bp_activation_code"));

				EmailRunnable myRunnable = new EmailRunnable(user, Constants.IP_ADDRESS_SERVER, emailSender, "forgotPasswordActivationEmail", responseMap);
				Thread thread = new Thread(myRunnable);
				thread.start();
			
		}
		
		}
		
		return s1;
	}
	
	
	@Override
	@Transactional
	public String resetPasswordOnActivation(ResetPasswordOnActivation resetPasswordOnActivation) {
		
		byte[] salt = new byte[64];
		random.nextBytes(salt);
		resetPasswordOnActivation.setSalt(Hex.encodeHexString(salt));
		resetPasswordOnActivation.setResetPassword(createPasswordHash(resetPasswordOnActivation.getResetPassword(), salt));

		String result =  daointerface.resetPasswordOnActivation(resetPasswordOnActivation);
		return result;
	}
	
	@Override
	@Transactional
	public String changePassword(ChangePassword changePassword, Integer businessPartnerId) {
		
		byte[] salt = new byte[64];
		random.nextBytes(salt);
		changePassword.setSalt(Hex.encodeHexString(salt));
		changePassword.setNewPassword(createPasswordHash(changePassword.getNewPassword(), salt));

		String result =  daointerface.changePassword(changePassword, businessPartnerId);
		return result;
	}

	@Override
	@Transactional
	public String forgotPasswordOnActivationAction(
			ForgotPasswordOnActivation forgotPasswordOnActivation) {
		// TODO Auto-generated method stub
		byte[] salt = new byte[64];
		random.nextBytes(salt);
		forgotPasswordOnActivation.setSalt(Hex.encodeHexString(salt));
		forgotPasswordOnActivation.setResetPassword(createPasswordHash(forgotPasswordOnActivation.getResetPassword(), salt));
		String result =  daointerface.forgotPasswordOnActivationAction(forgotPasswordOnActivation);
		return result;
	}
	
	/**
	 * Creates the password hash.
	 *
	 * @param password
	 *            the password
	 * @param salt
	 *            the salt
	 * @return the string
	 */
	private String createPasswordHash(String password, byte[] salt) {
		byte[] hash;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			digest.update(salt);
			digest.update(password.getBytes());
			hash = digest.digest();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
			System.out.println("Error generating secure password");
			throw new UnsupportedOperationException(e);
		}
		return Hex.encodeHexString(hash);
	}



	

	




}

class EmailRunnable implements Runnable {
	private User user;
	private String serverPath;
	private EmailSender emailSender;
	private String identifier;
	private HashMap<String, Object> map;

	public EmailRunnable(User user, String serverPath, EmailSender emailSender, String identifier, HashMap<String, Object> map) {
		this.user = user;
		this.serverPath = serverPath;
		this.emailSender = emailSender;
		this.identifier = identifier;
		this.map = map;
	}

	public void run() {
		if (identifier.contains("welcomeEmail")) {
			emailSender.welcomeEmail(user, serverPath, map);
		} else if (identifier.contains("activationEmail")) {
			emailSender.activationEmail(user, serverPath, map);
		}
		else if(identifier.contains("forgotPasswordActivationEmail")){
			System.out.println("in forgot password condition");
			emailSender.forgotPasswordEmail(user, serverPath, map);
		}
	}
}