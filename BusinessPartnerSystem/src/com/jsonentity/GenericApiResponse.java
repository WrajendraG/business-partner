/*
 * Copyright @2015 Alteatec Pvt Ltd.
 */
package com.jsonentity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericApiResponse.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenericApiResponse {

	/** The message. */
	private String message;
	
	/** The error. */
	private boolean error;
	
	/** The success. */
	private boolean success;
	
	/** The error code. */
	private int errorCode;
	
	/** The errors. */
	private List<Map<String,String>> errors;

	/**
	 * Instantiates a new generic api response.
	 */
	public GenericApiResponse() {
		this.message = "";
		this.error = false;
		this.success = true;
		this.errorCode = -1;
		this.errors = new ArrayList<Map<String,String>>();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public List<Map<String, String>> getErrors() {
		return errors;
	}

	public void setErrors(List<Map<String, String>> errors) {
		this.errors = errors;
	}

}
