package com.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.model.Login;

public class BusinessPartnerSessionFilter implements Filter {

	private ArrayList<String> urlList;

	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String url = request.getServletPath();
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		boolean allowedRequest = false;
		if (urlList.contains(url)) {
			allowedRequest = true;
		}

		if (!allowedRequest) {
			HttpSession httpSession = request.getSession(false);
			if (null == httpSession) {
				httpSession = request.getSession();
				httpSession.setAttribute("loginMessage", "You session has been timed out.");
				response.sendRedirect("businessindex.jsp");
			} else {
				Login businessPartnerUser = (Login) httpSession.getAttribute("businessPartnerUser");
				if (businessPartnerUser == null) {
					httpSession.setAttribute("loginMessage", "You must be logged in to access this page.");
					response.sendRedirect("businessindex.jsp");
				} else {
					httpSession.removeAttribute("indexPage");

					chain.doFilter(request, response);
					
				}
			}
		} else {
			if (urlList.contains(url)) {
				HttpSession httpSession = request.getSession();
				httpSession.setAttribute("indexPage", "login");
			}
			chain.doFilter(request, response);
		}
	}

	public void init(FilterConfig config) throws ServletException {
		String urls = config.getInitParameter("avoid-urls");
		StringTokenizer token = new StringTokenizer(urls, ",");
		urlList = new ArrayList<String>();
		while (token.hasMoreTokens()) {
			urlList.add(((String) token.nextToken()).trim());

		}
	}
}