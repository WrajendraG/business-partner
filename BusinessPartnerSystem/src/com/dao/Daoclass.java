package com.dao;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.model.BusinessPartnerBalancesModel;
import com.model.BusinessPartnerModel;
import com.model.ChangePassword;
import com.model.DepositToFicha;
import com.model.ForgotPasswordCredentials;
import com.model.ForgotPasswordOnActivation;
import com.model.PlayerCreditModel;
import com.model.PlayerDebitModel;
import com.model.PlayersModel;
import com.model.ResetPasswordOnActivation;
import com.util.Constants;

public class Daoclass implements Daointerface {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<HashMap> login(String uname, String pass) {

		Session session = null;
		List<HashMap> Loginlist = new ArrayList<HashMap>();
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select business_id, affiliate_id, business_salt, business_password, is_agent, agent_type from business "
				+ " where lower(business_username) = :userName " + " and business_status = 1 " + " and business_allowshared = true" + " and bp_activated_status = true");
		query.setParameter("userName", uname.toLowerCase());
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		Loginlist = query.list();
		byte[] saltInByte;
		if (Loginlist.size() > 0) {
			for (HashMap user : Loginlist) {
				String salt = (String) user.get("business_salt");
				String password = (String) user.get("business_password");

				try {
					saltInByte = Hex.decodeHex(salt.toCharArray());
				} catch (DecoderException e) {
					System.out.println("Error decoding password salt");
					System.out.println(e);
					throw new RuntimeException(e);
				}

				if (password.equals(createPasswordHash(pass, saltInByte))) {
					return Loginlist;
				}
			}
		}
		Loginlist = new ArrayList<HashMap>();
		return Loginlist;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getSubAgentsByUserName(String subAgentUserName, Integer agentBusinessPartnerId) {
		// TODO Auto-generated method stub
		Session session = null;
		List<String> subAgentsList = new ArrayList<String>();
		session = sessionFactory.getCurrentSession();

		System.out.println("agentBusinessPartnerId : " + agentBusinessPartnerId);
		SQLQuery query = session.createSQLQuery("select buss.business_firstname, buss.business_lastname, buss.business_username, buss.business_email, "
				+ " buss.business_allowshared, buss.business_status, buss.business_phone, buss.business_address, "
				+ " buss.business_town, buss.business_postcode, buss.business_country " + " from business buss, agent_sub_agent_map agentSubAgentMap "
				+ " where buss.business_username like :businessUserName" + " and agentSubAgentMap.sub_agent_id = buss.business_id"
				+ " and  agentSubAgentMap.main_agent_id = :agentBusinessPartnerId");
		query.setParameter("businessUserName", "%" + subAgentUserName + "%");
		query.setParameter("agentBusinessPartnerId", agentBusinessPartnerId);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		subAgentsList = query.list();
		return subAgentsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String createSubAgent(BusinessPartnerModel businessPartnerModel, Integer agentBusinessPartnerId) {

		Session session = null;
		List<String> businessshowallvaluescheck = new ArrayList<String>();
		session = sessionFactory.getCurrentSession();

		businessshowallvaluescheck.removeAll(businessshowallvaluescheck);

		SQLQuery query = session.createSQLQuery("select * from business where lower(business_username) = :userName or lower(business_email) = :businessPartnerEmail");

		query.setParameter("userName", businessPartnerModel.getUsername().toLowerCase());
		query.setParameter("businessPartnerEmail", businessPartnerModel.getEmail().toLowerCase());

		businessshowallvaluescheck = query.list();
		String result = "";
		if (businessshowallvaluescheck.isEmpty()) {

			businessPartnerModel.setCreatedOn(new Date());
			businessPartnerModel.setUpdatedOn(new Date());
			session.save(businessPartnerModel);

			query = session.createSQLQuery("insert into agent_sub_agent_map (main_agent_id, sub_agent_id) values (:agentBusinessPartnerId, :subAgentBusinessPartnerId)");
			query.setParameter("agentBusinessPartnerId", agentBusinessPartnerId);
			query.setParameter("subAgentBusinessPartnerId", businessPartnerModel.getId());
			int resultInt = query.executeUpdate();
			result = "Success";
		} else {
			result = "Fail";
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String createBusinessPartner(BusinessPartnerModel businessPartnerModel, String userName) {

		Session session = null;
		List<String> businessshowallvaluescheck = new ArrayList<String>();
		session = sessionFactory.getCurrentSession();

		businessshowallvaluescheck.removeAll(businessshowallvaluescheck);

		SQLQuery query = session.createSQLQuery("select * from business where lower(business_username) = :userName or lower(business_email) = :businessPartnerEmail");

		query.setParameter("userName", userName.toLowerCase());
		query.setParameter("businessPartnerEmail", businessPartnerModel.getEmail().toLowerCase());
		businessshowallvaluescheck = query.list();
		String result = "";
		if (businessshowallvaluescheck.isEmpty()) {

			businessPartnerModel.setCreatedOn(new Date());
			businessPartnerModel.setUpdatedOn(new Date());
			session.save(businessPartnerModel);

			result = "Success";
		} else {
			result = "Fail";
		}

		return result;

	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isPlayerUserNameAlreadyExists(PlayersModel player, Integer businessPartnerId) throws Exception {

		List<Integer> playerlistcheck = new ArrayList<Integer>();
		Session session = null;

		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select players_id from players where lower(players_username) = :userName or lower(players_email) = :playerEmail");

		query.setParameter("userName", player.getUsername().toLowerCase());
		query.setParameter("playerEmail", player.getEmail() == null ? "" : player.getEmail().toLowerCase());
		playerlistcheck = query.list();

		if (playerlistcheck.isEmpty()) {

			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean isPlayerExistsForCurrentBP(PlayersModel player, Integer businessPartnerId) {

		Integer playerId = null;
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select players_id from players where lower(players_username) = :userName");
		query.setParameter("userName", player.getUsername().toLowerCase());
		playerId = (Integer) query.uniqueResult();
		
		query = session.createSQLQuery("select pbpm.player_id from players_business_partners_map pbpm, business buss "
										+ " where pbpm.business_partner_id = buss.business_id " 
										+ " and buss.is_agent = true" 
										+ " and pbpm.player_id = :playerId");
		
		query.setParameter("playerId", playerId);
		List<String> playerlistcheck = query.list();

		if (!playerlistcheck.isEmpty()) {
			return true;
		}

		query = session.createSQLQuery("select * from players_business_partners_map where player_id = :playerId and business_partner_id = :businessPartnerId");
		query.setParameter("playerId", playerId);
		query.setParameter("businessPartnerId", businessPartnerId);
		playerlistcheck = query.list();

		if (playerlistcheck.isEmpty()) {
			player.setId(playerId);
			return false;
		} else {
			return true;
		}

	}

	@Override
	public boolean isPlayerAlreadyExistsForAgents(PlayersModel player, Integer businessPartnerId) {

		Integer playerId = null;
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select players_id from players where lower(players_username) = :userName");
		query.setParameter("userName", player.getUsername().toLowerCase());
		playerId = (Integer) query.uniqueResult();

/*		query = session.createSQLQuery("select pbpm.player_id from players_business_partners_map pbpm, business buss "
				+ " where pbpm.business_partner_id = buss.business_id " + " and buss.is_agent = true" + " and pbpm.player_id = :playerId");
*/		
		query = session.createSQLQuery("select pbpm.player_id from players_business_partners_map pbpm, business buss "
				+ " where pbpm.business_partner_id = buss.business_id and pbpm.player_id = :playerId");
		query.setParameter("playerId", playerId);
		List<String> playerlistcheck = query.list();

		if (playerlistcheck.isEmpty()) {
			player.setId(playerId);
			return false;
		} else {
			return true;
		}

	}

	@Override
	public String addExistingPlayerForBP(PlayersModel player, Integer businessPartnerId) {

		String result = "";
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("insert into players_business_partners_map (player_id, business_partner_id) values (:playerId, :businessPartnerId)");
		query.setParameter("playerId", player.getId());
		query.setParameter("businessPartnerId", businessPartnerId);
		int resultInt = query.executeUpdate();

		if (resultInt > 0) {
			result = "Success";
		} else {
			result = "Fail";
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String insertplayer(PlayersModel player, String userName, Integer businessPartnerId) throws Exception {
		// TODO Auto-generated method stub
		List<String> Playerlistcheck = new ArrayList<String>();
		String result = "";
		Session session = null;
		session = sessionFactory.getCurrentSession();
		// bpbalances.setTotalAmount(0.0);
		SQLQuery query = session.createSQLQuery("select * from players where lower(players_username) = :userName");
		query.setParameter("userName", userName.toLowerCase());
		Playerlistcheck = query.list();

		if (Playerlistcheck.isEmpty()) {
			player.setPassword("");
			session.save(player);

			query = session.createSQLQuery("insert into players_business_partners_map (player_id, business_partner_id) values (:playerId, :businessPartnerId)");
			query.setParameter("playerId", player.getId());
			query.setParameter("businessPartnerId", businessPartnerId);
			int resultInt = query.executeUpdate();
			result = "Success";
		} else {
			result = "Fail";
		}

		return result;
	}

	@Override
	public String depositToPlayer(PlayerCreditModel playerCreditModel, Integer businessPartnerId) throws Exception {
		String result = "";
		Session session = null;
		session = sessionFactory.getCurrentSession();

		session.save(playerCreditModel);
		String sqlStr = "update business_partner_balances set total_amount = total_amount - :depositAmount" + " where business_partner_id = :businessPartnerId "
				+ " and currency_id =  :currencyId ";

		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("depositAmount", playerCreditModel.getDepositAmount());
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("currencyId", playerCreditModel.getCurrencyId());
		int resultInt = query.executeUpdate();
		if (resultInt > 0) {
			result = "Success";
		} else {
			result = "Fail";
		}
		return result;
	}

	@Override
	public String withdrawFromPlayer(PlayerDebitModel playerDebitModel, Integer businessPartnerId) throws Exception {
		String result = "";
		Session session = null;
		session = sessionFactory.getCurrentSession();

		session.save(playerDebitModel);
		String sqlStr = "update business_partner_balances set total_amount = total_amount + :withdrawAmount" + " where business_partner_id = :businessPartnerId "
				+ " and currency_id =  :currencyId ";

		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("withdrawAmount", playerDebitModel.getWithdrawAmount());
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("currencyId", playerDebitModel.getCurrencyId());
		int resultInt = query.executeUpdate();
		if (resultInt > 0) {
			result = "Success";
		} else {
			result = "Fail";
		}
		return result;
	}

	@Override
	public PlayersModel getPalyerById(Integer playerId) {
		System.out.println("PalyerById --> " + playerId);
		Session session = null;
		PlayersModel playersModel = new PlayersModel();
		session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(PlayersModel.class);
		cr.add(Restrictions.eq("id", playerId));
		playersModel = (PlayersModel) cr.uniqueResult();
		System.out.println("after query result");

		return playersModel;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> displayPlayers(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<String> Playershow = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select * from players players, players_business_partners_map pbpm"
				+ " where players.players_id = pbpm.player_id and pbpm.business_partner_id = :businessPartnerId");
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		Playershow = query.list();

		return Playershow;
	}

	public List<String> displayPlayersCreditInfo(String username, Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<String> playerCreditInfo = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session
				.createSQLQuery("select players.players_username, curr.currency_name, credit.deposit_amount, to_char(credit.created_on, 'DD/MM/YYYY HH24:MI:SS') as created_on"
						+ " from players players, player_credits credit, currency curr"
						+ " where credit.player_id = players.players_id"
						+ " and credit.currency_id = curr.currency_id" + " and credit.business_partner_id = :businessPartnerId");

		query.setParameter("businessPartnerId", businessPartnerId);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		playerCreditInfo = query.list();

		return playerCreditInfo;
	}

	public List<String> displayPlayersDebitInfo(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<String> playerDebitInfo = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session
				.createSQLQuery("select players.players_username, curr.currency_name, debit.withdraw_amount, to_char(debit.created_on, 'DD/MM/YYYY HH24:MI:SS') as created_on"
						+ " from players players, player_debits debit, currency curr"
						+ " where debit.player_id = players.players_id"
						+ " and debit.currency_id = curr.currency_id" + " and debit.business_partner_id = :businessPartnerId");
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		playerDebitInfo = query.list();

		return playerDebitInfo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> showallvalues(int id) {
		// TODO Auto-generated method stub

		List<String> Playerallvalueshow = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select * from Players where players_id =" + id + ";");
		// query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		Playerallvalueshow = query.list();

		System.out.println(Playerallvalueshow);
		return Playerallvalueshow;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String updateplayer(PlayersModel player, String username) {

		// TODO Auto-generated method stub
		List<String> Playerlistcheck = new ArrayList<String>();
		Session session = null;
		Playerlistcheck.removeAll(Playerlistcheck);
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("Select * from Players where lower(Players_username) = :username");
		query.setParameter("username", username.toLowerCase());
		Playerlistcheck = query.list();
		String result = "";
		if (Playerlistcheck.isEmpty()) {
			session.update(player);
			result = "Success";
		} else {
			result = "Fail";
		}

		return result;
	}

	@Override
	public String delete(PlayersModel player) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.getCurrentSession();
		session.delete(player);
		return "Success";
	}

	// **************************************deposit to
	// Ficha********************************************
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getpaymentvalues() {
		// TODO Auto-generated method stub
		List<String> Paymentvalues = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select payment_method from payment");
		// query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		Paymentvalues = query.list();

		return Paymentvalues;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCurrencyList() {
		List<String> currencyList = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select currency_name from currency");

		currencyList = query.list();
		return currencyList;
	}

	@Override
	public List<String> getCountryNameList() {
		List<String> countryNameList = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select country_name from countries where risky_country = false order by country_name ");
		// query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		countryNameList = query.list();

		return countryNameList;
	}

	@Override
	public Map<String, String> getCountryPhoneCodeMap() {

		Map<String, String> countryPhoneCodeList = new HashMap<String, String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		List objectList = (List) session.createSQLQuery(
				"select country_phone_code,  concat_ws(' - ', country_phone_code, country_name ) from countries where risky_country = false order by country_name")
				.list();
		for (Object object : objectList) {

			Object objectArray[] = (Object[]) object;
			String key = (String) objectArray[0];
			String value = (String) objectArray[1];
			countryPhoneCodeList.put(key, value);
		}

		return countryPhoneCodeList;
	}

	@Override
	public Map<Integer, String> getBpTotalBalances(Integer businessPartnerId) {

		Map<Integer, String> businessPartnerTotalBal = new HashMap<Integer, String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		String sqlStr = "select curr.currency_id, concat_ws(' - ', curr.currency_name, bpb.total_amount ) from business_partner_balances bpb, currency curr"
				+ " where business_partner_id = :businessPartnerId" + " and curr.currency_id = bpb.currency_id";
		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("businessPartnerId", businessPartnerId);

		List objectList = (List) query.list();
		for (Object object : objectList) {

			Object objectArray[] = (Object[]) object;
			Integer key = (Integer) objectArray[0];
			String value = (String) objectArray[1];
			businessPartnerTotalBal.put(key, value);
		}

		return businessPartnerTotalBal;
	}

	@Override
	public Map<Integer, String> getPlayersList(Integer businessPartnerId) {

		Map<Integer, String> playersList = new HashMap<Integer, String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		String sqlStr = "select players_id, concat_ws(' - ', players_username, players_currency) as players_username from players players, players_business_partners_map pbpm"
				+ " where players.players_id = pbpm.player_id" + " and pbpm.business_partner_id = :businessPartnerId order by players_username";
		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("businessPartnerId", businessPartnerId);

		List objectList = (List) query.list();
		for (Object object : objectList) {

			Object objectArray[] = (Object[]) object;
			Integer key = (Integer) objectArray[0];
			String value = (String) objectArray[1];
			playersList.put(key, value);
		}

		return playersList;
	}

	@Override
	public Map<Integer, String> getPlayersListForAgents(Integer businessPartnerId, Integer agentType) {

		Map<Integer, String> playersList = new HashMap<Integer, String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		String sqlStr = " select players.players_id, concat_ws(' - ', players.players_username, players.players_currency) as players_username "
				+ " from players players, players_business_partners_map pbpm" + " where players.players_id = pbpm.player_id"
				+ " and pbpm.business_partner_id = :businessPartnerId ";

		if (agentType == Constants.MAIN_AGENT) {
			sqlStr = sqlStr + " union " + "select players.players_id, concat_ws(' - ', players.players_username, players.players_currency) as players_username "
					+ " from players players, players_business_partners_map pbpm , agent_sub_agent_map agentSubAgentMap " + " where players.players_id = pbpm.player_id "
					+ " and pbpm.business_partner_id = agentSubAgentMap.sub_agent_id" + " and agentSubAgentMap.main_agent_id = :businessPartnerId ";
		}

		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("businessPartnerId", businessPartnerId);

		List objectList = (List) query.list();
		for (Object object : objectList) {

			Object objectArray[] = (Object[]) object;
			Integer key = (Integer) objectArray[0];
			String value = (String) objectArray[1];
			playersList.put(key, value);
		}

		return playersList;
	}

	@Override
	public List<String> getPlayersListByCurrency(Integer currencyId, Integer businessPartnerId) {

		List<String> playersList = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		String sqlStr = "select players_id, players_email from players players, players_business_partners_map pbpm" + " where players.players_id = pbpm.player_id"
				+ " and pbpm.business_partner_id = :businessPartnerId"
				+ " and players.players_currency = (select currency_name from currency where currency_id = :currencyId)";
		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("currencyId", currencyId);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

		playersList = (List) query.list();
		/*
		 * for(Object object : objectList) {
		 * 
		 * Object objectArray[] = (Object[])object; Integer key =
		 * (Integer)objectArray[0]; String value = (String)objectArray[1];
		 * playersList.put(key, value); }
		 */

		return playersList;
	}

	@Override
	public boolean validateDepositAmount(PlayerCreditModel playerCreditModel, Integer businessPartnerId) {

		Session session = null;
		session = sessionFactory.getCurrentSession();

		String sqlStr = " select total_amount from business_partner_balances" + " where business_partner_id = :businessPartnerId" + " and currency_id = :currencyId";
		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("currencyId", playerCreditModel.getCurrencyId());

		Double totalAmount = (Double) query.uniqueResult();

		if (totalAmount == 0.0) {
			return false;
		} else if (totalAmount < playerCreditModel.getDepositAmount()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean validatePlayerCurrencyForDepositWithdraw(Integer playerId, Integer currencyIdUsedforDepositWithdraw) {

		Session session = null;

		session = sessionFactory.getCurrentSession();

		String sqlStr = "select curr.currency_id from players, currency curr" + " where curr.currency_name = players.players_currency"
				+ "	and players.players_id = :playerId";

		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("playerId", playerId);

		Integer currencyId = (Integer) query.uniqueResult();

		if (currencyId == currencyIdUsedforDepositWithdraw) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String insertDepositToFicha(DepositToFicha depositToFicha) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.getCurrentSession();

		Query query = session
				.createQuery("select count(*) from BusinessPartnerBalancesModel bpbalances where bpbalances.businessPartnerId=:businessPartnerId and bpbalances.currencyId=:currencyId");
		query.setParameter("businessPartnerId", depositToFicha.getBusinessPartnerId());
		query.setParameter("currencyId", depositToFicha.getCurrencyId());
		Long count = (Long) query.uniqueResult();

		if (count == 0) {
			BusinessPartnerBalancesModel bpbalances = new BusinessPartnerBalancesModel();
			bpbalances.setBusinessPartnerId(depositToFicha.getBusinessPartnerId());
			bpbalances.setCurrencyId(depositToFicha.getCurrencyId());
			bpbalances.setTotalAmount(0.0);
			bpbalances.setTotalCommissionAmount(0.0);
			bpbalances.setTotalCreditAmount(0.0);
			bpbalances.setCreatedOn(new Date());
			bpbalances.setUpdatedOn(new Date());

			session.save(bpbalances);
		}

		SQLQuery sqlQuery = session.createSQLQuery("select casino_manager_id, commission_percentage from business where business_id = :businessPartnerId");
		sqlQuery.setParameter("businessPartnerId", depositToFicha.getBusinessPartnerId());
		List queryResult = sqlQuery.list();

		if (queryResult.size() == 1) {
			for (Object object : queryResult) {

				Object objectArray[] = (Object[]) object;
				Integer casinoManagerId = (Integer) objectArray[0];
				Double commissionPaercentage = (Double) objectArray[1];

				if (commissionPaercentage == null) {
					commissionPaercentage = 0.0;
				}

				depositToFicha.setCasinoManagerId(casinoManagerId);
				depositToFicha.setStatusId(Constants.STATUS_PENDING);
				depositToFicha.setDepositType(Constants.DEPOSIT_AMOUNT_APPROVAL);
				depositToFicha.setRequestSource(Constants.FICHA_REGISTRATION_SELF);
				depositToFicha.setCommissionAmount(getPercentageValue(depositToFicha.getAmount(), commissionPaercentage));
				depositToFicha.setCreatedOn(new Date());
				depositToFicha.setUpdatedOn(new Date());
				session.save(depositToFicha);
				return "Success";
			}
		} else {
			return "Fail";
		}

		return "Fail";
	}

	@Override
	public Map<Integer, String> currencyValues() {
		Map<Integer, String> currencyIdValueMap = new HashMap<Integer, String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		List objectList = (List) session.createQuery("select id,  name from Currency").list();

		for (Object object : objectList) {

			Object objectArray[] = (Object[]) object;
			Integer key = (Integer) objectArray[0];
			String value = (String) objectArray[1];
			currencyIdValueMap.put(new Integer(key), value);
		}

		return currencyIdValueMap;
	}

	@Override
	public Map<Integer, String> paymentValues() {
		// TODO Auto-generated method stub
		Map<Integer, String> paymentTypeIdValueMap = new HashMap<Integer, String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		List objectList = (List) session.createQuery("select id,  name from Payment").list();
		for (Object object : objectList) {

			Object objectArray[] = (Object[]) object;
			Integer key = (Integer) objectArray[0];
			String value = (String) objectArray[1];
			paymentTypeIdValueMap.put(new Integer(key), value);
		}

		return paymentTypeIdValueMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> depositToFichaShowValues(DepositToFicha d1) {
		// TODO Auto-generated method stub
		List<String> depositToFichaValues = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session
				.createSQLQuery("select bal.balances_id, bal.business_partner_id, bal.casino_manager_id, bal.currency, bal.payment, cas.casino_username, bal.status, buss.business_firstname, pay.payment_name, curr.currency_name, bal.amount, bal.created_on"
						+ " from balances as bal, business as buss,  casinomanager as cas, currency as curr, payment as pay"
						+ " where  buss.business_id = bal.business_partner_id"
						+ " and  cas.casino_id = bal.casino_manager_id"
						+ " and  curr.currency_id = bal.currency"
						+ " and  pay.payment_id = bal.payment" + " and bal.business_partner_id = :businessPartnerId");
		query.setParameter("businessPartnerId", d1.getBusinessPartnerId());
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		depositToFichaValues = query.list();
		// System.out.println("depositToFichaValues = "+depositToFichaValues);
		return depositToFichaValues;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> businessPartnerCurrencyDetails(DepositToFicha df) {
		// TODO Auto-generated method stub
		List<String> businessPartnerCurrencyDetails = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = session
				.createSQLQuery("select currency_name, total_amount, total_commission_amount, total_credit_amount , (total_amount - total_credit_amount) as total_balance from business_partner_balances bpb, currency curr"
						+ " where  bpb.business_partner_id = :businessPartnerId" + " and bpb.currency_id = curr.currency_id");
		query.setParameter("businessPartnerId", df.getBusinessPartnerId());
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		businessPartnerCurrencyDetails = query.list();
		System.out.println("businessPartnerCurrencyDetails = " + businessPartnerCurrencyDetails);
		return businessPartnerCurrencyDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> businessPartnerInformationDetails(DepositToFicha df) {
		// TODO Auto-generated method stub
		List<String> businessPartnerInformationDetails = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from business where business_id = :businessPartnerId");
		query.setParameter("businessPartnerId", df.getBusinessPartnerId());
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		businessPartnerInformationDetails = query.list();
		// System.out.println("businessPartnerInformationDetails = "+businessPartnerInformationDetails);
		return businessPartnerInformationDetails;
	}

	@Override
	public List<String> getPlayersByUserName(String playerUserName, Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<String> playerList = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		SQLQuery query = session.createSQLQuery("select * from players players, players_business_partners_map pbpm" + " where players.players_id = pbpm.player_id "
				+ " and pbpm.business_partner_id = :businessPartnerId" + " and players.players_username like  :playerUserName");
		// query.setParameter("casinoManagerId", casinoManagerId);
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("playerUserName", "%" + playerUserName + "%");
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		playerList = query.list();
		// System.out.println("  CheckPlayerFromUserName "+CheckPlayerFromUserName);
		return playerList;

	}

	@Override
	public List<String> getPlayersByUserNameForAgents(String playerUserName, Integer businessPartnerId, Integer agentType) {
		// TODO Auto-generated method stub
		List<String> playerList = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();

		String sqlQuery = "select buss.business_username, players.players_id, players.players_firstname, players.players_lastname, players.players_username, players.players_address, players.players_country, players.city, players.players_postcode, players.players_email"
				+ " from players players, players_business_partners_map pbpm, business buss"
				+ " where players.players_id = pbpm.player_id"
				+ " and buss.business_id = pbpm.business_partner_id"
				+ " and pbpm.business_partner_id = :businessPartnerId"
				+ " and players.players_username like :playerUserName";

		if (agentType == Constants.MAIN_AGENT) {

			sqlQuery = sqlQuery
					+ " union "
					+ "select buss.business_username, players.players_id, players.players_firstname, players.players_lastname, players.players_username, players.players_address, players.players_country, players.city, players.players_postcode, players.players_email"
					+ " from players players, players_business_partners_map pbpm , business buss, agent_sub_agent_map agentSubAgentMap"
					+ " where players.players_id = pbpm.player_id" + " and pbpm.business_partner_id = agentSubAgentMap.sub_agent_id"
					+ " and buss.business_id = agentSubAgentMap.sub_agent_id" + " and agentSubAgentMap.main_agent_id = :businessPartnerId"
					+ " and players.players_username like :playerUserName";

		}

		SQLQuery query = session.createSQLQuery(sqlQuery);
		// query.setParameter("casinoManagerId", casinoManagerId);
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("playerUserName", "%" + playerUserName + "%");
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		playerList = query.list();
		// System.out.println("  CheckPlayerFromUserName "+CheckPlayerFromUserName);
		return playerList;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getPlayersByCurrencyName(Integer currencyId, Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<String> playerCurrencyList = new ArrayList<String>();

		Session session = null;
		session = sessionFactory.getCurrentSession();

		String sqlStr = "select players_id, players_username from players players, players_business_partners_map pbpm" + " where players.players_id = pbpm.player_id"
				+ " and pbpm.business_partner_id = :businessPartnerId"
				+ " and players.players_currency = (select currency_name from currency where currency_id = :currencyId)";
		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("currencyId", currencyId);

		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		playerCurrencyList = query.list();
		System.out.println("playerCurrencyList = " + playerCurrencyList);

		return playerCurrencyList;
	}

	@Override
	public Integer isAgent(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String resetPasswordOnActivation(ResetPasswordOnActivation resetPasswordOnActivation) {

		String resultStr = "";
		Session session = null;
		session = sessionFactory.getCurrentSession();

		Query query = session.createSQLQuery("select count(*) from business " + "where lower(business_username) = :businessPartnerUserName "
				+ "and bp_activation_code = :activationCode " + "and bp_activated_status = false");

		query.setParameter("businessPartnerUserName", resetPasswordOnActivation.getUserName().toLowerCase());
		query.setParameter("activationCode", resetPasswordOnActivation.getActivationCode());
		BigInteger count = (BigInteger) query.uniqueResult();

		if (count.intValue() == 1) {

			query = session.createSQLQuery("update business set bp_activated_status = true, " + "business_password = :resetPassword, " + "business_salt = :salt "
					+ "where lower(business_username) = :businessPartnerUserName " + "and bp_activation_code = :activationCode");

			query.setParameter("businessPartnerUserName", resetPasswordOnActivation.getUserName().toLowerCase());
			query.setParameter("activationCode", resetPasswordOnActivation.getActivationCode());
			query.setParameter("resetPassword", resetPasswordOnActivation.getResetPassword());
			query.setParameter("salt", resetPasswordOnActivation.getSalt());
			int result = query.executeUpdate();
			if (result > 0) {
				resultStr = "Success";
			} else {
				resultStr = "Fail";
			}
		} else {
			resultStr = "Fail";
		}

		return resultStr;

	}

	@Override
	public String changePassword(ChangePassword changePassword, Integer businessPartnerId) {

		String resultStr = "";
		Session session = null;
		session = sessionFactory.getCurrentSession();

		Query query = session.createSQLQuery("select business_password, business_salt from business " + "where business_id = :businessPartnerId ");
		query.setParameter("businessPartnerId", businessPartnerId);

		List<String> passwordVal = query.list();

		if (passwordVal.size() == 1) {

			for (Object object : passwordVal) {

				Object objectArray[] = (Object[]) object;
				String oldPassword = (String) objectArray[0];
				String oldSalt = (String) objectArray[1];
				byte[] saltInByte;
				try {
					saltInByte = Hex.decodeHex(oldSalt.toCharArray());
				} catch (DecoderException e) {
					System.out.println("Error decoding password salt");
					System.out.println(e);
					throw new RuntimeException(e);
				}

				if (oldPassword.equals(createPasswordHash(changePassword.getOldPassword(), saltInByte))) {
					query = session.createSQLQuery("update business set business_password = :newPassword, " + "business_salt = :salt "
													+ "where business_id = :businessPartnerId ");

					query.setParameter("businessPartnerId", businessPartnerId);
					query.setParameter("newPassword", changePassword.getNewPassword());
					query.setParameter("salt", changePassword.getSalt());

					int result = query.executeUpdate();

					if (result > 0) {
						resultStr = "Success";
					} else {
						resultStr = "Fail";
					}
				} else {
					resultStr = "Fail";
				}
			}
		} else {
			resultStr = "Fail";
		}

		return resultStr;

	}

	@Override
	public String checkUserName(ForgotPasswordCredentials forgotPasswordCredentials) {
		// TODO Auto-generated method stub

		Session session = null;
		List<String> businessCheckUserName = new ArrayList<String>();
		session = sessionFactory.getCurrentSession();

		businessCheckUserName.removeAll(businessCheckUserName);

		SQLQuery query = session.createSQLQuery("select business_email from business where lower(business_username) = :userName");

		query.setParameter("userName", forgotPasswordCredentials.getUserName().toLowerCase());

		businessCheckUserName = query.list();
		String result = "" + businessCheckUserName.get(0);
		System.out.println("" + result);
		if (businessCheckUserName.isEmpty()) {
			return "Fail";
		} else {
			return result;
		}

	}

	/**
	 * Creates the password hash.
	 *
	 * @param password
	 *            the password
	 * @param salt
	 *            the salt
	 * @return the string
	 */
	private String createPasswordHash(String password, byte[] salt) {
		byte[] hash;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			digest.update(salt);
			digest.update(password.getBytes());
			hash = digest.digest();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
			System.out.println("Error generating secure password");
			throw new UnsupportedOperationException(e);
		}
		return Hex.encodeHexString(hash);
	}

	private Double getPercentageValue(Double amount, Double percentage) {

		Double percentageAmount = (amount * percentage) / 100;
		return percentageAmount;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> updateActivationCode(ForgotPasswordCredentials forgotPasswordCredentials, BusinessPartnerModel businessPartner) {
		// TODO Auto-generated method stub

		List<Integer> businessPartnerInformation = new ArrayList<Integer>();
		Session session = null;
		String s1 = "";
		session = sessionFactory.getCurrentSession();
		String sqlStr = "select business_id from business where lower(business_username) = :userName";
		SQLQuery query = session.createSQLQuery(sqlStr);
		query.setParameter("userName", forgotPasswordCredentials.getUserName().toLowerCase());
		businessPartnerInformation = query.list();

		query = session.createSQLQuery("update business set bp_activation_code = :activationCode where lower(business_username) = :businessPartnerUserName");
		query.setParameter("activationCode", businessPartner.getActivationCode());
		query.setParameter("businessPartnerUserName", forgotPasswordCredentials.getUserName().toLowerCase());
		System.out.println("setting Activation code" + businessPartner.getActivationCode());
		int result = query.executeUpdate();
		System.out.println("businessPartnerInformation = " + businessPartnerInformation + " " + result);
		return businessPartnerInformation;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HashMap> getBusinessPartnersForActivation(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<Integer> items = new ArrayList<Integer>();
		items.add(businessPartnerId);
		List<HashMap> businessPartnersListForActivation = new ArrayList<HashMap>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = null;
		query = session.createSQLQuery("select business_username, bp_activation_code, business_email from  business where business_id in (:businessPartnetsIds)");
		query.setParameterList("businessPartnetsIds", items);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		businessPartnersListForActivation = query.list();
		System.out.println("businessPartnersListForActivation = " + businessPartnersListForActivation);
		return businessPartnersListForActivation;

	}

	@Override
	public List<String> depositWithdrwalPlayerInformation(int players_id) {
		// TODO Auto-generated method stub
		List<String> depositWithdrwalPlayerInformation = new ArrayList<String>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = null;
		query = session.createSQLQuery("select players_firstname, players_lastname, players_username, players_currency, players_country, players_address, players_phone, players_email, players_sscnum from players where players_id =:players_id");
		query.setParameter("players_id", players_id);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		depositWithdrwalPlayerInformation = query.list();
		return depositWithdrwalPlayerInformation;
	}
	
	
	
	@Override
	public String forgotPasswordOnActivationAction(ForgotPasswordOnActivation forgotPasswordOnActivation) {
		// TODO Auto-generated method stub

		// System.out.println("userName ="+forgotPasswordOnActivation.getUserName()+"  activationcode = "+forgotPasswordOnActivation.getActivationCode());

		String resultStr = "";
		Session session = null;
		session = sessionFactory.getCurrentSession();

		Query query = session
				.createSQLQuery("select count(*) from business where lower(business_username) = :businessPartnerUserName and bp_activation_code = :activationCode ");
		query.setParameter("businessPartnerUserName", forgotPasswordOnActivation.getUserName().toLowerCase());
		query.setParameter("activationCode", forgotPasswordOnActivation.getVerificationCode());
		BigInteger count = (BigInteger) query.uniqueResult();
		if (count.intValue() == 1) {

			query = session
					.createSQLQuery("update business set business_password = :resetPassword, business_salt = :salt where lower(business_username) = :businessPartnerUserName and bp_activation_code = :activationCode");
			query.setParameter("businessPartnerUserName", forgotPasswordOnActivation.getUserName().toLowerCase());
			query.setParameter("activationCode", forgotPasswordOnActivation.getVerificationCode());
			query.setParameter("resetPassword", forgotPasswordOnActivation.getResetPassword());
			query.setParameter("salt", forgotPasswordOnActivation.getSalt());

			int result = query.executeUpdate();

			query = session.createSQLQuery("update business set bp_activation_code = :activationCode where lower(business_username) = :businessPartnerUserName");
			query.setParameter("activationCode", UUID.randomUUID().toString());
			query.setParameter("businessPartnerUserName", forgotPasswordOnActivation.getUserName().toLowerCase());

			result = query.executeUpdate();

			if (result > 0) {
				resultStr = "Success";
			} else {
				resultStr = "Fail";
			}
		} else {
			resultStr = "Fail";
		}

		return resultStr;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List getPlayerNamesDetails(String playerName, Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<Integer> getPlayerNamesDetails = new ArrayList<Integer>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = null;
		query = session.createSQLQuery(" select players.players_username "
				+ " from players players, players_business_partners_map pbpm" + " where players.players_id = pbpm.player_id"
				+ " and pbpm.business_partner_id = :businessPartnerId and players.players_username like :playerUserName");
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setParameter("playerUserName",  playerName + "%");
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		getPlayerNamesDetails = query.list();
		
		/*if(getplayerNamesDetails.size()==0)
		{
		
			query1 = session.createSQLQuery(" select players.players_username "
					+ " from players players, players_business_partners_map pbpm" + " where players.players_id = pbpm.player_id"
					+ " and pbpm.business_partner_id = :businessPartnerId ");
			query1.setParameter("businessPartnerId", businessPartnerId);
			query1.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			getplayerNamesDetails = query1.list();
		}*/
		
		return getPlayerNamesDetails;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List getAllPlayerName(Integer businessPartnerId) {
		// TODO Auto-generated method stub
		List<Integer> getAllPlayerName = new ArrayList<Integer>();
		Session session = null;
		session = sessionFactory.getCurrentSession();
		SQLQuery query = null;
		query = session.createSQLQuery(" select players.players_username "
				+ " from players players, players_business_partners_map pbpm" + " where players.players_id = pbpm.player_id"
				+ " and pbpm.business_partner_id = :businessPartnerId ");
		query.setParameter("businessPartnerId", businessPartnerId);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		getAllPlayerName = query.list();
		return getAllPlayerName;
	}

	

}
