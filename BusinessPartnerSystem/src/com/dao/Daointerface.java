package com.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.model.BusinessPartnerModel;
import com.model.ChangePassword;
import com.model.DepositToFicha;
import com.model.ForgotPasswordCredentials;
import com.model.ForgotPasswordOnActivation;
import com.model.PlayerCreditModel;
import com.model.PlayerDebitModel;
import com.model.PlayersModel;
import com.model.ResetPasswordOnActivation;

public interface Daointerface {

	public List<HashMap> login(String uname, String pass);
	
	public List<String> getSubAgentsByUserName(String subAgentUserName,	Integer agentBusinessPartnerId);
	
	public String createSubAgent(BusinessPartnerModel businessPartnerModel, Integer agentBusinessPartnerId);

	public String createBusinessPartner(BusinessPartnerModel businessPartnerModel, String username);

	boolean isPlayerUserNameAlreadyExists(PlayersModel player, Integer businessPartnerId) throws Exception;
	
	public String addExistingPlayerForBP(PlayersModel player, Integer businessPartnerId);
	
	public boolean isPlayerExistsForCurrentBP(PlayersModel player, Integer businessPartnerId);
	
	public boolean isPlayerAlreadyExistsForAgents(PlayersModel player, Integer businessPartnerId);
	
	public String insertplayer(PlayersModel player, String username, Integer businessPartnerId) throws Exception;

	public String depositToPlayer(PlayerCreditModel playerCreditModel, Integer businessPartnerId) throws Exception;
	
	public String withdrawFromPlayer(PlayerDebitModel playerDebitModel, Integer businessPartnerId) throws Exception;

	public PlayersModel getPalyerById(Integer playerId);

	public List<String> displayPlayers(Integer businessPartnerId);

	public List<String> showallvalues(int id);

	public String updateplayer(PlayersModel player, String username);

	public String delete(PlayersModel player);

	public List<String> getpaymentvalues();

	public List<String> getCurrencyList();

	public List<String> getCountryNameList();

	public Map<String, String> getCountryPhoneCodeMap();

	public Map<Integer, String> getBpTotalBalances(Integer businessPartnerId);

	public Map<Integer, String> getPlayersList(Integer businessPartnerId);
	
	public Map<Integer, String> getPlayersListForAgents(Integer businessPartnerId, Integer agentType);

	public List<String> getPlayersListByCurrency(Integer currencyId, Integer businessPartnerId);

	public boolean validateDepositAmount(PlayerCreditModel playerCreditModel, Integer businessPartnerId);

	public boolean validatePlayerCurrencyForDepositWithdraw(Integer playerId, Integer currencyIdUsedforDepositWithdraw);

	public List<String> displayPlayersCreditInfo(String username, Integer businessPartnerId);
	
	public List<String> displayPlayersDebitInfo(Integer businessPartnerId);

	public String insertDepositToFicha(DepositToFicha df);

	public Map<Integer, String> currencyValues();

	public Map<Integer, String> paymentValues();

	public List<String> depositToFichaShowValues(DepositToFicha d1);

	public List<String> businessPartnerCurrencyDetails(DepositToFicha df);

	public List<String> businessPartnerInformationDetails(DepositToFicha df);

	public List<String> getPlayersByUserName(String playerUserName, Integer businessPartnerId);
	
	public List<String> getPlayersByUserNameForAgents(String playerUserName, Integer businessPartnerId, Integer agentType);

	public List<String> getPlayersByCurrencyName(Integer currencyValue, Integer businessPartnerId);
	
	public Integer isAgent(Integer businessPartnerId);
	
	public String resetPasswordOnActivation(ResetPasswordOnActivation resetPasswordOnActivation);
	
	public String changePassword(ChangePassword changePassword, Integer businessPartnerId);

	public String checkUserName(
			ForgotPasswordCredentials forgotPasswordCredentials);

	

	public List<HashMap> getBusinessPartnersForActivation(
			Integer integer);

	public List<Integer> updateActivationCode(ForgotPasswordCredentials forgotPasswordCredentials, BusinessPartnerModel businessPartner);

	public String forgotPasswordOnActivationAction(
			ForgotPasswordOnActivation forgotPasswordOnActivation);

	public List getPlayerNamesDetails(String playerName, Integer businessPartnerId);

	public List<String> getAllPlayerName(Integer businessPartnerId);

	public List<String> depositWithdrwalPlayerInformation(int players_id);



}
