package com.Actioncontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.form.validator.BusinessPartnerFormValidation;
import com.form.validator.CashierFormValidation;
import com.form.validator.ChangePasswordValidation;
import com.form.validator.DepositToFichaFormValidation;
import com.form.validator.ForgotPasswordEmailFormActivation;
import com.form.validator.ForgotPasswordFormValidation;
import com.form.validator.PlayersDepositFormValidation;
import com.form.validator.PlayersFormValidation;
import com.form.validator.PlayersWithdrawFormValidation;
import com.form.validator.ResetPasswordFormValidation;
import com.form.validator.SubAgentFormValidation;
import com.form.validator.WithdrawDepositFormValidations;
import com.model.AddExistingPlayerModel;
import com.model.BusinessPartnerModel;
import com.model.CashierModel;
import com.model.ChangePassword;
import com.model.DepositToFicha;
import com.model.DepositWithdrawl;
import com.model.ForgotPasswordCredentials;
import com.model.ForgotPasswordOnActivation;
import com.model.Login;
import com.model.PlayerCreditModel;
import com.model.PlayerDebitModel;
import com.model.PlayersModel;
import com.model.ResetPasswordOnActivation;
import com.service.Serviceinterface;

@Controller
public class BusinessPartnerController {

	@Autowired
	Serviceinterface serviceinter;
	
	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/login")
	public ModelAndView hello(@ModelAttribute("login") Login l1, Model model) {
		// System.out.println("inside login");
		return new ModelAndView("Login");
	}

	
	
	/*
	 * 
	 * bpDepositToFichaInsert
	 * subAgentInsert
	 * insertNewPlayer
	 * addExistingPlayer
	 * withdrawlToPlayerOperation
	 * depositToPlayerOperation
	 *
	 */
	
	
	
	@RequestMapping(value="/depositWithdrawl")
	public ModelAndView depositWithdrawl(@ModelAttribute("depositWithdrawl") DepositWithdrawl depositwithdrawl,Model model,HttpServletRequest req, BindingResult result)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
			return new ModelAndView("depositWithdrawl");
	}
	
	
	//playerDetail
	@RequestMapping(value="/playerDetail")
	public ModelAndView playerDetail(Model model)
	{
		System.out.println("playerDetails function");
		return new ModelAndView("playerDetail");
	}
	
	@RequestMapping(value="/depositWithdrawlOperation")
	public ModelAndView depositWithdrawlOperation(@ModelAttribute("depositWithdrawl") DepositWithdrawl depositwithdrawl,Model model,HttpServletRequest req, BindingResult result)
	{
		WithdrawDepositFormValidations withdrawDepositFormValidation = new WithdrawDepositFormValidations();
		withdrawDepositFormValidation.validate(depositwithdrawl, result);
		HttpSession httpSession = req.getSession(false);
		if(result.hasErrors())
		{
			httpSession = req.getSession();
			return new ModelAndView("depositWithdrawl");
		}
		
		if(depositwithdrawl.getAction().equalsIgnoreCase("D"))
		{
			System.out.println("Deposit done");
		}
		else
		{
			System.out.println("Withdrwal done");
		}
		return new ModelAndView("depositWithdrawl");
	}
	
	@RequestMapping(value="cashier")
	public ModelAndView Cashier()
	{
		return new ModelAndView("Cashier");
	}
	
	@RequestMapping(value="cashierInsert")
	public ModelAndView cashierInsert(@ModelAttribute("insertCashier")CashierModel cashierModel,Model model,HttpServletRequest req)
	{
		HttpSession httpSession = req.getSession(false);
		List<String> currencyList = new ArrayList<String>();
		currencyList = serviceinter.getCurrencyList();
		model.addAttribute("currencyList", currencyList);

		List<String> countryNameList = (List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);

		Map<String, String> countryPhoneCodeList = (Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		return new ModelAndView("insertCashier");
	}
	
	
	@RequestMapping(value="/insertCashier")
	public ModelAndView insertCashier(@ModelAttribute("insertCashier")CashierModel cashierModel, Model model, HttpServletRequest req, BindingResult result)
	{
  
		HttpSession httpSession = req.getSession(false);
		CashierFormValidation cashierFormValidation = new CashierFormValidation();
		cashierFormValidation.validate(cashierModel, result);

		if (result.hasErrors()) {
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "Y");
			return new ModelAndView("insertCashier");
		}	
		
		return new ModelAndView("insertCashier");
	}
	
	@RequestMapping(value="/depositWithdrwalPlayer")
	@ResponseBody
	public List depositWithdrwalPlayerInformation(int players_id)
	{
		System.out.println("players_id = "+players_id);
		List<String> depositWithdrwalPlayerInformation = serviceinter.depositWithdrwalPlayerInformation(players_id);
		/*System.out.println(" depositWithdrwalPlayerInformation = "+depositWithdrwalPlayerInformation);*/
		return depositWithdrwalPlayerInformation;
	}
	
	
	@RequestMapping(value="/withdrawlToPlayerOperation")
	public ModelAndView withdrawlToPlayerOperation(@ModelAttribute("withdrawFromPlayer") PlayerDebitModel playerDebitModel, Model model, HttpServletRequest req)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> playerDebitInfo = serviceinter.displayPlayersDebitInfo(businessPartnerId);
		
	/*	PagedListHolder pagedListHolder = new PagedListHolder(playerDebitInfo);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);*/
		
		httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "N");
		
		Map<Integer,String> playersList = new HashMap<Integer,String>();
		
		Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
		Integer agentType = (Integer) httpSession.getAttribute("agentType");
		
		if (isAgent) {
			playersList = serviceinter.getPlayersListForAgents(businessPartnerId, agentType);
		}else{
			playersList = serviceinter.getPlayersList(businessPartnerId);
		}
		

		Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

		model.addAttribute("playersList", playersList);
		model.addAttribute("bpTotalBalance", bpTotalBalance);

		return new ModelAndView("withdrawlToPlayerOperation");
	}
	
	
	
	@RequestMapping(value = "/insertFromPlayerOperation")
	public ModelAndView withdrawFromPlayerCreate(@ModelAttribute("withdrawFromPlayer") PlayerDebitModel playerDebitModel, BindingResult result, Model model,
			HttpServletRequest req) {
		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}
			
			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
			List<String> playerDebitInfo = new ArrayList<String>();
			
			PagedListHolder pagedListHolder = new PagedListHolder(playerDebitInfo);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);

			Map<Integer, String> playersList = serviceinter.getPlayersList(businessPartnerId);
			Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

			model.addAttribute("playersList", playersList);
			model.addAttribute("bpTotalBalance", bpTotalBalance);

			PlayersWithdrawFormValidation playersWithdrawFormValidation = new PlayersWithdrawFormValidation();
			playersWithdrawFormValidation.validate(playerDebitModel, result);

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "Y");
				return new ModelAndView("withdrawlToPlayerOperation");
			}
			
			playerDebitModel.setWithdrawAmount(Double.parseDouble(playerDebitModel.getAmountStr()));
			
			boolean isPlayerCurrencyValid = serviceinter.validatePlayerCurrencyForDepositWithdraw(playerDebitModel.getPlayerId(), playerDebitModel.getCurrencyId());
			if (!isPlayerCurrencyValid) {
				result.rejectValue("currencyId", "error.invalid.currencyId", "Selected Player has different currency than selected credit account.");
			}

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "Y");
				return new ModelAndView("withdrawFromPlayer");
			}
			httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "N");
			List<Map<String, String>> errorList = serviceinter.withdrawFromPlayer(playerDebitModel, businessPartnerId);

			List errorListForDisplay = new ArrayList();
			//errorListForDisplay.add("error occurred");
			for (Map<String, String> errorMap : errorList) {
				errorListForDisplay.add(errorMap.get("error_message"));
				
			}
			

			if (errorListForDisplay.isEmpty()) {
				//model.addAttribute("message", "Inserted Successfully");
				
				playersList = serviceinter.getPlayersList(businessPartnerId);
				bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

				model.addAttribute("playersList", playersList);
				model.addAttribute("bpTotalBalance", bpTotalBalance);
				
				playerDebitInfo = serviceinter.displayPlayersDebitInfo(businessPartnerId);
				pagedListHolder.setSource(playerDebitInfo);
			} else {
				
				model.addAttribute("message", errorListForDisplay);
				httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "Y");
				return new ModelAndView("withdrawFromPlayer");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("withdrawFromPlayer");
	}

	
	
	
	@RequestMapping(value="/depositToPlayerOperation")
	public ModelAndView depositToPlayerOperation(@ModelAttribute("depositToPlayer") PlayerCreditModel playerCreditModel, Model model, HttpServletRequest req,String username)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		//playerList = servint.getPlayers(username==null ?"":username);
		//playerList = serviceinter.getPlayersByUserNameForAgents(player.getUsername() == null ? "" : player.getUsername(), businessPartnerId, agentType);
		List<String> playerCreditInfo = serviceinter.displayPlayersCreditInfo(username == null ?"" : username,businessPartnerId);
		
		
		
		httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "N");
		
		Map<Integer,String> playersList = new HashMap<Integer,String>();
		
		Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
		Integer agentType = (Integer) httpSession.getAttribute("agentType");
		
		if (isAgent) {
			playersList = serviceinter.getPlayersListForAgents(businessPartnerId, agentType);
		}else{
			playersList = serviceinter.getPlayersList(businessPartnerId);
		}
		

		Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

		model.addAttribute("playersList", playersList);
		model.addAttribute("bpTotalBalance", bpTotalBalance);
		
		return new ModelAndView("depositToPlayerOperation");
	}
	
	
	@RequestMapping(value = "/insertDepositToPlayerOperation")
	public ModelAndView distributeToPlayerCreate(@ModelAttribute("depositToPlayer") PlayerCreditModel playerCreditModel, BindingResult result, Model model,
			HttpServletRequest req,String username) {

		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}
			System.out.println("Insert Distribute to Palyer Operation ");
			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
			List<String> playerCreditInfo = new ArrayList<String>();
			
			PagedListHolder pagedListHolder = new PagedListHolder(playerCreditInfo);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);

			Map<Integer, String> playersList = serviceinter.getPlayersList(businessPartnerId);
			Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

			model.addAttribute("playersList", playersList);
			model.addAttribute("bpTotalBalance", bpTotalBalance);

			PlayersDepositFormValidation playersCreditFormValidation = new PlayersDepositFormValidation();
			playersCreditFormValidation.validate(playerCreditModel, result);

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "Y");
				return new ModelAndView("depositToPlayerOperation");
			}
			
			playerCreditModel.setDepositAmount(Double.parseDouble(playerCreditModel.getAmountStr()));
			boolean isPlayerCurrencyValid = serviceinter.validatePlayerCurrencyForDepositWithdraw(playerCreditModel.getPlayerId(), playerCreditModel.getCurrencyId());
			if (!isPlayerCurrencyValid) {
				result.rejectValue("currencyId", "error.invalid.currencyId", "Selected Player has different currency than selected credit account.");
			}

			boolean isDepositAmountValid = serviceinter.validateDepositAmount(playerCreditModel, businessPartnerId);
			if (!isDepositAmountValid) {
				result.rejectValue("amountStr", "error.insufficient.depositamount", "Insufficient amount to deposit");
			}

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "Y");
				return new ModelAndView("distributeToPlayer");
			}
			httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "N");
			
			Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
			String userType = "ficha";
			if (isAgent) {
				userType = "agent";
			}
			
			List<Map<String, String>> errorList = serviceinter.depositToPlayer(playerCreditModel, businessPartnerId, userType);

			List errorListForDisplay = new ArrayList();
            //errorListForDisplay.add("error occurred");
			for (Map<String, String> errorMap : errorList) {
				errorListForDisplay.add(errorMap.get("error_message"));
				
			}
			

			if (errorListForDisplay.isEmpty()) {
				//model.addAttribute("message", "Inserted Successfully");
				
				playersList = serviceinter.getPlayersList(businessPartnerId);
				bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

				model.addAttribute("playersList", playersList);
				model.addAttribute("bpTotalBalance", bpTotalBalance);
				
				playerCreditInfo = serviceinter.displayPlayersCreditInfo(username == null ?"" : username,businessPartnerId);
				pagedListHolder.setSource(playerCreditInfo);
			} else {
				
				model.addAttribute("message", errorListForDisplay);
				httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "Y");
				return new ModelAndView("distributeToPlayer");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("distributeToPlayer");
	}
	

	
	
	@RequestMapping(value="/newPlayerInsert")
	public ModelAndView createNewPlayer(@ModelAttribute("players") PlayersModel player, Model model, HttpServletRequest req)
	{
	
		HttpSession httpSession = req.getSession(false);
	if (httpSession == null) {
		return new ModelAndView("Login");
	}

	Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");

	List<String> playerList = new ArrayList<String>();	
	List<String> gender = new ArrayList<String>();
	gender.removeAll(gender);
	gender.add("Male");
	gender.add("Female");
	model.addAttribute("array1", gender);

	List<String> currencyList = new ArrayList<String>();
	currencyList = serviceinter.getCurrencyList();
	model.addAttribute("currencyList", currencyList);

	List<String> countryNameList = (List<String>) httpSession.getAttribute("CountryNameList");
	model.addAttribute("countryList", countryNameList);

	Map<String, String> countryPhoneCodeList = (Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
	model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);

	httpSession = req.getSession();
	httpSession.setAttribute("isValidationErrorPresentPlayer", "N");
	httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
	model.addAttribute("addExistingPlayer", new AddExistingPlayerModel());
	return new ModelAndView("insertNewPlayer");
	}
	
	
	
	@RequestMapping(value = "/insertNewPlayer")
	public ModelAndView insertNewPlayer(@ModelAttribute("players") PlayersModel player, BindingResult result, Model model, HttpServletRequest req,BusinessPartnerModel businessPartnerModel) {

		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}

			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");

			List<String> playerList = new ArrayList<String>();
			
			PagedListHolder pagedListHolder = new PagedListHolder(playerList);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);

			
			List<String> gender = new ArrayList<String>();
			gender.removeAll(gender);
			gender.add("Male");
			gender.add("Female");
			model.addAttribute("array1", gender);

			List<String> currencyList = new ArrayList<String>();
			currencyList = serviceinter.getCurrencyList();
			model.addAttribute("currencyList", currencyList);

			List<String> countryNameList = (List<String>) httpSession.getAttribute("CountryNameList");
			model.addAttribute("countryList", countryNameList);

			Map<String, String> countryPhoneCodeList = (Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
			model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);

			PlayersFormValidation playersFormValidation = new PlayersFormValidation();
			playersFormValidation.validate(player, result);
			model.addAttribute("addExistingPlayer", new AddExistingPlayerModel());
			if (result.hasErrors()) {
				httpSession.setAttribute("isValidationErrorPresentPlayer", "Y");
				return new ModelAndView("insertNewPlayer");
			}

			String username = player.getUsername();
			
			boolean isPlayerAlreadyExists = serviceinter.isPlayerUserNameAlreadyExists(player, businessPartnerId);
			List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
			//System.out.println("isPlayerAlreadyExists = "+isPlayerAlreadyExists);
			String affiliateId=businessPartnerModel.getAffiliateId();
			if(!isPlayerAlreadyExists){
				errorList = serviceinter.insertplayers(player, username, businessPartnerId,affiliateId);
				
				List errorListForDisplay = new ArrayList();
				for (Map<String, String> errorMap : errorList) {
					errorListForDisplay.add(errorMap.get("error_message"));
				}

				
				//model.addAttribute("message", errorListForDisplay);
				if (errorListForDisplay.isEmpty()) {
					httpSession.setAttribute("isValidationErrorPresentPlayer", "N");
					playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
					pagedListHolder.setSource(playerList);
					//model.addAttribute("message", "Inserted Successfully");
				} else {
					model.addAttribute("message", errorListForDisplay);
					httpSession.setAttribute("isValidationErrorPresentPlayer", "Y");
				}

				
			}else{
				httpSession.setAttribute("isValidationErrorPresentPlayer", "Y");
				model.addAttribute("message", messageSource.getMessage("error.player.already.exists", null, RequestContextUtils.getLocale(req)));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("players");
	}

	
	
	
	
	@RequestMapping(value="/addExistingPlayer")
	public ModelAndView addExistingPlayer(@ModelAttribute("addExistingPlayer") AddExistingPlayerModel player, BindingResult result, Model model, HttpServletRequest req)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		return new ModelAndView("addExistingPlayer");
	}
	
	
	
	
	
	@RequestMapping(value = "/insertExistingPlayer")
	public ModelAndView insertExistingPlayer(@ModelAttribute("addExistingPlayer") AddExistingPlayerModel player, BindingResult result, Model model, HttpServletRequest req) {

		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}
            System.out.println("insert add existing player function");
			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
            // System.out.println("add Existig players = "+player.getUserName());
			List<String> playerList = new ArrayList<String>();
			PagedListHolder pagedListHolder = new PagedListHolder(playerList);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);
			
			List<String> gender = new ArrayList<String>();
			gender.removeAll(gender);
			gender.add("Male");
			gender.add("Female");
			model.addAttribute("array1", gender);

			List<String> currencyList = new ArrayList<String>();
			currencyList = serviceinter.getCurrencyList();
			model.addAttribute("currencyList", currencyList);

			List<String> countryNameList = (List<String>) httpSession.getAttribute("CountryNameList");
			model.addAttribute("countryList", countryNameList);

			Map<String, String> countryPhoneCodeList = (Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
			model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
			
			model.addAttribute("players", new PlayersModel());
			
			ValidationUtils.rejectIfEmpty(result, "userName", "required.username", "User Name is required.");
			
			if (result.hasErrors()) {
				httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
				return new ModelAndView("addExistingPlayer");
			}
			
			Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
			Integer agentType = (Integer) httpSession.getAttribute("agentType");

			String userNameToBeAdd = player.getUserName();

			PlayersModel newPlayerModel = new PlayersModel();
			newPlayerModel.setUsername(userNameToBeAdd);
			boolean isPlayerAlreadyExists = serviceinter.isPlayerUserNameAlreadyExists(newPlayerModel, businessPartnerId);
			List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
			if(!isPlayerAlreadyExists){
				
				String userType = "ficha";
				
				if (isAgent) {
					userType = "agent";
					String affiliateId = (String) httpSession.getAttribute("affiliateId");
					errorList = serviceinter.addExistingPlayers(newPlayerModel, userNameToBeAdd, businessPartnerId, affiliateId, userType);
				}else{
					errorList = serviceinter.addExistingPlayers(newPlayerModel, userNameToBeAdd, businessPartnerId, "", userType);
				}
				
				List errorListForDisplay = new ArrayList();
				for (Map<String, String> errorMap : errorList) {
					errorListForDisplay.add(errorMap.get("error_message"));
				}

				if (errorListForDisplay.isEmpty()) {
					//model.addAttribute("message", "Inserted Successfully");
					httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
					playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
					pagedListHolder.setSource(playerList);
				} else {
					httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
					model.addAttribute("message", errorListForDisplay);
				}
			}else{
				
				if (isAgent) {
					boolean isPlayerExits = serviceinter.isPlayerAlreadyExistsForAgents(newPlayerModel, businessPartnerId);
					
					if(!isPlayerExits){
						serviceinter.addExistingPlayerForBP(newPlayerModel, businessPartnerId);
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
						playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
						pagedListHolder.setSource(playerList);
					}else{
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
						model.addAttribute("message", messageSource.getMessage("error.player.already.exists", null, RequestContextUtils.getLocale(req)));
					}
					
				}else{
					boolean isPlayerExits = serviceinter.isPlayerExistsForCurrentBP(newPlayerModel, businessPartnerId);
					
					if(!isPlayerExits){
						serviceinter.addExistingPlayerForBP(newPlayerModel, businessPartnerId);
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
						playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
						pagedListHolder.setSource(playerList);
					}else{
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
						model.addAttribute("message", messageSource.getMessage("error.player.already.exists", null, RequestContextUtils.getLocale(req)));
					}
				}
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		return new ModelAndView("addExistingPlayer");
		
	}
	
	
	
	
	
	
	@RequestMapping(value="/subAgentInsert")
	public ModelAndView subAgentInsert(@ModelAttribute("subAgent") BusinessPartnerModel businessPartnerModel, Model model, HttpServletRequest req, BindingResult result)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		httpSession.setAttribute("isValidationErrorPresent", "N");
		httpSession.setAttribute("isValidationErrorPresentForUpdate", "N");
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		return new ModelAndView("subAgentInsert");
	}
	
	
	
	
/*	*/
	
	
	
	
	
	
	@RequestMapping(value="/insertSubAgents")
	public ModelAndView insertSubAgents(@ModelAttribute("subAgent") BusinessPartnerModel businessPartnerModel, Model model, HttpServletRequest req, BindingResult result)
	{
		HttpSession httpSession = req.getSession(false);
		businessPartnerModel.setCustomErrorMessage("");
		//List<String> businessPartnerList = servint.checkBusinessPartnerFromusername(b1.getBusinessUserName() == null ? "" : b1.getBusinessUserName(), casinoManagerId);
		List<String> subAgentsList = new ArrayList<String>();
		PagedListHolder pagedListHolder = new PagedListHolder(subAgentsList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		//model.addAttribute("searchKey", businessPartnerModel.getBusinessUserName());
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		SubAgentFormValidation b1validation = new SubAgentFormValidation();
		b1validation.validate(businessPartnerModel, result);
		
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresent", "Y");
			return new ModelAndView("subAgentInsert");
		} else {
			httpSession.setAttribute("isValidationErrorPresent", "N");
			String password = businessPartnerModel.getPassword();
			String passwordConfirmation = businessPartnerModel.getConpassword();
			
			String username = businessPartnerModel.getUsername();
			String s1 = serviceinter.createSubAgent(businessPartnerModel, businessPartnerId);

			if (s1.equalsIgnoreCase("Success")) {
				subAgentsList = serviceinter.getSubAgentsByUserName("",	businessPartnerId);
				pagedListHolder.setSource(subAgentsList);
				model.addAttribute("message", "Registration request sent successfully");
			} else {
				httpSession.setAttribute("isValidationErrorPresent", "Y");
				businessPartnerModel.setPassword(password);
				businessPartnerModel.setPassword(passwordConfirmation);
				result.rejectValue("customErrorMessage", "error.user.name.already.exists", "Username or email already used. Please choose another user name or email.");
				return new ModelAndView("subAgents");
			}
		}
		
		
		return new ModelAndView("subAgents");

	}
	
	
	@RequestMapping(value="/bpDepositToFichaInsert")
	public ModelAndView bpDepositToFichaInsert(@ModelAttribute("depositficha") DepositToFicha df, Model model,HttpServletRequest req)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		
		

		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		df.setBusinessPartnerId(businessPartnerId);
		List<String> depositToFichaValues = new ArrayList<String>();
		depositToFichaValues = serviceinter.depositToFichaValue(df);
		
		
		// List<String> currencyList = new ArrayList<String>();
		Map<Integer, String> currencyIdValueMap = serviceinter.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = serviceinter.getPaymentTypeIdValueMap();
		System.out.println(" currencyIdValueMap = " + currencyIdValueMap);

		model.addAttribute("payment", paymentTypeIdValueMap);
		model.addAttribute("Currency", currencyIdValueMap);
		httpSession = req.getSession();
		//httpSession.setAttribute("username", );
		httpSession.setAttribute("isValidationErrorPresentDepositFicha", "N");

		return new ModelAndView("bpDepositToFichaInsert");

	}
	
	//insertDepositToFichaBp
	
	
	@RequestMapping(value="/insertDepositToFichaBp")
	public ModelAndView insertDepositToFichaBp(@ModelAttribute("depositficha") DepositToFicha df, Model model, HttpServletRequest req, BindingResult result)
	{
		
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
         System.out.println("inside insertDepositToFichaBp action");
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> depositToFichaValues = new ArrayList<String>();
		
		PagedListHolder pagedListHolder = new PagedListHolder(depositToFichaValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		// List<String> currencyList = new ArrayList<String>();
		Map<Integer, String> currencyIdValueMap = serviceinter.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = serviceinter.getPaymentTypeIdValueMap();
		// insert logic.....
		System.out.println(" currencyIdValueMap = " + currencyIdValueMap);
		model.addAttribute("payment", paymentTypeIdValueMap);
		model.addAttribute("Currency", currencyIdValueMap);

		df.setBusinessPartnerId(businessPartnerId);

		DepositToFichaFormValidation deposittofichaformvalidation = new DepositToFichaFormValidation();
		deposittofichaformvalidation.validate(df, result);

		if (result.hasErrors()) {
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentDepositFicha", "Y");
			return new ModelAndView("bpDepositToFichaInsert");
		}
		httpSession.setAttribute("isValidationErrorPresentDepositFicha", "N");
		df.setAmount(Double.parseDouble(df.getAmountStr()));
		String s1 = serviceinter.insertDepositToFicha(df);
		
		df.setBusinessPartnerId(businessPartnerId);
		
		depositToFichaValues = serviceinter.depositToFichaValue(df);
		pagedListHolder.setSource(depositToFichaValues);
		
		return new ModelAndView("Deposittoficha");
	}
	
	
	@RequestMapping(value = "/validate", params = "loginbutton")
	public ModelAndView check(@ModelAttribute("login") Login l1, Model model, BindingResult result, HttpServletRequest req) {

		List<HashMap> Loginlist = new ArrayList<HashMap>();
		Loginlist = serviceinter.login(l1.getUsername(), l1.getPassword());
		//System.out.println("Login List = " + Loginlist);
		if (!Loginlist.isEmpty()) {
			System.out.println("inside if condition");
			HttpSession httpSession = req.getSession(true);

			httpSession.setAttribute("businessPartnerUser", l1);
			//
			httpSession.setAttribute("currentusername",l1.getUsername());
			Integer businessPartnerId = null;
			Boolean isAgent = false;
			Integer agentType = null;
			String affiliateId = null;
			if(Loginlist.size() > 0){
				for(HashMap user:Loginlist){
					businessPartnerId = (Integer)user.get("business_id");
					affiliateId = (String)user.get("affiliate_id");
					isAgent = (Boolean)user.get("is_agent");
					agentType = (Integer)user.get("agent_type");
				}
			}

			List<String> countryNameList = serviceinter.getCountryNameList();
			Map<String, String> countryPhoneCodeList = serviceinter.getCountryPhoneCodeMap();

			httpSession.setAttribute("businessPartnerId", businessPartnerId);
			httpSession.setAttribute("affiliateId", affiliateId);
			httpSession.setAttribute("isAgent", isAgent);
			httpSession.setAttribute("agentType", agentType);
			httpSession.setAttribute("CountryNameList", countryNameList);
			httpSession.setAttribute("CountryPhoneCodeList", countryPhoneCodeList);
			System.out.println("isAgent = "+isAgent);
			httpSession.removeAttribute("indexPage");
			
			ModelAndView welcomeModelView = new ModelAndView("Welcome");
			DepositToFicha df = new DepositToFicha();
			df.setBusinessPartnerId(businessPartnerId);
			List<String> businessPartnerCurrencyDetails = new ArrayList<String>();
			businessPartnerCurrencyDetails = serviceinter.businessPartnerCurrencyDetails(df);
		
			PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerCurrencyDetails);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			welcomeModelView.addObject("pagedListHolder", pagedListHolder);
			
			return welcomeModelView;
		} else {
			ModelAndView loginModelView = new ModelAndView("Login");
			result.rejectValue("", "error.invalidLogin", "Incorrect User Name or Password");
			System.out.println("Username and password is Not there");
			l1.setUsername("");
			l1.setPassword("");
			HttpSession indexPageSession = req.getSession(true);
			indexPageSession.setAttribute("indexPage", "login");

			return loginModelView;
		}
	}
	
	@RequestMapping(value = "/subAgents")
	public ModelAndView subAgentView(@ModelAttribute("subAgent") BusinessPartnerModel businessPartnerModel, Model model, HttpServletRequest req, BindingResult result) {

		HttpSession httpSession = req.getSession(false);
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> subAgentsList = serviceinter.getSubAgentsByUserName(businessPartnerModel.getBusinessUserName() == null ? "" : businessPartnerModel.getBusinessUserName(), businessPartnerId);
		PagedListHolder pagedListHolder = new PagedListHolder(subAgentsList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", businessPartnerModel.getBusinessUserName());

		httpSession.setAttribute("isValidationErrorPresent", "N");
		httpSession.setAttribute("isValidationErrorPresentForUpdate", "N");
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		return new ModelAndView("subAgents");
	}
	
	
	/*@RequestMapping(value = "/subAgents", params="create")
	public ModelAndView subAgentOperation(@ModelAttribute("subAgent") BusinessPartnerModel businessPartnerModel, Model model, HttpServletRequest req, BindingResult result) {

		HttpSession httpSession = req.getSession(false);
		businessPartnerModel.setCustomErrorMessage("");
		//List<String> businessPartnerList = servint.checkBusinessPartnerFromusername(b1.getBusinessUserName() == null ? "" : b1.getBusinessUserName(), casinoManagerId);
		List<String> subAgentsList = new ArrayList<String>();
		PagedListHolder pagedListHolder = new PagedListHolder(subAgentsList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		//model.addAttribute("searchKey", businessPartnerModel.getBusinessUserName());
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		SubAgentFormValidation b1validation = new SubAgentFormValidation();
		b1validation.validate(businessPartnerModel, result);
		
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresent", "Y");
			return new ModelAndView("subAgents");
		} else {
			httpSession.setAttribute("isValidationErrorPresent", "N");
			String password = businessPartnerModel.getPassword();
			String passwordConfirmation = businessPartnerModel.getConpassword();
			
			String username = businessPartnerModel.getUsername();
			String s1 = serviceinter.createSubAgent(businessPartnerModel, businessPartnerId);

			if (s1.equalsIgnoreCase("Success")) {
				subAgentsList = serviceinter.getSubAgentsByUserName("",	businessPartnerId);
				pagedListHolder.setSource(subAgentsList);
				model.addAttribute("message", "Registration request sent successfully");
			} else {
				httpSession.setAttribute("isValidationErrorPresent", "Y");
				businessPartnerModel.setPassword(password);
				businessPartnerModel.setPassword(passwordConfirmation);
				result.rejectValue("customErrorMessage", "error.user.name.already.exists", "Username or email already used. Please choose another user name or email.");
				return new ModelAndView("subAgents");
			}
		}
		
		
		return new ModelAndView("subAgents");
	}


*/
	@RequestMapping(value = "/validate", params = "registerBP")
	public ModelAndView registrationBusinessPartner(@ModelAttribute("businessPartnerRegistration") BusinessPartnerModel businessPartnerModel, Model model) {
		
		businessPartnerModel.setUsername("");
		businessPartnerModel.setPassword("");
		
		List<String> countryNameList = serviceinter.getCountryNameList();
		Map<String, String> countryPhoneCodeList = serviceinter.getCountryPhoneCodeMap();
		
		model.addAttribute("countryList", countryNameList);
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		//System.out.println("Registration Page");
		return new ModelAndView("businessPartner");
	}

	@RequestMapping(value = "/register", params = "login")
	public ModelAndView register(@ModelAttribute("login") Login l1, Model model) {
		l1.setUsername("");
		l1.setPassword("");
		return new ModelAndView("Login");

	}
	
	@RequestMapping(value = "/register", params = "register")
	public ModelAndView register(@ModelAttribute("businessPartnerRegistration") BusinessPartnerModel businessPartnerModel, BindingResult result, Model model) {

		System.out.println("business partner register action ");
		
		List<String> countryNameList = serviceinter.getCountryNameList();
		Map<String, String> countryPhoneCodeList = serviceinter.getCountryPhoneCodeMap();
		
		model.addAttribute("countryList", countryNameList);
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		BusinessPartnerFormValidation b1validation = new BusinessPartnerFormValidation();
		b1validation.validate(businessPartnerModel, result);
		
		if (result.hasErrors()) {
			return new ModelAndView("businessPartner");
		} else {

			String password = businessPartnerModel.getPassword();
			String passwordConfirmation = businessPartnerModel.getConpassword();
			
			String username = businessPartnerModel.getUsername();
			String s1 = serviceinter.createBusinessPartner(businessPartnerModel, username);

			if (s1.equalsIgnoreCase("Success")) {
				businessPartnerModel = new BusinessPartnerModel();
				model.addAttribute("businessPartnerRegistration", businessPartnerModel);
				model.addAttribute("message", "Registration request sent successfully");
			} else {
				
				businessPartnerModel.setPassword(password);
				businessPartnerModel.setPassword(passwordConfirmation);
				
				model.addAttribute("message", "Username or Email already used. Please choose another user name or email.");
			}
		}

		return new ModelAndView("businessPartner");

	}


	// *************************************************players CRUD
	// operations**********************************************

	@RequestMapping(value = "/Players")
	public ModelAndView Playersview(@ModelAttribute("players") PlayersModel player, Model model, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}

		
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");

		List<String> playerList = new ArrayList<String>();
		
		Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
		Integer agentType = (Integer) httpSession.getAttribute("agentType");
		
		if (isAgent) {
			playerList = serviceinter.getPlayersByUserNameForAgents(player.getUsername() == null ? "" : player.getUsername(), businessPartnerId, agentType);
		}else{
			playerList = serviceinter.getPlayersByUserName(player.getUsername() == null ? "" : player.getUsername(), businessPartnerId);
		}
		
		
		PagedListHolder pagedListHolder = new PagedListHolder(playerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", player.getUsername());
		
		List<String> gender = new ArrayList<String>();
		gender.removeAll(gender);
		gender.add("Male");
		gender.add("Female");
		model.addAttribute("array1", gender);

		List<String> currencyList = new ArrayList<String>();
		currencyList = serviceinter.getCurrencyList();
		model.addAttribute("currencyList", currencyList);

		List<String> countryNameList = (List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);

		Map<String, String> countryPhoneCodeList = (Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);

		httpSession = req.getSession();
		httpSession.setAttribute("isValidationErrorPresentPlayer", "N");
		httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
		model.addAttribute("addExistingPlayer", new AddExistingPlayerModel());
		return new ModelAndView("players");
	}

	/*@RequestMapping(value = "/playersoper")
	public ModelAndView Playersinsert(@ModelAttribute("players") PlayersModel player, BindingResult result, Model model, HttpServletRequest req,BusinessPartnerModel businessPartnerModel) {

		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}

			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");

			List<String> playerList = new ArrayList<String>();
			
			PagedListHolder pagedListHolder = new PagedListHolder(playerList);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);

			
			List<String> gender = new ArrayList<String>();
			gender.removeAll(gender);
			gender.add("Male");
			gender.add("Female");
			model.addAttribute("array1", gender);

			List<String> currencyList = new ArrayList<String>();
			currencyList = serviceinter.getCurrencyList();
			model.addAttribute("currencyList", currencyList);

			List<String> countryNameList = (List<String>) httpSession.getAttribute("CountryNameList");
			model.addAttribute("countryList", countryNameList);

			Map<String, String> countryPhoneCodeList = (Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
			model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);

			PlayersFormValidation playersFormValidation = new PlayersFormValidation();
			playersFormValidation.validate(player, result);
			model.addAttribute("addExistingPlayer", new AddExistingPlayerModel());
			if (result.hasErrors()) {
				httpSession.setAttribute("isValidationErrorPresentPlayer", "Y");
				return new ModelAndView("players");
			}

			String username = player.getUsername();
			
			boolean isPlayerAlreadyExists = serviceinter.isPlayerUserNameAlreadyExists(player, businessPartnerId);
			List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
			//System.out.println("isPlayerAlreadyExists = "+isPlayerAlreadyExists);
			String affiliateId=businessPartnerModel.getAffiliateId();
			if(!isPlayerAlreadyExists){
				errorList = serviceinter.insertplayers(player, username, businessPartnerId,affiliateId);
				
				List errorListForDisplay = new ArrayList();
				for (Map<String, String> errorMap : errorList) {
					errorListForDisplay.add(errorMap.get("error_message"));
				}

				
				//model.addAttribute("message", errorListForDisplay);
				if (errorListForDisplay.isEmpty()) {
					httpSession.setAttribute("isValidationErrorPresentPlayer", "N");
					playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
					pagedListHolder.setSource(playerList);
					//model.addAttribute("message", "Inserted Successfully");
				} else {
					model.addAttribute("message", errorListForDisplay);
					httpSession.setAttribute("isValidationErrorPresentPlayer", "Y");
				}

				
			}else{
				httpSession.setAttribute("isValidationErrorPresentPlayer", "Y");
				model.addAttribute("message", messageSource.getMessage("error.player.already.exists", null, RequestContextUtils.getLocale(req)));
			}
			 

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("players");
	}
*/
	/*@RequestMapping(value = "/addExistingPlayer", params = "addExistingPlayerButton")
	public ModelAndView addExistingPlayer(@ModelAttribute("addExistingPlayer") AddExistingPlayerModel player, BindingResult result, Model model, HttpServletRequest req) {

		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}

			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
            // System.out.println("add Existig players = "+player.getUserName());
			List<String> playerList = new ArrayList<String>();
			PagedListHolder pagedListHolder = new PagedListHolder(playerList);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);
			
			List<String> gender = new ArrayList<String>();
			gender.removeAll(gender);
			gender.add("Male");
			gender.add("Female");
			model.addAttribute("array1", gender);

			List<String> currencyList = new ArrayList<String>();
			currencyList = serviceinter.getCurrencyList();
			model.addAttribute("currencyList", currencyList);

			List<String> countryNameList = (List<String>) httpSession.getAttribute("CountryNameList");
			model.addAttribute("countryList", countryNameList);

			Map<String, String> countryPhoneCodeList = (Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
			model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
			
			model.addAttribute("players", new PlayersModel());
			
			ValidationUtils.rejectIfEmpty(result, "userName", "required.username", "User Name is required.");
			
			if (result.hasErrors()) {
				httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
				return new ModelAndView("players");
			}
			
			Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
			Integer agentType = (Integer) httpSession.getAttribute("agentType");

			String userNameToBeAdd = player.getUserName();

			PlayersModel newPlayerModel = new PlayersModel();
			newPlayerModel.setUsername(userNameToBeAdd);
			boolean isPlayerAlreadyExists = serviceinter.isPlayerUserNameAlreadyExists(newPlayerModel, businessPartnerId);
			List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
			if(!isPlayerAlreadyExists){
				
				String userType = "ficha";
				
				if (isAgent) {
					userType = "agent";
					String affiliateId = (String) httpSession.getAttribute("affiliateId");
					errorList = serviceinter.addExistingPlayers(newPlayerModel, userNameToBeAdd, businessPartnerId, affiliateId, userType);
				}else{
					errorList = serviceinter.addExistingPlayers(newPlayerModel, userNameToBeAdd, businessPartnerId, "", userType);
				}
				
				List errorListForDisplay = new ArrayList();
				for (Map<String, String> errorMap : errorList) {
					errorListForDisplay.add(errorMap.get("error_message"));
				}

				if (errorListForDisplay.isEmpty()) {
					//model.addAttribute("message", "Inserted Successfully");
					httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
					playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
					pagedListHolder.setSource(playerList);
				} else {
					httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
					model.addAttribute("message", errorListForDisplay);
				}
			}else{
				
				if (isAgent) {
					boolean isPlayerExits = serviceinter.isPlayerAlreadyExistsForAgents(newPlayerModel, businessPartnerId);
					
					if(!isPlayerExits){
						serviceinter.addExistingPlayerForBP(newPlayerModel, businessPartnerId);
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
						playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
						pagedListHolder.setSource(playerList);
					}else{
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
						model.addAttribute("message", messageSource.getMessage("error.player.already.exists", null, RequestContextUtils.getLocale(req)));
					}
					
				}else{
					boolean isPlayerExits = serviceinter.isPlayerExistsForCurrentBP(newPlayerModel, businessPartnerId);
					
					if(!isPlayerExits){
						serviceinter.addExistingPlayerForBP(newPlayerModel, businessPartnerId);
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "N");
						playerList = serviceinter.getPlayersByUserName("", businessPartnerId);
						pagedListHolder.setSource(playerList);
					}else{
						httpSession.setAttribute("isPlayerAlreadyExistsForAddExistingPlayer", "Y");
						model.addAttribute("message", messageSource.getMessage("error.player.already.exists", null, RequestContextUtils.getLocale(req)));
					}
				}
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("players");
	}*/

/*	@RequestMapping(value = "/playersoper", params = "update")
	public ModelAndView Playersupdate(@ModelAttribute("players") PlayersModel player, Model model) {

		List<String> gender = new ArrayList<String>();
		gender.removeAll(gender);
		gender.add("Male");
		gender.add("Female");
		model.addAttribute("array1", gender);

		String username = player.getUsername();
		String s1 = serviceinter.updateplayers(player, username);
		model.addAttribute("addExistingPlayer", new AddExistingPlayerModel());
		if (s1.equalsIgnoreCase("Success")) {
			model.addAttribute("message", "Updated Successfully");
			return new ModelAndView("players");

		} else {
			model.addAttribute("message", "Username already Available");
			return new ModelAndView("players");
		}
	}

	@RequestMapping(value = "/playersoper", params = "delete")
	public ModelAndView Playersdelete(@ModelAttribute("players") PlayersModel player, Model model) {
		String s1 = serviceinter.deleteplayers(player);

		List<String> gender = new ArrayList<String>();
		gender.removeAll(gender);
		gender.add("Male");
		gender.add("Female");
		model.addAttribute("array1", gender);
		model.addAttribute("addExistingPlayer", new AddExistingPlayerModel());
		return new ModelAndView("players");
	}*/

	//

	@RequestMapping(value = "/Player", method = RequestMethod.GET)
	@ResponseBody
	public List ajax(@ModelAttribute("players") PlayersModel player, Model model, HttpServletRequest req) {

		List<String> show = new ArrayList<String>();
		HttpSession httpSession = req.getSession(false);
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");

		show = serviceinter.displayPlayers(businessPartnerId);
		return show;
	}

	@RequestMapping(value = "/playersallvalues", method = RequestMethod.GET)
	@ResponseBody
	public List playerallvalues(@ModelAttribute("players") PlayersModel player, Model model, int id) {

		List<String> showallvalues = new ArrayList<String>();
		showallvalues = serviceinter.playerallvalue(id);
		// System.out.println("Controller class = "+showallvalues);
		return showallvalues;
	}

	//

	@RequestMapping(value = "/getPlayersByUserName", method = RequestMethod.GET)
	@ResponseBody
	public List getPlayersByUserName(String playerUserName, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");

		List<String> playerList = new ArrayList<String>();
		System.out.println("Controller class     = " + playerUserName);

		playerList = serviceinter.getPlayersByUserName(playerUserName, businessPartnerId);
		return playerList;
	}

	// **************************************Deposit to
	// Ficha****************************************************

	@RequestMapping(value = "/Deposittoficha")
	public ModelAndView Deposittoficha(@ModelAttribute("depositficha") DepositToFicha df, Model model, HttpServletRequest req) {

		
		HttpSession httpSession = req.getSession(false);

		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		df.setBusinessPartnerId(businessPartnerId);
		List<String> depositToFichaValues = new ArrayList<String>();
		depositToFichaValues = serviceinter.depositToFichaValue(df);
		
		PagedListHolder pagedListHolder = new PagedListHolder(depositToFichaValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		// List<String> currencyList = new ArrayList<String>();
		Map<Integer, String> currencyIdValueMap = serviceinter.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = serviceinter.getPaymentTypeIdValueMap();
		System.out.println(" currencyIdValueMap = " + currencyIdValueMap);

		model.addAttribute("payment", paymentTypeIdValueMap);
		model.addAttribute("Currency", currencyIdValueMap);
		httpSession = req.getSession();
		//httpSession.setAttribute("username", );
		httpSession.setAttribute("isValidationErrorPresentDepositFicha", "N");

		return new ModelAndView("Deposittoficha");
	}

	/*@RequestMapping(value = "/depositfi", params = "create")
	public ModelAndView Deposittofichainsert(@ModelAttribute("depositficha") DepositToFicha df, Model model, HttpServletRequest req, BindingResult result) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}

		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> depositToFichaValues = new ArrayList<String>();
		
		PagedListHolder pagedListHolder = new PagedListHolder(depositToFichaValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		// List<String> currencyList = new ArrayList<String>();
		Map<Integer, String> currencyIdValueMap = serviceinter.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = serviceinter.getPaymentTypeIdValueMap();
		// insert logic.....
		System.out.println(" currencyIdValueMap = " + currencyIdValueMap);
		model.addAttribute("payment", paymentTypeIdValueMap);
		model.addAttribute("Currency", currencyIdValueMap);

		df.setBusinessPartnerId(businessPartnerId);

		DepositToFichaFormValidation deposittofichaformvalidation = new DepositToFichaFormValidation();
		deposittofichaformvalidation.validate(df, result);

		if (result.hasErrors()) {
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentDepositFicha", "Y");
			return new ModelAndView("Deposittoficha");
		}
		httpSession.setAttribute("isValidationErrorPresentDepositFicha", "N");
		df.setAmount(Double.parseDouble(df.getAmountStr()));
		String s1 = serviceinter.insertDepositToFicha(df);
		
		df.setBusinessPartnerId(businessPartnerId);
		
		depositToFichaValues = serviceinter.depositToFichaValue(df);
		pagedListHolder.setSource(depositToFichaValues);
		
		return new ModelAndView("Deposittoficha");
	}

*/	//

	@RequestMapping(value = "/deposittoFitchaAllValues", method = RequestMethod.GET)
	@ResponseBody
	public List playerallvalues(@ModelAttribute("depositficha") DepositToFicha df, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {

		}

		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		df.setBusinessPartnerId(businessPartnerId);
		List<String> depositToFichaValues = new ArrayList<String>();
		depositToFichaValues = serviceinter.depositToFichaValue(df);
		System.out.println("Controller class = " + depositToFichaValues);
		return depositToFichaValues;
	}

	// *************************************Deposit to
	// Players*******************************************************

	@RequestMapping(value = "/getPlayerListByCurrency", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getPlayerListByCurrency(@ModelAttribute("depositToPlayer") PlayerCreditModel player, Model model, Integer currencyId, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {

		}
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> playersList = serviceinter.getPlayersListByCurrency(currencyId, businessPartnerId);

		// System.out.println("Controller class = "+show);
		return playersList;
	}

	@RequestMapping(value = "/distributeToPlayerView", method = RequestMethod.GET)
	@ResponseBody
	public List ajax(@ModelAttribute("depositToPlayer") PlayerCreditModel player, Model model, HttpServletRequest req,String username) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {

		}
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> playerCreditInfo = serviceinter.displayPlayersCreditInfo(username == null ?"" : username,businessPartnerId);
		// System.out.println("Controller class = "+show);
		return playerCreditInfo;
	}

	// playersListAccordingToCurrency

	@RequestMapping(value = "/playersListAccordingToCurrency", method = RequestMethod.GET)
	@ResponseBody
	public List getPlayerListByCurrency(Integer currencyValue, HttpServletRequest req) {
		// System.out.println("Controller Currency Value = "+currencyValue);
		List<String> playerCurrencyList = new ArrayList<String>();
		HttpSession httpSession = req.getSession(false);
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		playerCurrencyList = serviceinter.getPlayerListByCurrency(currencyValue, businessPartnerId);
		System.out.println("playerCurrencyList = " + playerCurrencyList);
		return playerCurrencyList;
	}

	@RequestMapping(value = "/distributeToPlayer")
	public ModelAndView Distributetoplayerview(@ModelAttribute("depositToPlayer") PlayerCreditModel playerCreditModel, Model model, HttpServletRequest req,String username) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}

		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		//playerList = servint.getPlayers(username==null ?"":username);
		//playerList = serviceinter.getPlayersByUserNameForAgents(player.getUsername() == null ? "" : player.getUsername(), businessPartnerId, agentType);
		List<String> playerCreditInfo = serviceinter.displayPlayersCreditInfo(username == null ?"" : username,businessPartnerId);
		
		PagedListHolder pagedListHolder = new PagedListHolder(playerCreditInfo);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "N");
		
		Map<Integer,String> playersList = new HashMap<Integer,String>();
		
		Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
		Integer agentType = (Integer) httpSession.getAttribute("agentType");
		
		if (isAgent) {
			playersList = serviceinter.getPlayersListForAgents(businessPartnerId, agentType);
		}else{
			playersList = serviceinter.getPlayersList(businessPartnerId);
		}
		

		Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

		model.addAttribute("playersList", playersList);
		model.addAttribute("bpTotalBalance", bpTotalBalance);

		return new ModelAndView("distributeToPlayer");
	}

	/*@RequestMapping(value = "/distributeToPlayerOperation", params = "create")
	public ModelAndView distributeToPlayerCreate(@ModelAttribute("depositToPlayer") PlayerCreditModel playerCreditModel, BindingResult result, Model model,
			HttpServletRequest req,String username) {

		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}
			
			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
			List<String> playerCreditInfo = new ArrayList<String>();
			
			PagedListHolder pagedListHolder = new PagedListHolder(playerCreditInfo);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);

			Map<Integer, String> playersList = serviceinter.getPlayersList(businessPartnerId);
			Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

			model.addAttribute("playersList", playersList);
			model.addAttribute("bpTotalBalance", bpTotalBalance);

			PlayersDepositFormValidation playersCreditFormValidation = new PlayersDepositFormValidation();
			playersCreditFormValidation.validate(playerCreditModel, result);

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "Y");
				return new ModelAndView("distributeToPlayer");
			}
			
			playerCreditModel.setDepositAmount(Double.parseDouble(playerCreditModel.getAmountStr()));
			boolean isPlayerCurrencyValid = serviceinter.validatePlayerCurrencyForDepositWithdraw(playerCreditModel.getPlayerId(), playerCreditModel.getCurrencyId());
			if (!isPlayerCurrencyValid) {
				result.rejectValue("currencyId", "error.invalid.currencyId", "Selected Player has different currency than selected credit account.");
			}

			boolean isDepositAmountValid = serviceinter.validateDepositAmount(playerCreditModel, businessPartnerId);
			if (!isDepositAmountValid) {
				result.rejectValue("amountStr", "error.insufficient.depositamount", "Insufficient amount to deposit");
			}

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "Y");
				return new ModelAndView("distributeToPlayer");
			}
			httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "N");
			
			Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
			String userType = "ficha";
			if (isAgent) {
				userType = "agent";
			}
			
			List<Map<String, String>> errorList = serviceinter.depositToPlayer(playerCreditModel, businessPartnerId, userType);

			List errorListForDisplay = new ArrayList();
            //errorListForDisplay.add("error occurred");
			for (Map<String, String> errorMap : errorList) {
				errorListForDisplay.add(errorMap.get("error_message"));
				
			}
			

			if (errorListForDisplay.isEmpty()) {
				//model.addAttribute("message", "Inserted Successfully");
				
				playersList = serviceinter.getPlayersList(businessPartnerId);
				bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

				model.addAttribute("playersList", playersList);
				model.addAttribute("bpTotalBalance", bpTotalBalance);
				
				playerCreditInfo = serviceinter.displayPlayersCreditInfo(username == null ?"" : username,businessPartnerId);
				pagedListHolder.setSource(playerCreditInfo);
			} else {
				
				model.addAttribute("message", errorListForDisplay);
				httpSession.setAttribute("isValidationErrorPresentDistributiontoPlayer", "Y");
				return new ModelAndView("distributeToPlayer");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("distributeToPlayer");
	}
	
*/	
	@RequestMapping(value = "/withdrawFromPlayer")
	public ModelAndView withdrawFromPlayerView(@ModelAttribute("withdrawFromPlayer") PlayerDebitModel playerDebitModel, Model model, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}

		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		List<String> playerDebitInfo = serviceinter.displayPlayersDebitInfo(businessPartnerId);
		
		PagedListHolder pagedListHolder = new PagedListHolder(playerDebitInfo);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "N");
		
		Map<Integer,String> playersList = new HashMap<Integer,String>();
		
		Boolean isAgent = (Boolean) httpSession.getAttribute("isAgent");
		Integer agentType = (Integer) httpSession.getAttribute("agentType");
		
		if (isAgent) {
			playersList = serviceinter.getPlayersListForAgents(businessPartnerId, agentType);
		}else{
			playersList = serviceinter.getPlayersList(businessPartnerId);
		}
		

		Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

		model.addAttribute("playersList", playersList);
		model.addAttribute("bpTotalBalance", bpTotalBalance);

		return new ModelAndView("withdrawFromPlayer");
	}

	/*@RequestMapping(value = "/withdrawFromPlayerOperation", params = "create")
	public ModelAndView withdrawFromPlayerCreate(@ModelAttribute("withdrawFromPlayer") PlayerDebitModel playerDebitModel, BindingResult result, Model model,
			HttpServletRequest req) {

		try {
			HttpSession httpSession = req.getSession(false);
			if (httpSession == null) {
				return new ModelAndView("Login");
			}
			
			Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
			List<String> playerDebitInfo = new ArrayList<String>();
			
			PagedListHolder pagedListHolder = new PagedListHolder(playerDebitInfo);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);

			Map<Integer, String> playersList = serviceinter.getPlayersList(businessPartnerId);
			Map<Integer, String> bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

			model.addAttribute("playersList", playersList);
			model.addAttribute("bpTotalBalance", bpTotalBalance);

			PlayersWithdrawFormValidation playersWithdrawFormValidation = new PlayersWithdrawFormValidation();
			playersWithdrawFormValidation.validate(playerDebitModel, result);

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "Y");
				return new ModelAndView("withdrawFromPlayer");
			}
			
			playerDebitModel.setWithdrawAmount(Double.parseDouble(playerDebitModel.getAmountStr()));
			
			boolean isPlayerCurrencyValid = serviceinter.validatePlayerCurrencyForDepositWithdraw(playerDebitModel.getPlayerId(), playerDebitModel.getCurrencyId());
			if (!isPlayerCurrencyValid) {
				result.rejectValue("currencyId", "error.invalid.currencyId", "Selected Player has different currency than selected credit account.");
			}

			if (result.hasErrors()) {
				httpSession = req.getSession();
				httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "Y");
				return new ModelAndView("withdrawFromPlayer");
			}
			httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "N");
			List<Map<String, String>> errorList = serviceinter.withdrawFromPlayer(playerDebitModel, businessPartnerId);

			List errorListForDisplay = new ArrayList();
			//errorListForDisplay.add("error occurred");
			for (Map<String, String> errorMap : errorList) {
				errorListForDisplay.add(errorMap.get("error_message"));
				
			}
			

			if (errorListForDisplay.isEmpty()) {
				//model.addAttribute("message", "Inserted Successfully");
				
				playersList = serviceinter.getPlayersList(businessPartnerId);
				bpTotalBalance = serviceinter.getBpTotalBalances(businessPartnerId);

				model.addAttribute("playersList", playersList);
				model.addAttribute("bpTotalBalance", bpTotalBalance);
				
				playerDebitInfo = serviceinter.displayPlayersDebitInfo(businessPartnerId);
				pagedListHolder.setSource(playerDebitInfo);
			} else {
				
				model.addAttribute("message", errorListForDisplay);
				httpSession.setAttribute("isValidationErrorPresentWithDrawFromPlayer", "Y");
				return new ModelAndView("withdrawFromPlayer");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("withdrawFromPlayer");
	}
*/
	@RequestMapping(value = "/Welcome")
	public String dashboard(@ModelAttribute("depositficha") DepositToFicha df, Model model, HttpServletRequest req) {
		
		HttpSession httpSession = req.getSession(false);
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		df.setBusinessPartnerId(businessPartnerId);
		List<String> businessPartnerCurrencyDetails = new ArrayList<String>();
		businessPartnerCurrencyDetails = serviceinter.businessPartnerCurrencyDetails(df);
	
		PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerCurrencyDetails);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		return "Welcome";
	}

	// businessPartnerCurrencyDetails

	@RequestMapping(value = "/businessPartnerCurrencyDetails", method = RequestMethod.GET)
	@ResponseBody
	public List businessPartnerCurrencyDetails(@ModelAttribute("depositficha") DepositToFicha df, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {

		}
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		df.setBusinessPartnerId(businessPartnerId);
		List<String> businessPartnerCurrencyDetails = new ArrayList<String>();
		businessPartnerCurrencyDetails = serviceinter.businessPartnerCurrencyDetails(df);
		System.out.println("Controller class = " + businessPartnerCurrencyDetails);
		return businessPartnerCurrencyDetails;
	}

	//

	@RequestMapping(value = "/businessPartnerInformationDetails", method = RequestMethod.GET)
	@ResponseBody
	public List busbusinessPartnerInformationDetails(@ModelAttribute("depositficha") DepositToFicha df, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {

		}
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		df.setBusinessPartnerId(businessPartnerId);
		List<String> businessPartnerInformationDetails = new ArrayList<String>();
		businessPartnerInformationDetails = serviceinter.businessPartnerInformationDetails(df);
		System.out.println("Controller class = " + businessPartnerInformationDetails);
		return businessPartnerInformationDetails;
	}
	
	
	
	
	
	@RequestMapping(value = "/activateBusinessPartner")
	public ModelAndView activateBusinessPartner(@ModelAttribute("resetPassword") ResetPasswordOnActivation resetPasswordOnActivation, Model model, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		return new ModelAndView("activateBusinessPartner");
	}

	
	
	
	
	@RequestMapping(value = "/resetPasswordOnActivation")
	public ModelAndView resetPasswordOnActivation(@ModelAttribute("resetPassword") ResetPasswordOnActivation resetPasswordOnActivation, Model model, BindingResult result, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		
		ResetPasswordFormValidation resetPasswordValidation = new ResetPasswordFormValidation();
		resetPasswordValidation.validate(resetPasswordOnActivation, result);
		
		if (result.hasErrors()) {
			return new ModelAndView("activateBusinessPartner");
		} else {

			String resultStr = serviceinter.resetPasswordOnActivation(resetPasswordOnActivation);

			if (resultStr.equalsIgnoreCase("Success")) {
				//model.addAttribute("message", "Password is successfully updated on Activation.");
				model.addAttribute("login", new Login());
				return new ModelAndView("Login");
			} else {
				model.addAttribute("message", "Either activation code has been expired or User is already activated.");
				return new ModelAndView("activateBusinessPartner");
			}
		}	
		
	}	

	@RequestMapping(value = "/changePassword")
	public ModelAndView forgotPassword(@ModelAttribute("forgotPassword") ChangePassword changePassword) {
		return new ModelAndView("forgotPassword");
	}
	
	@RequestMapping(value = "/resetPassword",params="changePassword")
	public ModelAndView changePassword(@ModelAttribute("forgotPassword") ChangePassword changePassword, Model model, BindingResult result, HttpServletRequest req) {
		System.out.println("Change Password Page");
		
		HttpSession httpSession = req.getSession(false);
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		
		ChangePasswordValidation changepasswordvalidation= new ChangePasswordValidation();
		changepasswordvalidation.validate(changePassword, result);
		if (result.hasErrors()) {
			return new ModelAndView("forgotPassword");
		}
	
		String password = changePassword.getNewPassword();
		String passwordConfirmation = changePassword.getConfirmPassword();
		
		String resultStr = serviceinter.changePassword(changePassword, businessPartnerId);
		
		if (resultStr.equalsIgnoreCase("Success")) {
			changePassword.setNewPassword("");
			changePassword.setConfirmPassword("");
			changePassword.setOldPassword("");
			changePassword.setSalt("");
			model.addAttribute("message", messageSource.getMessage("message.password.successfully.changed", null, RequestContextUtils.getLocale(req)));
		} else {
			changePassword.setNewPassword(password);
			changePassword.setConfirmPassword(passwordConfirmation);
			model.addAttribute("message", messageSource.getMessage("error.invalid.old.password", null, RequestContextUtils.getLocale(req)));

		}
		return new ModelAndView("forgotPassword");

	}
	
	
	@RequestMapping(value = "/validate", params = "forgotPasswordButton")
	public ModelAndView forgotPassword(@ModelAttribute("forgotPasswordCredentials") ForgotPasswordCredentials forgotPasswordCredentials, Model model, HttpServletRequest req) {
		
	return new ModelAndView("forgotPasswordCredentials");	
	}
	
	@RequestMapping(value = "/forgotPasswordActivationAction", params = "forgotPasswordActivationButton")
	public ModelAndView forgotPasswordCredentials(@ModelAttribute("forgotPasswordCredentials") ForgotPasswordCredentials forgotPasswordCredentials, BindingResult result, HttpServletRequest req,Model model) {
		//System.out.println("forgotPasswordActivation = "+forgotPasswordCredentials.getUserName());
	ForgotPasswordFormValidation forgotPasswordFormValidation=new ForgotPasswordFormValidation();
	forgotPasswordFormValidation.validate(forgotPasswordCredentials, result);
	if(result.hasErrors())
	{
			
		return new ModelAndView("forgotPasswordCredentials");
	}
	else
	{
		String s1 = serviceinter.checkUserName(forgotPasswordCredentials);
		System.out.println("reyurn type = "+s1);
		if(s1.contains("Fail"))
		{
			model.addAttribute("message","Please give valid User Name");
		return new ModelAndView("forgotPasswordCredentials");
		}
		else
		{
			model.addAttribute("message","Your Verfication Code is send to your Registered Email");
			return new ModelAndView("forgotPasswordCredentials");	
		}
	}
	
}
	
	
	@RequestMapping(value = "/forgotPasswordEmailActivation")
	public ModelAndView forgotPasswordActivation(@ModelAttribute("forgotPasswordEmail") ForgotPasswordOnActivation forgotPasswordOnActivation, Model model, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		return new ModelAndView("forgotPasswordEmailActivation");
	}
	
	
	
	
	
	@RequestMapping(value = "/forgotPasswordEmailActivation",params="reset")
	public ModelAndView forgotPasswordOnActivationAction(@ModelAttribute("forgotPasswordEmail") ForgotPasswordOnActivation forgotPasswordOnActivation, Model model, BindingResult result, HttpServletRequest req) {

		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		
		ForgotPasswordEmailFormActivation forgotPasswordEmailFormActivation = new ForgotPasswordEmailFormActivation();
		forgotPasswordEmailFormActivation.validate(forgotPasswordOnActivation, result);
		
		if (result.hasErrors()) {
			return new ModelAndView("forgotPasswordEmailActivation");
		} else {

			String resultStr = serviceinter.forgotPasswordOnActivationAction(forgotPasswordOnActivation);

			if (resultStr.equalsIgnoreCase("Success")) {
				//model.addAttribute("message", "Password is successfully updated on Activation.");
				model.addAttribute("login", new Login());
				return new ModelAndView("Login");
			} else {
				model.addAttribute("message", "Either activation code has been expired or User is already activated.");
				
			}
		}
		return new ModelAndView("forgotPasswordEmailActivation");
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/playersName",method = RequestMethod.GET)
	@ResponseBody
	public List playersNameValue(HttpServletRequest req, String playerName)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
		}
		System.out.println("playerName = "+playerName);
		List<String> playerNamesDetails = new ArrayList<String>();
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		playerNamesDetails = serviceinter.playerNamesDetails(playerName,businessPartnerId);
		System.out.println("playerNamesDetails = "+playerNamesDetails);
		return playerNamesDetails;
	}
	
	//allPlayersName
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/allPlayersName",method = RequestMethod.GET)
	@ResponseBody
	public List allPlayersName(HttpServletRequest req)
	{
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
		}
		System.out.println("all player function");
		List<String> playerNamesDetails = new ArrayList<String>();
		Integer businessPartnerId = (Integer) httpSession.getAttribute("businessPartnerId");
		playerNamesDetails = serviceinter.allPlayersName(businessPartnerId);
		System.out.println("playerNamesDetails = "+playerNamesDetails);
		return playerNamesDetails;
	}
	
}
