package com.Actioncontroller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Controller
@EnableWebMvc
@RequestMapping(value = "/logout")
class BusinessPartnerLogoutController {

	@RequestMapping(method=RequestMethod.GET)
	public String  handleRequest(HttpServletRequest request, HttpServletResponse response) {

    	HttpSession sessionToBeDestroyed = request.getSession(false);
    	
    	if(sessionToBeDestroyed != null){
    		sessionToBeDestroyed.invalidate();
    	}
    	HttpSession indexPageSession = request.getSession(true);
    	indexPageSession.setAttribute("indexPage", "login");
		return "redirect:/login.html";
	}
}