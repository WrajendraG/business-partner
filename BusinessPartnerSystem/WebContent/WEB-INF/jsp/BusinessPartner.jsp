<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type="text/javascript">
	function reset() {
		$("#firstname").val("");
		$("#lastname").val("");
		$("#username").val("");
		$("#email").val("");
		$("#password").val("");
		//
		$("#conpassword").val("");
		$("#status").val("");
		$("#phone").val("");
		//sscnum  if()
		$("#sscnum").val("");
		$("#address").val("");
		$("#postcode").val("");
		$("#country").val("");
		$("#town").val("");
		$("#language").val("");

	}
	
	$("#document")
	.ready(
			function() {
				
			});
</script>

	
 <div class="col-md-12">

		<h3>BUSINESS PARTNER REGISTRATION</h3>
		
		<form:form method="post" action="register.html" commandName="businessPartnerRegistration">
		<form:input type="hidden" path="status" id="status" value="0"/>
		<div id="tablediv" class="table-responsive"
			style="margin-top: 15px; width: 100%; height: 100%;"></div>
		<div class="panel panel-default" id="content">
			<div class="panel-heading">
				<center>
					<h4>Business Partner Registration</h4>
				</center>
			</div>
			<div class="panel-body">

				<div class="col-sm-3"></div>
				<div class="col-sm-2 "></div>
				<div class="col-sm-3">
					<font size=2 color="red">${message} </font>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>

			<!-- <form:input type="hidden" path="id" id="id" /> -->
			
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>First Name:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="firstname" id="firstname"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="firstname"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Last Name:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="lastname" id="lastname"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="lastname"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>User Name:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="username" id="username"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="username"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Email:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="email" id="email"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="email"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Password:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="password" path="password" id="password"
						class="form-control" minlength="8" maxlength="16" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="password"></form:errors>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					Password should contain upper and lower-case English letters, at least one digit , no spaces and these special characters (@#$%^&+=) are allowed.
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Password Confirmation:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="password" path="conpassword" id="conpassword"
						class="form-control" minlength="8" maxlength="16" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="conpassword"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Agent:</b>
				</div>
				<div class="col-sm-3">
					<div class="checkbox" style="margin: 0px 0px 0px 20px;">
						<form:checkbox path="agent" name="agent" id="agent" />
					</div>
				</div>
				<div class="col-sm-5"></div>
			</div>
			<br>

		
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Affiliate Id:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="affiliateId" id="affiliateId"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="affiliateId"></form:errors>
				</div>
			</div>
			
			
			
			
	
	         <br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Phone Number:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="phone" id="phone"
						class="form-control" maxlength="12" minlength="6" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="phone"></form:errors>
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Language:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="language" id="language"
						class="form-control" maxlength="10" />
				</div>
				<div class="col-sm-5"></div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Social Security Number:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="sscnum" id="sscnum"
						class="form-control" maxlength="40" />
				</div>
				<div class="col-sm-5"></div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Address 1:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="address" id="address"
						class="form-control" maxlength="45" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="address"></form:errors>
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Country Name:</b>
				</div>
				<div class="col-sm-3">
					<form:select path="country" id="country" class="form-control">
						<form:option value="" label="Select Country"></form:option>
						<form:options items="${countryList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="country"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Town:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="town" id="town" class="form-control"
						maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="town"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Postcode:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="text" path="postcode" id="postcode"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="postcode"></form:errors>
				</div>
			</div>
			<br>
			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>

					<input name="register" type="submit" value="Register" class="btn btn-success" id="register" />
					<input name="login" type="submit" value="Go Back To Login Page" class="btn btn-success" id="login" /> 
					
				</center>
			</div>
		</div>
		<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>
				
		</div>
		</center>

	</form:form>
</div>
