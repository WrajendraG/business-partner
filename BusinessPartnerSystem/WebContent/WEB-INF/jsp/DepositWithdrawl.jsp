<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style>
  .ui-autocomplete {
    max-height: 300px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 300px;
  }
  </style>
  
<script type="text/javascript">

$( function() {
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
    $( "#userName" ).autocomplete({
      source: availableTags
    });
  } );
  
  
function submitDeposit()
{
	$("#action").val("D");
	document.getElementById("depositWithdrawlOperation").submit();
};
	
	function submitWithdraw()
	{
		$("#action").val("W");
		document.getElementById("depositWithdrawlOperation").submit();
	};
	
//allPlayersName	
	function myImageFunction()
	{

		var playerName = "";
		  $("#userName").autocomplete({
		       minLength: 0,
		        delay: 500,
		        source: function (request, response) {
		            $.getJSON("allPlayersName.html", {playerName : playerName}, function(result) {

		                response($.map(result, function(item) {
		                    return {
		                    	label: item.players_username
		                    }
		                   
		                }));
		            });
		        },

		        //define select handler
		        select : function(event, ui) {
		            if (ui.item) {
		                event.preventDefault();
		                $("#selected_tags span").append('<a>'+ ui.item.label +'</a>');
		                var defValue = $("#playerId").prop('defaultValue');
		                $("#userName").val(ui.item.label); 
		               $("#userName").blur();
		                return false;
		            }
		        }
		       
		    });
	
	
	};
	
	
	
	
	
	function myFunction()
	{
		var playerName = $("#userName").val();
		  $("#userName").autocomplete({
		        minLength: 1,
		        delay: 500,
		        source: function (request, response) {
		            $.getJSON("playersName.html", {playerName : playerName}, function(result) {

		                response($.map(result, function(item) {
		                    return {
		                    	label: item.players_username
		                    }
		                   
		                }));
		            });
		        },

		        //define select handler
		        select : function(event, ui) {
		            if (ui.item) {
		                event.preventDefault();
		                $("#selected_tags span").append('<a>'+ ui.item.label +'</a>');
		                var defValue = $("#playerId").prop('defaultValue');
		                $("#userName").val(ui.item.label); 
		                $("#userName").blur();
		                return false;
		            }
		        }
		    });
		
	};
	
	function inputFocus(i){
		if(i.value==i.defaultValue){ i.value=""; i.style.color="#000"; }
	}

	function inputBlur(i){
		//var playerName = $("#userName").val();
		
		//if(i.value==""){ i.value=i.defaultValue; i.style.color="#848484"; }
	}
	
	
	$("#document").ready(function(){
		
		
		var getVar= location.search.replace('?', '').split('=');	
		 var getvar2 = getVar.toString();
		 var queryString = new Array();
		 queryString = getvar2.split(",");
		 var players_id = queryString[1];
			
			$.getJSON('depositWithdrwalPlayer.html', {
				players_id : players_id
			}).done(function(data) {
				 $("#userName").val(data[0].players_username);
				$("#amount").val(data[0].players_currency); 
			}); 
		
		
		
	});
	
	$(document).on('click', '#arrow', function() {
		   $('#userName').autocomplete('search', '');
		});
</script>

</head>
<body>
<form:form action="depositWithdrawlOperation.html" method="post" commandName="depositWithdrawl" id="depositWithdrawlOperation">
<form:hidden path="action" id="action" />
<div class="panel panel-default" div id="content">
			<div class="panel-heading">
				<center>
					<h4><spring:message code="label.deposit.withdraw"/></h4>
				</center>
			</div>
			<div class="panel-body">


<div class="row">
					<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
					
						<b><spring:message code="label.user.name"/>:</b>
					</div>
					<div class="col-sm-4" style="position: relative;">
						<form:input path="userName" id="userName" type="text" name="userName" value="" onFocus="inputFocus(this)" onBlur="inputBlur(this)"   onkeyup="myFunction()" class="form-control"/>
					 	<img style="position: absolute;right: 14px;top: 0;height: 33px;" src="http://www.dhtmlgoodies.com/scripts/form_widget_editable_select/images/select_arrow.gif" id="arrow" onclick="myImageFunction()"/>	
					</div> 
					
			
					
					
				</div>
				
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="userName"></form:errors>
					</div>
				</div>
				<br>
				
				<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
					
						<b><spring:message code="label.currency"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="amount" id="amount" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="amount"></form:errors>
					</div>
				</div>
				</div>
				</div>
				
				
				<center>
					<input name="create" type="button" value="<spring:message code="label.deposit"/>" class="btn btn-success" id="insert" onclick="submitDeposit();"/> 
				    <input name="update" type="button" value="<spring:message code="label.withdraw"/>" class="btn btn-success" id="update" onclick="submitWithdraw();"/> 
				</center>
				
</form:form>
</body>
</html>