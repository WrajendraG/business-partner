<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
        <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  document.getElementById("insertCashier").submit(); 
}

$("#document").ready(function(){
	
	
$("#back").click(function(){
		
		window.location.href = document.getElementById("cashierId").href;
	});
	
});
</script>
</head>
<body>
<form:form action="insertCashier.html" method="post" commandName="insertCashier" id="insertCashier">

  <div class="panel panel-default" id="newUserDiv">
			<div class="panel-heading">
				<center><spring:message code="label.cashier"/></center>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div id="errorMessageAddNewPlayer" style="font-size:12px; color:red" class="col-sm-12 text-center">
					${message}
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<form:input type="hidden" path="id" id="id" />
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.first.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="firstname" id="firstname"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="firstname" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.last.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="lastname" id="lastname"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="lastname" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.user.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="username" id="username"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="username" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.email"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="email" id="email"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="email" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.password"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="password" id="password"
						class="form-control" minlength="8" maxlength="16"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="password" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<spring:message code="label.password.policy"/>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
						<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.affiliate.id"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="affiliateId" id="affiliateId"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="affiliateId" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			
			
			
			
			
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.birth.date"/>:</b>
				</div>
				<div class="col-sm-4">
				
				<div class="input-group date" data-provide="datepicker">
    				<form:input type="text" path="bdate" id="bdate" class="form-control" readonly="true" />
    					<div class="input-group-addon">
        			<span class="glyphicon glyphicon-th"></span>
    				</div>
					</div>
						
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="bdate" />
				</div>
				<div class="col-sm-4"></div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.gender"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="gender" id="gender" class="form-control">
						<form:option value="" label="Select Gender"></form:option>
						<form:options items="${array1}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="gender" />
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.currency"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="currency" id="currency" class="form-control">
						<form:option value="" label="Select Currency"></form:option>
						<form:options items="${currencyList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="currency" />
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.country"/> <spring:message code="label.code"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="phonePrefix" id="phonePrefix"
						class="form-control">
						<form:option value="" label="Select Country Code"></form:option>
						<form:options items="${countryPhoneCodeList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="phonePrefix" />
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.phone.number"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="phone" id="phone"
						class="form-control" maxlength="12" minlength="6"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="phone" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.language"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="language" id="language" class="form-control" maxlength="10"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="language" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.social.security.number"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="sscnum" id="sscnum"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="sscnum" />
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.address"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="address" id="address"
						class="form-control" maxlength="50"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="address" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.city"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="City" id="City" class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="City" />
				</div>
			</div>
			<br>



			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.country"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="country" id="country" class="form-control">
						<form:option value="" label="Select Country"></form:option>
						<form:options items="${countryList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="country" />
				</div>
			</div>
			<br>



			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.post.code"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="postcode" id="postcode"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="postcode" />
				</div>
			</div>
			<br>
		</div>
		<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>
             <input	name="create" type="button" value="<spring:message code="label.create"/>" class="btn btn-success" id="insert" onclick="submitForm();"/> 
					
					<input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
</center>
</div>


</form:form>
</body>
</html>