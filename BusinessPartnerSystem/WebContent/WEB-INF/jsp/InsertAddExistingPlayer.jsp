<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
        <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  document.getElementById("addExistingPlayer").submit(); 
}
$("#document").ready(function(){
	
	
	$("#backOnAddExistingPlayer").click(function(){
		
		window.location.href = document.getElementById("Players").href;
	});
	
});

</script>
</head>
<body>
	<form:form method="post" action="insertExistingPlayer.html" commandName="addExistingPlayer" id="addExistingPlayer">
		<div class="panel panel-default" id="addUserDiv">
			<div class="panel-heading">
				<h4><spring:message code="label.add.existing.player"/></h4>
			</div>
			<div class="panel-body">
			
				<div id="errorMessageAddExistingPlayer" style="font-size:12px; color:red" class="col-sm-12 text-center">
					
				</div>
				<br>

				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.user.name"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="userName" id="userName"
							class="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="userName" />
					</div>
				</div>
				<br>

			</div>
		</div>

		<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>

					<input name="addExistingPlayerButton" type="button" value="<spring:message code="label.add"/>" class="btn btn-success" id="addExistingPlayerButton" onclick="submitForm();"/>
					<input name="backOnAddExistingPlayer" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="backOnAddExistingPlayer" />

			</center>
		</div>
	</form:form> 
</body>
</html>