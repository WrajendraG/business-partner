<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  document.getElementById("withdrawFromPlayerOperation").submit(); 
}
$("#document").ready(function(){
	
	$("#back").click(function(){
		window.location.href = document.getElementById("withdrawFromPlayer").href;
	});
});
</script>
</head>
<body>
<form:form method="post" action="insertFromPlayerOperation.html" commandName="withdrawFromPlayer" id="withdrawFromPlayerOperation">

		<div class="panel panel-default" id="content">
			<div class="panel-heading">
				<h4><spring:message code="label.withdraw.from.player"/></h4>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<font size=2 color="red">${message} </font>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<form:input type="hidden" path="id" id="id" />

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.debit"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="currencyId" id="currencyId" class="form-control">
						<form:option value="" label="Select Currency"></form:option>
						<form:options items="${bpTotalBalance}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="currencyId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.player"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="playerId" id="playerId" class="form-control">
						<form:option value="" label="Select Player"></form:option>
						<form:options items="${playersList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="playerId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.amount"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="amountStr" id="amountStr" class="form-control" maxlength="12" />	
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="amountStr" />
				</div>
			</div>

			<br> <br>
			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>
					<input name="create" type="button" value="<spring:message code="label.withdraw"/>" class="btn btn-success" id="insert" onclick="submitForm();"/> 
					<input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
				</center>
			</div>
		</div>

</form:form>
</body>
</html>