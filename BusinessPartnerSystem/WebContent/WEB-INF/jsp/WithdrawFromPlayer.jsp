<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>

<style type="text/css">
#content {
	display: none;
}

#back {
	display: none;
}

#insert {
	display: none;
}
</style>

<script type="text/javascript">
	function reset() {
		$("#currencyId").val("");
		$("#playerId").val("");
		$("amountStr").val("");
	}

	var select = function($this, players_id) {

		var id1 = players_id;
		$.getJSON('playersallvalues.html', {
			id : id1
		}).done(function(data) {

			$("#id").val(data[0].players_id);
			$("#firstname").val(data[0].players_firstname);
			$("#lastname").val(data[0].players_lastname);
			$("#username").val(data[0].players_username);
			$("#email").val(data[0].players_email);
			$("#password").val(data[0].players_password);
			//
			$("#bdate").val(data[0].bdate);
			$("#gender").val(data[0].gender);
			$("#phone").val(data[0].players_phone);

			$("#sscnum").val(data[0].players_sscnum);
			$("#address").val(data[0].players_address);
			$("#postcode").val(data[0].players_postcode);
			$("#country").val(data[0].players_country);
			$("#City").val(data[0].city);
			$("#language").val(data[0].players_language);
		});

	};

	$("#document")
			.ready(
					function() {
						
						var errors = "${isValidationErrorPresentWithDrawFromPlayer}";
						
						if (errors == 'Y') {
							$("#tablediv").hide();
							$("#withdrawFrom").hide();
							$("#content").show();
							$("#back").show();
							$("#insert").show();

							$("#back")
									.click(
											function() {
												
												window.location.href = document
														.getElementById("withdrawFromPlayer").href;
											});

						} else {
							reset();
							    $("#back").click(function() {
								$("#tablediv").show();
								$("#withdrawFrom").show();
								$("#content").hide();
								$("#back").hide();
								$("#insert").hide();
								$("#page-nav").show();
							});

							$("#withdrawFrom").click(function() {
								window.location = 'withdrawlToPlayerOperation.html?';
								/* $("#page-nav").hide();
								$("#insert").show();
								$("#back").show();
								$("#content").show();
								$("#tablediv").hide();
								$("#withdrawFrom").hide(); */
							});

														
							$('#currencyId').change(function(e){
							       var currencyValue=$('#currencyId').val();
							      
							       $.getJSON('playersListAccordingToCurrency.html', {currencyValue:currencyValue}).done(function(data) {
							    	   
							       });
							       
							       
							    });
							
						}
					});
</script>


 <div class="col-md-12">

		<h3><spring:message code="label.heading.withdraw.from.player"/></h3>
		
		<form:form method="post" action="withdrawFromPlayerOperation.html" commandName="withdrawFromPlayer">
		<c:url value="/withdrawFromPlayer.html" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>
		<div id="tablediv" class="table-responsive" style="margin-top: 15px; width: 100%; height: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
			<tr class='success' align ='left'><th><spring:message code="label.player"/></th><th><spring:message code="label.currency"/></th><th><spring:message code="label.debit"/></th><th><spring:message code="label.date"/></th></tr>
			<fmt:setLocale value="en_US" scope="session"/>
			<c:forEach items="${pagedListHolder.pageList}" var="item">
				<tr align='left' class='info'>
					<td>${item.players_username}</td>
					<td>${item.currency_name}</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.withdraw_amount}" /></td>
					<td>${item.created_on}</td>
				</tr>
			</c:forEach>
			</table>		
		
		</div>
		<div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>

		<%-- <div class="panel panel-default" id="content">
			<div class="panel-heading">
				<h4><spring:message code="label.withdraw.from.player"/></h4>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<font size=2 color="red">${message} </font>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<form:input type="hidden" path="id" id="id" />

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.debit"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="currencyId" id="currencyId" class="form-control">
						<form:option value="" label="Select Currency"></form:option>
						<form:options items="${bpTotalBalance}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="currencyId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.player"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="playerId" id="playerId" class="form-control">
						<form:option value="" label="Select Player"></form:option>
						<form:options items="${playersList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="playerId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.amount"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="amountStr" id="amountStr" class="form-control" maxlength="12" />	
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="amountStr" />
				</div>
			</div>

			<br> <br>
			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>
					<input name="create" type="submit" value="<spring:message code="label.withdraw"/>" class="btn btn-success" id="insert" /> 
					<input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
				</center>
			</div>
		</div>
 --%>		
            <div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>
				<input name="withdrawFromPlayer" type="button"
					value="<spring:message code="label.withdraw.from.player"/>" class="btn btn-success" id="withdrawFrom" />
			</center>
		</div>

	</form:form>
	</div>
