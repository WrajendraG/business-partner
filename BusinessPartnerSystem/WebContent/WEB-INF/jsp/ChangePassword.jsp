<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

$("#document").ready(function(){
	

	   $("#back").click(function() {
	    	window.location.href = document.getElementById("menudashboard").href;
	    }); 	
	
});

</script>
</head>
<body>
<form:form action ="resetPassword.html" method="post" commandName="forgotPassword">
	<div class="panel panel-default" id="newUserDiv">
	<br><br>
			<div class="panel-heading">
				<center><spring:message code="label.change.password"/></center>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<div id="errorMessageBusinessPartner" style="font-size:12px; color:red" class="col-sm-12 text-center">${message}</div>
					</div>
					<div class="col-sm-4"></div>
				</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.old.password"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="oldPassword" id="oldPassword"
						class="form-control" minlength="8" maxlength="16"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="oldPassword" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.new.password"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="newPassword" id="newPassword"
						class="form-control" minlength="8" maxlength="16"/>
				</div>
			</div>
			<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4"><spring:message code="label.password.policy"/></div>
			<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="newPassword" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.confirm.password"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="confirmPassword" id="confirmPassword"
						class="form-control" minlength="8" maxlength="16"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="confirmPassword" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			
			</div>
			</div>
			<center>
			<input name="changePassword" type="submit" value="<spring:message code="label.change.password"/>" class="btn btn-success" id="changePassword" />
            <input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
            </center>		
		</form:form>	
</body>
</html>