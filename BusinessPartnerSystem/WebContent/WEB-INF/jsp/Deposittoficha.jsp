<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>

<style type="text/css">
#content {
	display: none;
}

#create {
	display: none;
}

#back {
	display: none;
}
</style>
<script type="text/javascript">
	function reset() {
		$("#currency").val("");
		$("#payment").val("");
		$("#amount").val("");
	}

	$("#document").ready(function() {
						//"${isValidationErrorPresentDistributiontoPlayer}"
						var errors = "${isValidationErrorPresentDepositFicha}"
						if (errors == 'Y') {
							$("#add").hide();
							$("#back").show();
							$("#tablediv").hide();
							$("#content").show();
							$("#create").show();
							$("#back")
									.click(
											function() {
												window.location.href = document
														.getElementById("Deposittoficha").href;
											});
						} else {
							reset();
							$("#add").click(function() {
								window.location = 'bpDepositToFichaInsert.html?';
								/* reset();
								$("#add").hide();
								$("#back").show();
								$("#tablediv").hide();
								$("#content").show();
								$("#create").show();
								$("#page-nav").hide(); */
							});

							$("#back").click(function() {
								/* reset(); */
								$("#add").show();
								$("#back").hide();
								$("#tablediv").show();
								$("#content").hide();
								$("#create").hide();
								$("#page-nav").show();
							});
                            //alert("username = "+username);
							var username = "${currentusername}"
							var businessPartnerId = "${businessPartnerId}"

							$("#businessPartner").append("" + username);
							$("#username").append("" + username);


						}

					});
</script>

 <div class="col-md-12">
	<form:form method="post" action="depositfi.html" commandName="depositficha">
		
			<h3><spring:message code="label.heading.deposit.to.ficha"/></h3>

		<c:url value="/Deposittoficha.html" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>
		<div id="tablediv" class="table-responsive" style="margin-top: 15px; width: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
			<tr class='success' align ='left'><th><spring:message code="label.business.partner"/></th><th><spring:message code="label.status"/></th><th><spring:message code="label.payment"/></th><th><spring:message code="label.currency"/></th><th><spring:message code="label.amount"/></th></tr>
			<fmt:setLocale value="en_US" scope="session"/>
			<c:forEach items="${pagedListHolder.pageList}" var="item">
				<tr align='left' class='info'>
					<td>${item.business_firstname}</td>
					<td>
						<c:if test="${item.status == 0}">Pending</c:if>
						<c:if test="${item.status == 1}">Confirmed</c:if>
						<c:if test="${item.status == 2}">Rejected</c:if>
					</td>
					<td>${item.payment_name}</td>
					<td>${item.currency_name}</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.amount}" /></td>
				</tr>
			</c:forEach>
			</table>		
		</div>
		<div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>

		<div id="page-nav"></div>
		
		<%-- <div class="panel panel-default" id="content">
			<div class="panel-heading">
				<h4><spring:message code="label.deposit.to.ficha"/></h4>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<font size=2 color="red">${message} </font>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>



			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.business.partner"/>:</b>
				</div>
				<div class="col-sm-4">
					<div id="businessPartner"></div>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.currency"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="currencyId" id="currency" class="form-control">
						<form:option value="" label="Select type"></form:option>
						<form:options items="${Currency}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="currencyId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.payment"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="paymentId" id="payment" class="form-control">
						<form:option value="" label="Select type"></form:option>
						<form:options items="${payment}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="paymentId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.amount"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="amountStr" id="amount" class="form-control" maxlength="12" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="amountStr" />
				</div>
			</div>
			<br>
	

		<br>
		<br>
		<div class="btn-group btn-group-justified" style="margin-bottom: 15px;">
			<center>
				<input name="create" type="submit" value="<spring:message code="label.deposit"/>" class="btn btn-success" id="create" /> 
				<input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" /> 
			
			</center>
		</div>
		</div>
 --%>	
 	<div class="btn-group btn-group-justified" style="margin-bottom: 15px;">
			<center>
				 <input name="add" type="button" value="<spring:message code="label.new.deposit"/>" class="btn btn-success" id="add" />
			</center>
		</div>
	</form:form>
</div>
