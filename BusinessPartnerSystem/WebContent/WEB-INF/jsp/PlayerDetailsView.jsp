<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

$("#document").ready(function() {
	
	var getVar= location.search.replace('?', '').split('=');	
	 var getvar2 = getVar.toString();
	 var queryString = new Array();
	 queryString = getvar2.split(",");
	 var players_id = queryString[1];
		
		$.getJSON('depositWithdrwalPlayer.html', {
			players_id : players_id
		}).done(function(data) {
			
			$("#playerInformation").append("<b><spring:message code="label.first.name"/> : "+data[0].players_firstname +"</b><br><br><b><spring:message code="label.last.name"/> : "+data[0].players_lastname+"</b><br><br><b><spring:message code="label.user.name"/> : "+data[0].players_username+"</b><br><br><b><spring:message code="label.email"/> : "+data[0].players_email+"</b><br><br><b><spring:message code="label.country"/> : "+data[0].players_country+"</b><br><br><b><spring:message code="label.address"/> : "+data[0].players_address+"</b><br><br><b><spring:message code="label.phone.number"/> : "+data[0].players_phone+"</b><br><br><b><spring:message code="label.post.code"/> : "+data[0].players_postcode+"</b></div>"); 
		}); 	
	
		
		/* 
		label.code=DDD
		label.phone.number=N�mero de telefone
		label.language=Idioma
		label.social.security.number=CPF
		label.address=Endere�o
		label.city=Cidade
		label.post.code=CEP */
		
});
</script>
</head>
<body>
<div class="panel panel-default">
    <div class="panel-heading"><center><h4><spring:message code="label.player.details"/></h4></center></div>
    <div class="panel-body">
    <div id="playerInformation" class="table-responsive" style=" margin-top: 15px; width:100%;height:100%;"></div>
  
</body>
</html>