<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  document.getElementById("distributeToPlayerOperation").submit(); 
};
function myFunction()
{

	var playerName = $("#playerId").val();
	
	  $("#playerId").autocomplete({
	        minLength: 1,
	        delay: 500,
	        //define callback to format results
	        source: function (request, response) {
	            $.getJSON("playersName.html", {playerName : playerName}, function(result) {

	                response($.map(result, function(item) {
	                    return {
	                    	label: item.players_username
	                    }
	                   
	                }));
	            });
	        },

	        //define select handler
	        select : function(event, ui) {
	            if (ui.item) {
	                event.preventDefault();
	                $("#selected_tags span").append('<a>'+ ui.item.label +'</a>');
	                var defValue = $("#playerId").prop('defaultValue');
	                $("#playerId").val(ui.item.label); 
	                $("#playerId").blur();
	                return false;
	            }
	        }
	    });
	
};

$("#document").ready(function() {
	
	$("#back").click(function(){
		window.location.href = document.getElementById("distributeToPlayer").href;
	});	
	
});

function inputFocus(i){
	if(i.value==i.defaultValue){ i.value=""; i.style.color="#000"; }
}

function inputBlur(i){
	var playerName = $("#playerId").val();
	
	if(i.value==""){ i.value=i.defaultValue; i.style.color="#848484"; }
}



/* 
$("#document").ready(function(){
	
	
	
	
}); */

/* function myFunction() {
	alert("value of player Id = ");
}; */
</script>
</head>
<body>
<form:form method="post" action="insertDepositToPlayerOperation.html" commandName="depositToPlayer" id="distributeToPlayerOperation">
 <div class="panel panel-default" id="content">
			<div class="panel-heading">
				<h4><spring:message code="label.deposit.to.players"/></h4>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<font size=2 color="red">${message} </font>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<form:input type="hidden" path="id" id="id" />

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.credit"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="currencyId" id="currencyId" class="form-control">
						<form:option value="" label="Select Currency"></form:option>
						<form:options items="${bpTotalBalance}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="currencyId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.player"/>:</b>
				</div>
				<!--  -->
				<div class="col-sm-4">
				<%-- <form:input type="text" path="playerId" id="playerId" value ="" class="form-control" onkeyup="myFunction();"/> --%>
				
				<form:input path="playerId" id="playerId" type="text" name="playerId" value="" onFocus="inputFocus(this)" onBlur="inputBlur(this)"   onkeyup="myFunction()"  onfocus="myFunction()" class="form-control"/>
			    
				<!--  -->
				
				
					<%-- <form:select path="playerId" id="playerId" class="form-control">
						<form:option value="" label="Select Player"></form:option>
						<form:options items="${playersList}"></form:options>
					</form:select> --%>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="playerId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.amount"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="amountStr" id="amountStr" class="form-control" maxlength="12" />	
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="amountStr" />
				</div>
			</div>
 <br> <br>
			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>
					<input name="create" type="button" value="<spring:message code="label.deposit"/>" class="btn btn-success" id="insert" onclick="submitForm();"/> 
					<input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
				</center>
			</div>
		</div>
	
 </form:form>
</body>
</html>