<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
$("#document").ready(function() {	
	$("#back").click(function() {
		//window.location.href = document.getElementById("menudashboard").href;
		window.location = 'login.html?'
	});	
		});
</script>
</head>
<body>
	<form:form method="post" action="forgotPasswordActivationAction.html" commandName="forgotPasswordCredentials">
	
	
	<div class="panel panel-default" id="forgotPasswordDiv">
			<div class="panel-heading">
				<center>Forgot Password</center>
			</div>
			<div class="panel-body">
	<div id="errorMessageResetPassword" style="font-size:12px; color:red" class="col-sm-12 text-center">${message}</div>
		<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b>User Name:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="userName" id="userName"
						class="form-control" maxlength="40" />
				</div>
				<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="userName"></form:errors>
				</div>
			</div>
			</div>
			<br>
	<div class="btn-group btn-group-justified" style="margin-bottom: 15px;">
				<center>
					<input name="forgotPasswordActivationButton" type="submit" value="Ok" class="btn btn-success" id="forgotPasswordActivationButton" />
					<input name="back" type="button" value="Back" class="btn btn-primary" id="back" /> 
	</center>
	</div>
	</form:form>
</body>
</html>