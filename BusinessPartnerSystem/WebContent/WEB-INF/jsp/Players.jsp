<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>

<style type="text/css">
#addUserDiv {
	display: none;
}

#insert {
	display: none;
}

#back {
	display: none;
}

#addExistingPlayerButton {
	display: none;
}

#backOnAddExistingPlayer {
	display: none;
}

#newUserDiv {
	display: none;
}

#playerInformation
{
display: none;

}

#labelID
{
float:left;
}

#valueID
{
float:right;
}
</style>

<script type="text/javascript">
	
	
	
	
	
	
	
	
	
	function reset() {
		$("#firstname").val("");
		$("#lastname").val("");
		$("#username").val("");
		$("#email").val("");
		$("#password").val("");
		//
		$("#bdate").val("");
		$("#gender").val("");
		$("#phone").val("");

		$("#sscnum").val("");
		$("#address").val("");
		$("#postcode").val("");
		$("#country").val("");
		$("#City").val("");
		$("#language").val("");
		$("#currency").val("");
		$("#phonePrefix").val("");
	}

	
	var block= function($this, players_id)
	{
		alert("playerBlock = "+players_id);
	};
	
	
	var detail = function($this, players_id){
		//depositWithdrawl
		/* window.location = 'playerDetail.html?players_id=' + players_id;	 */
		
		$.getJSON('depositWithdrwalPlayer.html', {
			players_id : players_id
		}).done(function(data) {
			$('#playerInformation').html("");
			$('#playerInformation').append( "<table class= 'table table-bordered'  table id='tablec'>");
			var table = $('#playerInformation').children();
			$("#playerInformation").append("<tr><td style='text-align: right;'><spring:message code="label.first.name"/> :</td><td style='text-align: left; padding-right: 20px;'> "+data[0].players_firstname +"</td></tr><tr><td style='text-align: right;'><spring:message code="label.last.name"/> :</td><td style='text-align: left; padding-right: 20px;'> "+data[0].players_lastname+"</td></tr><tr><td style='text-align: right;'><spring:message code="label.user.name"/> :</td><td style='text-align: left; padding-right: 20px;'> "+data[0].players_username+"</td></tr><tr><td style='text-align: right;'><spring:message code="label.email"/> : </td><td style='text-align: left; padding-right: 20px;'>"+data[0].players_email+"</td></tr><tr><td style='text-align: right;'><spring:message code="label.country"/> :</td><td style='text-align: left; padding-right: 20px;'> "+data[0].players_country+"</td></tr><tr><td style='text-align: right;'><spring:message code="label.address"/> :</td><td style='text-align: left; padding-right: 20px;'> "+data[0].players_address+"</td></tr><tr><td style='text-align: right;'><spring:message code="label.phone.number"/> :</td><td style='text-align: left; padding-right: 20px;'> "+data[0].players_phone+"</td></tr></div>"); 
		});
		
		$("#playerInformation").css({"display":"block"});
		$("#playerDetailsId").css({"display":"block"});

		
		
	};
	

	
	var depositWithdrwal = function($this, players_id){
		
		window.location = 'depositWithdrawl.html?players_id=' + players_id;
		//alert("playerDepositWithdrwal = "+players_id);
	};
	
	
	var select = function($this, players_id) {

		var id1 = players_id;
		$.getJSON('playersallvalues.html', {
			id : id1
		}).done(function(data) {

			$("#id").val(data[0].players_id);
			$("#firstname").val(data[0].players_firstname);
			$("#lastname").val(data[0].players_lastname);
			$("#username").val(data[0].players_username);
			$("#email").val(data[0].players_email);
			$("#password").val(data[0].players_password);
			$("#bdate").val(data[0].bdate);
			$("#gender").val(data[0].gender);
			$("#phone").val(data[0].players_phone);
			$("#sscnum").val(data[0].players_sscnum);
			$("#address").val(data[0].players_address);
			$("#postcode").val(data[0].players_postcode);
			$("#country").val(data[0].players_country);
			$("#City").val(data[0].city);
			$("#language").val(data[0].players_language);
		});

	};

	$("#document")
			.ready(
					function() {
						
						
						
						$("#cancel").click(function(){

							$("#playerInformation").css({"display":"none"});
							$("#playerDetailsId").css({"display":"none"});
					   });
						
						
                        var isAgent="${isAgent}"; 
                       
						var errors = "${isValidationErrorPresentPlayer}";
						var errorPlayerAlreadyExists = "${isPlayerAlreadyExistsForAddExistingPlayer}";

						if (errors == 'Y') {
							$("#errorMessageAddNewPlayer").html("${message}");
							$("#tablediv").hide();
							$("#tablediv1").hide();
							$("#newUserDiv").show();
							$("#addUserDiv").hide();
							$("#insert").show();
							$("#back").show();
							$("#backOnAddExistingPlayer").hide();
							$("#new").hide();
							$("#add").hide();
							$("#back")
									.click(
											function() {
												window.location.href = document
														.getElementById("Players").href;
											});
						} else	if (errorPlayerAlreadyExists == 'Y') {
							$("#errorMessageAddExistingPlayer").html("${message}");	
							$("#tablediv").hide();
							$("#tablediv1").hide();
							$("#newUserDiv").hide();
							$("#addUserDiv").show();
							$("#insert").hide();
							$("#back").hide();
							$("#backOnAddExistingPlayer").show();
							$("#new").hide();
							$("#add").hide();
							$("#addExistingPlayerButton").show();
							$("#backOnAddExistingPlayer")
									.click(
											function() {
												window.location.href = document
														.getElementById("Players").href;
											});
						} else {
							reset();
							
							
							
							
							$("#new").click(function() {
								
								window.location = 'newPlayerInsert.html?';
								
								/* reset();
								$("#tablediv").hide();
								$("#tablediv1").hide();
								$("#newUserDiv").show();
								$("#addUserDiv").hide();
								$("#insert").show();
								$("#back").show();
								$("#new").hide();
								$("#add").hide();
								$("#backOnAddExistingPlayer").hide();
								$("#addExistingPlayerButton").hide();
								$("#page-nav").hide();
								$("errorMessageAddNewPlayer").hide(); */
								
							});

							$("#add").click(function() {
								
								window.location = 'addExistingPlayer.html?';
								
								/* $("#tablediv").hide();
								$("#tablediv1").hide();
								$("#addUserDiv").show();
								$("#newUserDiv").hide();
								$("#back").hide();
								$("#addExistingPlayerButton").show();
								$("#backOnAddExistingPlayer").show();
								$("#add").hide();
								$("#new").hide();
								$("#page-nav").hide(); */
							});

							$("#back").click(function() {
								$("#tablediv").show();
								$("#tablediv1").show();
								$("#newUserDiv").hide();
								$("#addUserDiv").hide();
								$("#insert").hide();
								$("#back").hide();
								$("#backOnAddExistingPlayer").hide();
								$("#new").show();
								$("#addExistingPlayerButton").hide();
								$("#add").show();
								$("#page-nav").show();

							});

							$("#backOnAddExistingPlayer").click(function() {
								$("#tablediv").show();
								$("#tablediv1").show();
								$("#newUserDiv").hide();
								$("#addUserDiv").hide();
								$("#insert").hide();
								$("#back").hide();
								$("#backOnAddExistingPlayer").hide();
								$("#new").show();
								$("#addExistingPlayerButton").hide();
								$("#add").show();
								$("#page-nav").show();

							});
							
														
							
							$('#srch-term').bind('keyup', function(e) {

							    if ( e.keyCode === 13 ) { // 13 is enter key
							    	//alert("enter button clicked ");
							    	var playerUserName=$("#srch-term").val();
							    	//alert("enter button clicked "+playerUserName);
									if(playerUserName!="")
									{
										window.location.href = document.getElementById("Players").href+"?"+$.param({'username':playerUserName});
									}
								else
									{
										window.location.href = document.getElementById("Players").href;
									}

							    }

							});
							
							$("#search").click(function() {
								var playerUserName=$("#srch-term").val();
								
								
								//CheckBussinessPartnerFromusername
								if(playerUserName!="")
									{
										window.location.href = document.getElementById("Players").href+"?"+$.param({'username':playerUserName});
									}
								else
									{
										window.location.href = document.getElementById("Players").href;
									}


					});
									
							$('#bdate').datepicker({
							    format: 'dd/mm/yyyy',
							    autoclose : true,
							    clearBtn : true,
							    todayHighlight : true
							});

							

						}

					});
</script>

 <div class="col-md-12">
	
		
			<h3><spring:message code="label.heading.players"/></h3>
			
		<div class="row">
			<div class="col-sm-3 col-md-3 pull-right" id="tablediv1">
				
					<div class="input-group">
						<input type="text" class="form-control" placeholder="<spring:message code="label.search.by.user.name"/>"
							name="srch-term" id="srch-term" maxlength="50">
						<div class="input-group-btn">
							<button style="height: 34px;" class="btn btn-info" type="button" id="search">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				
			</div>

		</div>
			
			
		<form:form method="post" action="playersoper.html" commandName="players">	
		
		
		
		 <div id="playerDetailsId" class="modal" role="dialog">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-body" style="height: 400px">
	                    <div class="form-group" id="playerInformation"> 	              
						</div>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" id="cancel" class="btn btn-primary" data-dismiss="modal"><spring:message code="label.cancel"/></button>
	                </div>
	            </div>
	        </div>
    	</div>	
		
		
		
		
		<c:url value="/Players.html?username=${searchKey}" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>			
		  <%
								Boolean isAgent = (Boolean) session.getAttribute("isAgent");
								if (isAgent==true) {
									
							  %>
		<div id="tablediv" class="table-responsive" style="margin-top: 15px; width: 100%; height: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
			<tr class='success' align ='left'><c:if test="${sessionScope.isAgent == true}"><th><spring:message code="label.agent"/></th></c:if><th><spring:message code="label.first.name"/></th><th><spring:message code="label.last.name"/></th><th><spring:message code="label.user.name"/></th><th><spring:message code="label.block"/></th><th><spring:message code="label.details"/></th><th><spring:message code="label.deposit.withdraw"/></th></tr>
			<c:forEach items="${pagedListHolder.pageList}" var="item">
				<tr align='left' class='info'>
					<c:if test="${sessionScope.isAgent == true}"><td>${item.business_username}</td></c:if>
					<%--  <td>${item.players_id}</td>  --%>
					<td>${item.players_firstname}</td>
					<td>${item.players_lastname}</td>
					<td>${item.players_username}</td>
					<%--<td>${item.players_email}</td>
					 <td>${item.players_address}</td>
					<td>${item.players_country}</td>
					<td>${item.city}</td>
					<td>${item.players_postcode}</td> --%>
					<td><button name='playerBlock'  type='button' class='btn btn-danger' onclick='block(this,${item.players_id})'><spring:message code="label.block"/></button></td>
					<td><button name='playerDetail' type='button' class='btn btn-primary' onclick='detail(this,${item.players_id})'><spring:message code="label.details"/></button></td>
					<td><button name='playerDepositWithdrwal' type='button' class='btn btn-success' onclick='depositWithdrwal(this,${item.players_id})'><spring:message code="label.deposit.withdraw"/></button></td>

				</tr>
			</c:forEach>
			</table>		
		</div>
			<%
			}else{
				
			%>
			
			<div id="tablediv" class="table-responsive" style="margin-top: 15px; width: 100%; height: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
			<tr class='success' align ='left'><c:if test="${sessionScope.isAgent == false}"></c:if><th><spring:message code="label.first.name"/></th><th><spring:message code="label.user.name"/></th><th><spring:message code="label.country"/></th></th></tr>
			<c:forEach items="${pagedListHolder.pageList}" var="item">
				<tr align='left' class='info'>
					<c:if test="${sessionScope.isAgent == true}"><td>${item.business_username}</td></c:if>
					<td>${item.players_firstname}</td>
					<td>${item.players_username}</td>
					<td>${item.players_country}</td>
				</tr>
			</c:forEach>
			</table>		
		</div>
			<%	
			}
			%>
			
		<div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>


	
<%-- 		



<div class="panel panel-default" id="newUserDiv">
			<div class="panel-heading">
				<center><spring:message code="label.players"/></center>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div id="errorMessageAddNewPlayer" style="font-size:12px; color:red" class="col-sm-12 text-center">
					${message}
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<form:input type="hidden" path="id" id="id" />
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.first.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="firstname" id="firstname"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="firstname" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.last.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="lastname" id="lastname"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="lastname" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.user.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="username" id="username"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="username" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.email"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="email" id="email"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="email" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.password"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="password" id="password"
						class="form-control" minlength="8" maxlength="16"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="password" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<spring:message code="label.password.policy"/>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
						<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.affiliate.id"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="affiliateId" id="affiliateId"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="affiliateId" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			
			
			
			
			
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.birth.date"/>:</b>
				</div>
				<div class="col-sm-4">
				
				<div class="input-group date" data-provide="datepicker">
    				<form:input type="text" path="bdate" id="bdate" class="form-control" readonly="true" />
    					<div class="input-group-addon">
        			<span class="glyphicon glyphicon-th"></span>
    				</div>
					</div>
						
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="bdate" />
				</div>
				<div class="col-sm-4"></div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.gender"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="gender" id="gender" class="form-control">
						<form:option value="" label="Select Gender"></form:option>
						<form:options items="${array1}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="gender" />
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.currency"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="currency" id="currency" class="form-control">
						<form:option value="" label="Select Currency"></form:option>
						<form:options items="${currencyList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="currency" />
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.country"/> <spring:message code="label.code"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="phonePrefix" id="phonePrefix"
						class="form-control">
						<form:option value="" label="Select Country Code"></form:option>
						<form:options items="${countryPhoneCodeList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="phonePrefix" />
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.phone.number"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="phone" id="phone"
						class="form-control" maxlength="12" minlength="6"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="phone" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.language"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="language" id="language" class="form-control" maxlength="10"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="language" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.social.security.number"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="sscnum" id="sscnum"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="sscnum" />
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.address"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="address" id="address"
						class="form-control" maxlength="50"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="address" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.city"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="City" id="City" class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="City" />
				</div>
			</div>
			<br>



			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.country"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="country" id="country" class="form-control">
						<form:option value="" label="Select Country"></form:option>
						<form:options items="${countryList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="country" />
				</div>
			</div>
			<br>



			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.post.code"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="postcode" id="postcode"
						class="form-control" maxlength="40"/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="postcode" />
				</div>
			</div>
			<br>
		</div>
		<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center> --%>
			<center>
			                 <%
								if (isAgent==true) {
									System.out.println("agent condition");
							  %>
					<input name="new" type="button" value="<spring:message code="label.register.new.player"/>"	class="btn btn-success" id="new" />
					<%
					}
					%>
					<input name="add" type="button"	value="<spring:message code="label.add.existing.player"/>" class="btn btn-success" id="add" /> 
				
					
				<%-- 	<input	name="create" type="submit" value="<spring:message code="label.create"/>" class="btn btn-success" id="insert" /> 
					
					<input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" /> --%>

			</center>
		</div>
	</form:form>
<%-- 	<form:form method="post" action="addExistingPlayer.html" commandName="addExistingPlayer">
		<div class="panel panel-default" id="addUserDiv">
			<div class="panel-heading">
				<h4><spring:message code="label.add.existing.player"/></h4>
			</div>
			<div class="panel-body">
			
				<div id="errorMessageAddExistingPlayer" style="font-size:12px; color:red" class="col-sm-12 text-center">
					
				</div>
				<br>

				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.user.name"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="userName" id="userName"
							class="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="userName" />
					</div>
				</div>
				<br>

			</div>
		</div>

		<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>

					<input name="addExistingPlayerButton" type="submit" value="<spring:message code="label.add"/>" class="btn btn-success" id="addExistingPlayerButton" />
					<input name="backOnAddExistingPlayer" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="backOnAddExistingPlayer" />

			</center>
		</div>
	</form:form> --%>


	</div>
