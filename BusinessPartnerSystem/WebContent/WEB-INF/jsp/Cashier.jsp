<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
	window.location = 'cashierInsert.html?';
};
	
$("#document").ready(function(){
	
	
});
</script>
</head>
<body>
<form:form action="" method="post" commandname="">  


 <div class="col-md-12">

			<h3><spring:message code="label.cashier"/></h3>
		
		<div class="row">
			<div class="col-sm-3 col-md-3 pull-right" id="tablediv1">
				
					<div class="input-group">
						<input type="text" class="form-control" placeholder="<spring:message code="label.search.by.user.name"/>"
							name="srch-term" id="srch-term" maxlength="50">
						<div class="input-group-btn">
							<button style="height: 34px;" class="btn btn-info" type="button" id="search">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				
			</div>

		</div>



<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>
				<input name="newCashier" type="button" value="<spring:message code="label.new"/>" class="btn btn-success" id="newCashier" onclick="submitForm();"/>
			</center>
		</div>
</form:form>
</body>
</html>