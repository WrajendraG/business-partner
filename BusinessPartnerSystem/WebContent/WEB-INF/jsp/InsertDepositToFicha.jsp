<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  document.getElementById("insertDepositToFichaBp").submit(); 
}
$("#document").ready(
		function() {
			
			var username = "${currentusername}"
				var businessPartnerId = "${businessPartnerId}"

				$("#businessPartner").append("" + username);
				$("#username").append("" + username);
			
			$("#back").click(function(){
				
				window.location.href = document.getElementById("Deposittoficha").href;
			});
			
			
			
			$("#")
			
		});
</script>
</head>
<body>
<form:form method="post" action="insertDepositToFichaBp.html" commandName="depositficha" id="insertDepositToFichaBp">

  <div class="panel panel-default" id="content">
			<div class="panel-heading">
				<h4><spring:message code="label.deposit.to.ficha"/></h4>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<%-- <font size=2 color="red">${message} </font> --%>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>



			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.business.partner"/>:</b>
				</div>
				<div class="col-sm-4">
					<div id="businessPartner"></div>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.currency"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="currencyId" id="currency" class="form-control">
						<form:option value="" label="Select type"></form:option>
						<form:options items="${Currency}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="currencyId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.payment"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="paymentId" id="payment" class="form-control">
						<form:option value="" label="Select type"></form:option>
						<form:options items="${payment}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="paymentId" />
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.amount"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="amountStr" id="amount" class="form-control" maxlength="12" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="amountStr" />
				</div>
			</div>
			<br>
	

		<br>
		<br>
		<div class="btn-group btn-group-justified" style="margin-bottom: 15px;">
			<center>
				<input name="create" type="button" value="<spring:message code="label.deposit"/>" class="btn btn-success" id="create" onclick="submitForm();"/> 
				<input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" /> 
			
			</center>
		</div>
		</div>
		<%-- <div class="btn-group btn-group-justified" style="margin-bottom: 15px;">
			<center>
				 <input name="add" type="button" value="<spring:message code="label.new.deposit"/>" class="btn btn-success" id="add" />
			</center>
		</div> --%>
  
</form:form>
</body>
</html>