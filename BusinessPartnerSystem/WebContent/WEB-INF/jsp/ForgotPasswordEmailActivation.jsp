<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


	
 <div class="col-md-12">

		<h3>RESET PASSWORD</h3>
		<form:form method="post" action="forgotPasswordEmailActivation.html" commandName="forgotPasswordEmail">
		<form:input type="hidden" path="userName" id="userName"/>
		<form:input type="hidden" path="verificationCode" id="verificationCode"/>
		
		<div class="panel panel-default" id="content">
			<div class="panel-heading">
				<center>
					<h4>Change Password </h4>
				</center>
			</div>
			<div class="panel-body">

            <div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div id="errorMessageResetPassword" style="font-size:12px; color:red" class="col-sm-12 text-center">${message}</div>
				<div class="col-sm-4"></div>

				<div class="col-sm-3"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-3">
					<font size=2 color="red"><form:errors path="userName"></form:errors></font>
					<font size=2 color="red"><form:errors path="verificationCode"></form:errors></font>
				</div>
				<div class="col-sm-5"></div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Password:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="password" path="resetPassword" id="resetPassword" class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="resetPassword"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-2">
					<b>Confirm Password:</b>
				</div>
				<div class="col-sm-3">
					<form:input type="password" path="confirmResetPassword" id="confirmResetPassword" class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5"></div>
				<div class="col-sm-5 errorMsg">
					<form:errors path="confirmResetPassword"></form:errors>
				</div>
			</div>
			<br>

			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>
					<input name="reset" type="submit" value="Reset" class="btn btn-success" id="reset" />
				</center>
			</div>
		</div>
		<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>
				
		</div>
		</center>

	</form:form>
</div>
</body>
</html>