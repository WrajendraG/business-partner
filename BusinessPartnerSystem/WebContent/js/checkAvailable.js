function searchDisplayTable(inputVal, inputId,table, col1, col2) {
		        var consumableName="";
		        var consumableId=0;
			        for (var r = 0, n = table.rows.length; r < n; r++) 
			        {
		        	
		                if(table.rows[r].cells[col2].innerHTML ==inputVal)
		                {
		                	consumableName=table.rows[r].cells[col2].innerHTML;
		                	consumableId=table.rows[r].cells[col1].innerHTML;
		                	break;
		                }        
			        }
			        consumableName=consumableName.toLowerCase();
			        inputVal=inputVal.toLowerCase();
			        if(inputId==0){
                			if (consumableName==inputVal){
		                		document.getElementById('errorDisplay').style.visibility = "visible" ;
		                		document.getElementById("save").disabled = true;
                			}
                			else
                			{
                				document.getElementById("save").disabled = false;
        						document.getElementById('errorDisplay').style.visibility = "hidden" ;
                			}
                		}
                		else if(inputId==consumableId)
                		{
                			document.getElementById("save").disabled = true;
                			if (consumableName==inputVal)
                			{
                				document.getElementById("update").disabled = false;
                				document.getElementById("delete").disabled = false;
                				document.getElementById('errorDisplay').style.visibility = "hidden" ;
                			}
                			else
                			{
                				document.getElementById("update").disabled = true;
                				document.getElementById("delete").disabled = true;
        						document.getElementById('errorDisplay').style.visibility = "visible" ;
                			}
                		}
                    	else 
    	                {
    	                	document.getElementById("save").disabled = true;
    	                	if (consumableName==inputVal)
                			{
    	                		document.getElementById("update").disabled = true;
    	                		document.getElementById("delete").disabled = true;
        						document.getElementById('errorDisplay').style.visibility = "visible" ;
                			}
                			else
                			{
                				document.getElementById("update").disabled = false;
                				document.getElementById("delete").disabled = true;
                				document.getElementById('errorDisplay').style.visibility = "hidden" ;
                			}
    					}
                	
		    }


/*--------------------Search for chemical type Master-----------------------*/

function searchChemType(inputVal, inputId,chemtypeid,table, col1, col2) {
    var consumableName="";
    var consumableId=0;

        for (var r = 0, n = table.rows.length; r < n; r++) 
        {
    	
            if(table.rows[r].cells[col2].innerHTML ==inputVal && table.rows[r].cells[col1].innerHTML==inputId)
            {
            	consumableName=table.rows[r].cells[col2].innerHTML;
            	consumableId=table.rows[r].cells[col1].innerHTML;
            	chem_type_id=table.rows[r].cells[0].innerHTML;
            	break;
            }        
        }
        consumableName=consumableName.toLowerCase();
        inputVal=inputVal.toLowerCase();
        if(chemtypeid==0){
    			if (consumableName==inputVal && inputId==consumableId){
            		document.getElementById('errorDisplay').style.visibility = "visible" ;
            		document.getElementById("save").disabled = true;
    			}
    			else
    			{
    				document.getElementById("save").disabled = false;
					document.getElementById('errorDisplay').style.visibility = "hidden" ;
    			}
    		}
        
    		else if(inputId==consumableId)
    		{
    			document.getElementById("save").disabled = true;
    			if (consumableName==inputVal && chem_type_id==chemtypeid)
    			{
    				document.getElementById("update").disabled = false;
    				document.getElementById("delete").disabled = false;
    				document.getElementById('errorDisplay').style.visibility = "hidden" ;
    			}
    			else
    			{
    				document.getElementById("update").disabled = true;
    				document.getElementById("delete").disabled = true;
					document.getElementById('errorDisplay').style.visibility = "visible" ;
    			}
    		}
        	else 
            {
            	document.getElementById("save").disabled = true;
            	if (consumableName==inputVal && chem_type_id==chemtypeid)
    			{
            		document.getElementById("update").disabled = true;
            		document.getElementById("delete").disabled = true;
					document.getElementById('errorDisplay').style.visibility = "visible" ;
    			}
    			else
    			{
    				document.getElementById("update").disabled = false;
    				document.getElementById("delete").disabled = true;
    				document.getElementById('errorDisplay').style.visibility = "hidden" ;
    			}
			}
    	
}


